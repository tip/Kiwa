/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.kinsources.kiwa.accounts.Account.EmailScope;
import org.kinsources.kiwa.accounts.Account.Status;

import fr.devinsy.util.xml.XMLBadFormatException;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLAccountManagerTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLAccountManagerTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAccount01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Account source = new Account(100, "foo@foo.org", "foo first name", "foo last name", "secret888");
		source.setStatus(Status.ACTIVATED);
		source.setEmailScope(EmailScope.PUBLIC);
		source.setTimeZone("Asia/Calcutta");
		source.setLastConnection(null);
		source.setBusinessCard(null);
		source.setCountry(null);
		source.setEmailNotification(false);
		source.setOrganization(null);
		source.setWebsite(null);

		//
		String xml = XMLAccountManager.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		Account target = XMLAccountManager.readAccount(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getEmail(), target.getEmail());
		Assert.assertEquals(source.getFullName(), target.getFullName());
		Assert.assertEquals(source.getStatus(), target.getStatus());
		Assert.assertEquals(source.getEmailScope(), target.getEmailScope());
		Assert.assertEquals(source.getTimeZone(), target.getTimeZone());
		Assert.assertEquals(source.getLastConnection(), target.getLastConnection());
		Assert.assertEquals(source.getBusinessCard(), target.getBusinessCard());
		Assert.assertEquals(source.getCountry(), target.getCountry());
		Assert.assertEquals(source.isEmailNotification(), target.isEmailNotification());
		Assert.assertEquals(source.getOrganization(), target.getOrganization());
		Assert.assertEquals(source.getWebsite(), target.getWebsite());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAccount02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Account source = new Account(100, "foo@foo.org", "foo first name", "foo last name", "secret888");
		source.setStatus(Status.ACTIVATED);
		source.setEmailScope(EmailScope.PUBLIC);
		source.setTimeZone("Asia/Calcutta");
		source.setLastConnection(null);
		source.setBusinessCard("About me me me.");
		source.setCountry("France");
		source.setEmailNotification(false);
		source.setOrganization("FOO corp.");
		source.setWebsite("http://foo.org/");
		source.roles().setRole(new Role(1, "alpha role"));
		source.attributes().add(new Attribute("label alpha", "value alpha"));

		//
		String xml = XMLAccountManager.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		Account target = XMLAccountManager.readAccount(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getEmail(), target.getEmail());
		Assert.assertEquals(source.getFullName(), target.getFullName());
		Assert.assertEquals(source.getStatus(), target.getStatus());
		Assert.assertEquals(source.getEmailScope(), target.getEmailScope());
		Assert.assertEquals(source.getTimeZone(), target.getTimeZone());
		Assert.assertEquals(source.getLastConnection(), target.getLastConnection());
		Assert.assertEquals(source.getBusinessCard(), target.getBusinessCard());
		Assert.assertEquals(source.getCountry(), target.getCountry());
		Assert.assertEquals(source.isEmailNotification(), target.isEmailNotification());
		Assert.assertEquals(source.getOrganization(), target.getOrganization());
		Assert.assertEquals(source.getWebsite(), target.getWebsite());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAccoutManager01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		AccountManager source = new AccountManager();

		//
		String xml = XMLAccountManager.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		AccountManager target = XMLAccountManager.readAccountManager(xml);

		//
		Assert.assertEquals(0, target.roles().size());
		Assert.assertEquals(0, target.accounts().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAccoutManager02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		AccountManager source = new AccountManager();
		source.roles().setRole(new Role(1, "alpha"));
		source.roles().setRole(new Role(2, "bravo"));
		source.roles().setRole(new Role(5, "charlie"));

		source.createAccount("alpha first name", "alpha last name", "alpha@foo.net", "secret888");
		source.createAccount("bravo first name", "bravo last name", "bravo@foo.net", "motdepasse");

		//
		String xml = XMLAccountManager.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		AccountManager target = XMLAccountManager.readAccountManager(xml);

		//
		Assert.assertEquals(3, target.roles().size());
		Assert.assertEquals(source.roles().getById(1).getId(), target.roles().getById(1).getId());
		Assert.assertEquals(source.roles().getById(1).getName(), target.roles().getById(1).getName());
		Assert.assertEquals(source.roles().getById(2).getId(), target.roles().getById(2).getId());
		Assert.assertEquals(source.roles().getById(2).getName(), target.roles().getById(2).getName());
		Assert.assertEquals(source.roles().getById(5).getId(), target.roles().getById(5).getId());
		Assert.assertEquals(source.roles().getById(5).getName(), target.roles().getById(5).getName());

		//
		Assert.assertEquals(2, target.accounts().size());
		Assert.assertEquals(source.accounts().getByIndex(0).getFullName(), target.accounts().getByIndex(0).getFullName());
		Assert.assertEquals(source.accounts().getByIndex(0).getEmail(), source.accounts().getByIndex(0).getEmail());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testRoles01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Roles source = new Roles();

		//
		String xml = XMLAccountManager.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Roles target = new Roles();
		XMLAccountManager.readRoles(target, xml);

		//
		Assert.assertEquals(0, target.toList().size());
		Assert.assertNull(target.getById(4));

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testRoles02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Roles source = new Roles();

		source.setRole(new Role(1, "alpha"));
		source.setRole(new Role(2, "bravo"));
		source.setRole(new Role(5, "charlie"));

		//
		String xml = XMLAccountManager.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Roles target = new Roles();
		XMLAccountManager.readRoles(target, xml);

		//
		Assert.assertEquals(3, target.toList().size());
		Assert.assertNull(target.getById(4));
		Assert.assertEquals(source.getById(1).getId(), target.getById(1).getId());
		Assert.assertEquals(source.getById(1).getName(), target.getById(1).getName());
		Assert.assertEquals(source.getById(2).getId(), target.getById(2).getId());
		Assert.assertEquals(source.getById(2).getName(), target.getById(2).getName());
		Assert.assertEquals(source.getById(5).getId(), target.getById(5).getId());
		Assert.assertEquals(source.getById(5).getName(), target.getById(5).getName());

		//
		logger.debug("===== test done.");
	}
}
