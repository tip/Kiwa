/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.exceptions.KiwaControlException;
import org.tip.puck.net.workers.AttributeFilter;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLWriter;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLKidarepTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLKidarepTest.class);

	/**
	 *  
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCollaborator01()
	{
		//
		logger.debug("===== test starting...");

		//
		new Collaborator(null, null, null);

		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testCollaborator02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Collaborator source = new Collaborator(new Account(17, "foo@foo.org", "FooFirstName", "FooLastName", "FooPassword"), null, null);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Collaborator target = XMLKidarep.readCollaborator(xmlSource);

		//
		Assert.assertEquals(source.getDescription(), target.getDescription());
		Assert.assertNotNull(source.getAccount());
		Assert.assertEquals(source.getAccount().getId(), target.getAccount().getId());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());
		Assert.assertTrue(target.filters().isEmpty());
		Assert.assertEquals(source.filters().size(), target.filters().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testCollaborator03() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Collaborator source = new Collaborator(new Account(17, "foo@foo.org", "FooFirstName", "FooLastName", "FooPassword"), "foo description", null);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Collaborator target = XMLKidarep.readCollaborator(xmlSource);

		//
		Assert.assertEquals(source.getDescription(), target.getDescription());
		Assert.assertNotNull(source.getAccount());
		Assert.assertEquals(source.getAccount().getId(), target.getAccount().getId());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());
		Assert.assertTrue(target.filters().isEmpty());
		Assert.assertEquals(source.filters().size(), target.filters().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testCollaborator04() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Collaborator source = new Collaborator(new Account(17, "foo@foo.org", "FooFirstName", "FooLastName", "FooPassword"), "foo description", null);

		source.filters().add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "fooA", AttributeFilter.Mode.REMOVE));
		source.filters().add(new AttributeFilter(AttributeFilter.Scope.FAMILIES, "fooB", AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING));
		source.filters().add(new AttributeFilter(AttributeFilter.Scope.CORPUS, "fooC", AttributeFilter.Mode.REPLACE_BY_BLANK));
		source.filters().add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "name", AttributeFilter.Mode.ANONYMIZE_BY_FIRST_NAME));
		source.filters().add(new AttributeFilter(AttributeFilter.Scope.RELATIONS, "fooD", AttributeFilter.Mode.CLEAN));

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Collaborator target = XMLKidarep.readCollaborator(xmlSource);

		//
		Assert.assertEquals(source.getDescription(), target.getDescription());
		Assert.assertNotNull(source.getAccount());
		Assert.assertEquals(source.getAccount().getId(), target.getAccount().getId());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());

		//
		Assert.assertFalse(target.filters().isEmpty());
		Assert.assertEquals(source.filters().size(), target.filters().size());

		//
		for (int index = 0; index < source.filters().size(); index++)
		{
			//
			AttributeFilter sourceFilter = source.filters().getByIndex(index);
			AttributeFilter targetFilter = target.filters().getByIndex(index);

			//
			Assert.assertEquals(sourceFilter.getLabel(), targetFilter.getLabel());
			Assert.assertEquals(sourceFilter.getOptionalRelationName(), targetFilter.getOptionalRelationName());
			Assert.assertEquals(sourceFilter.getScope(), targetFilter.getScope());
			Assert.assertEquals(sourceFilter.getMode(), targetFilter.getMode());
		}

		//
		logger.debug("===== test done.");
	}

	/**
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws XMLStreamException
	 * @throws XMLBadFormatException
	 */
	@Test
	public void testCollaborators01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Collaborators source = new Collaborators();

		//
		{
			Collaborator collaborator = new Collaborator(new Account(17, "foo@foo.org", "FooFirstName", "FooLastName", "FooPassword"), "foo description", null);

			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "foo", AttributeFilter.Mode.REMOVE));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.FAMILIES, "foo", AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.CORPUS, "foo", AttributeFilter.Mode.REPLACE_BY_BLANK));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "name", AttributeFilter.Mode.ANONYMIZE_BY_FIRST_NAME));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.RELATIONS, "foo", AttributeFilter.Mode.CLEAN));

			source.add(collaborator);
		}

		//
		{
			Collaborator collaborator = new Collaborator(new Account(17, "foo2@foo.org", "FooFirstName2", "FooLastName2", "FooPassword2"), "foo description2", null);

			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "foo2", AttributeFilter.Mode.REMOVE));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.FAMILIES, "foo2", AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.CORPUS, "foo2", AttributeFilter.Mode.REPLACE_BY_BLANK));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "name", AttributeFilter.Mode.ANONYMIZE_BY_FIRST_NAME));
			collaborator.filters().add(new AttributeFilter(AttributeFilter.Scope.RELATIONS, "foo2", AttributeFilter.Mode.CLEAN));

			source.add(collaborator);
		}

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Collaborators target = new Collaborators();
		XMLKidarep.readCollaborators(target, xmlSource);

		//
		Assert.assertFalse(target.isEmpty());
		Assert.assertEquals(source.size(), target.size());

		//
		{
			//
			Collaborator sourceCollaborator = source.getByIndex(0);
			Collaborator targetCollaborator = target.getByIndex(0);

			//
			Assert.assertEquals(sourceCollaborator.getDescription(), targetCollaborator.getDescription());
			Assert.assertNotNull(sourceCollaborator.getAccount());
			Assert.assertEquals(sourceCollaborator.getAccount().getId(), targetCollaborator.getAccount().getId());
			Assert.assertEquals(sourceCollaborator.getCreationDate().toString(), targetCollaborator.getCreationDate().toString());
			Assert.assertEquals(sourceCollaborator.getEditionDate().toString(), targetCollaborator.getEditionDate().toString());

			//
			Assert.assertFalse(targetCollaborator.filters().isEmpty());
			Assert.assertEquals(sourceCollaborator.filters().size(), targetCollaborator.filters().size());

			//
			for (int index = 0; index < sourceCollaborator.filters().size(); index++)
			{
				//
				AttributeFilter sourceFilter = sourceCollaborator.filters().getByIndex(index);
				AttributeFilter targetFilter = targetCollaborator.filters().getByIndex(index);

				//
				Assert.assertEquals(sourceFilter.getLabel(), targetFilter.getLabel());
				Assert.assertEquals(sourceFilter.getOptionalRelationName(), targetFilter.getOptionalRelationName());
				Assert.assertEquals(sourceFilter.getScope(), targetFilter.getScope());
				Assert.assertEquals(sourceFilter.getMode(), targetFilter.getMode());
			}
		}

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testFormatIssue01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		byte[] data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		LoadedFile loadedFile = new LoadedFile("tester", data);
		FormatIssue source = new FormatIssue(10, 20, 30, DateTime.now(), loadedFile);

		//
		String xml = XMLKidarep.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		FormatIssue target = XMLKidarep.readFormatIssue(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getContributorId(), target.getContributorId());
		Assert.assertEquals(source.getDatasetId(), target.getDatasetId());
		Assert.assertEquals(source.getStatus().toString(), target.getStatus().toString());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());

		Assert.assertEquals(source.getFile().name(), target.getFile().name());
		Assert.assertEquals(source.getFile().header().digest(), target.getFile().header().digest());
		Assert.assertEquals(source.getFile().header().id(), target.getFile().header().id());
		Assert.assertEquals(source.getFile().header().length(), target.getFile().header().length());
		Assert.assertEquals(source.getFile().header().loadingDate().toString(), target.getFile().header().loadingDate().toString());
		Assert.assertEquals(source.getFile().header().name(), target.getFile().header().name());
		Assert.assertTrue(Arrays.equals(source.getFile().data(), target.getFile().data()));

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testKidarep01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Kidarep source = new Kidarep();

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Kidarep target = XMLKidarep.readKidarep(xmlSource);

		//
		Assert.assertEquals(0, source.countOfContributors());
		Assert.assertEquals(0, source.countOfDatasets());
		Assert.assertEquals(0, source.countOfCollections());

		//
		Assert.assertEquals(source.countOfContributors(), target.countOfContributors());
		Assert.assertEquals(source.countOfDatasets(), target.countOfDatasets());
		Assert.assertEquals(source.countOfCollections(), target.countOfCollections());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testKidarep02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Kidarep source = new Kidarep();

		source.createContributor(new Account(10, "10@foo.org", "FirstNameTen", "LastNameTen", "secret10"));
		source.createContributor(new Account(20, "20@foo.org", "FirstNameTwenty", "LastNameTwenty", "secret20"));

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Kidarep target = XMLKidarep.readKidarep(xmlSource);

		//
		Assert.assertEquals(2, source.countOfContributors());
		Assert.assertEquals(0, source.countOfDatasets());
		Assert.assertEquals(0, source.countOfCollections());

		//
		Assert.assertEquals(source.countOfContributors(), target.countOfContributors());
		Assert.assertEquals(source.countOfDatasets(), target.countOfDatasets());
		Assert.assertEquals(source.countOfCollections(), target.countOfCollections());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testKidarep03() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Kidarep source = new Kidarep();

		Contributor l10 = source.createContributor(new Account(10, "10@foo.org", "FirstNameTen", "LastNameTen", "secret10"));
		Contributor l20 = source.createContributor(new Account(20, "20@foo.org", "FirstNameTwenty", "LastNameTwenty", "secret20"));

		Dataset dataset1 = source.createDataset(l10, "Hundred one dataset");
		Dataset dataset2 = source.createDataset(l10, "Hundred two dataset");

		Collection col1 = source.createCollection(l10, "A new collection", "blablae dlblal", Collection.Scope.PUBLIC);
		source.createCollection(l10, "A new collection2", "blablae dlblal2", Collection.Scope.PRIVATE);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Kidarep target = XMLKidarep.readKidarep(xmlSource);

		//
		Assert.assertEquals(2, source.countOfContributors());
		Assert.assertEquals(2, source.countOfDatasets());
		Assert.assertEquals(2, source.countOfCollections());

		//
		Assert.assertEquals(source.countOfContributors(), target.countOfContributors());
		Assert.assertEquals(source.countOfDatasets(), target.countOfDatasets());
		Assert.assertEquals(source.countOfCollections(), target.countOfCollections());

		//
		Dataset sourceDataset = source.contributors().getByIndex(0).personalDatasets().getByIndex(0);
		Dataset targetDataset = target.contributors().getByIndex(0).personalDatasets().getByIndex(0);

		Assert.assertEquals(sourceDataset.getAtlasCode(), targetDataset.getAtlasCode());
		Assert.assertEquals(sourceDataset.getAuthor(), targetDataset.getAuthor());
		Assert.assertEquals(sourceDataset.getBibliography(), targetDataset.getBibliography());
		Assert.assertEquals(sourceDataset.getCitation(), targetDataset.getCitation());
		Assert.assertEquals(sourceDataset.getCoder(), targetDataset.getCoder());
		Assert.assertEquals(sourceDataset.getCollectionNotes(), targetDataset.getCollectionNotes());
		Assert.assertEquals(sourceDataset.getContact(), targetDataset.getContact());
		Assert.assertEquals(sourceDataset.getCountry(), targetDataset.getCountry());
		Assert.assertEquals(sourceDataset.getCoverage(), targetDataset.getCoverage());
		Assert.assertEquals(sourceDataset.getCreationDate().toString(), targetDataset.getCreationDate().toString());
		Assert.assertEquals(sourceDataset.getDescription(), targetDataset.getDescription());
		Assert.assertEquals(sourceDataset.getEditionDate().toString(), targetDataset.getEditionDate().toString());
		Assert.assertEquals(sourceDataset.getEthnicOrCulturalGroup(), targetDataset.getEthnicOrCulturalGroup());
		Assert.assertEquals(sourceDataset.getGeographicCoordinate(), targetDataset.getGeographicCoordinate());
		Assert.assertEquals(sourceDataset.getHistory(), targetDataset.getHistory());
		Assert.assertEquals(sourceDataset.getId(), targetDataset.getId());
		Assert.assertEquals(sourceDataset.getLicense(), targetDataset.getLicense());
		Assert.assertEquals(sourceDataset.getName(), targetDataset.getName());
		Assert.assertEquals(sourceDataset.getOtherRepositories(), targetDataset.getOtherRepositories());
		Assert.assertEquals(sourceDataset.getPeriod(), targetDataset.getPeriod());
		Assert.assertEquals(sourceDataset.getPeriodNote(), targetDataset.getPeriodNote());
		Assert.assertEquals(sourceDataset.getPermanentLink(), targetDataset.getPermanentLink());
		Assert.assertEquals(sourceDataset.getPublicationDate(), targetDataset.getPublicationDate());
		Assert.assertEquals(sourceDataset.getRadiusFromCenter(), targetDataset.getRadiusFromCenter());
		Assert.assertEquals(sourceDataset.getReference(), targetDataset.getReference());
		Assert.assertEquals(sourceDataset.getRegion(), targetDataset.getRegion());
		Assert.assertEquals(sourceDataset.getShortDescription(), targetDataset.getShortDescription());
		Assert.assertEquals(sourceDataset.getStatus().toString(), targetDataset.getStatus().toString());
		Assert.assertEquals(sourceDataset.getSubmissionDate(), targetDataset.getSubmissionDate());

		Assert.assertEquals(sourceDataset.getOriginFile(), targetDataset.getOriginFile());
		Assert.assertEquals(sourceDataset.getPublicFile(), targetDataset.getPublicFile());
		Assert.assertEquals(sourceDataset.attachments().size(), targetDataset.attachments().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testKidarep04() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Kidarep source = new Kidarep();

		Contributor l10 = source.createContributor(new Account(10, "10@foo.org", "FirtNameTen", "LastNameTen", "secret10"));
		Contributor l20 = source.createContributor(new Account(20, "20@foo.org", "FirstNameTwenty", "LastNameTwenty", "secret20"));

		Dataset dataset1 = source.createDataset(l10, "Hundred one dataset");
		dataset1.setAtlasCode("altas code, qdlfkjqsf");
		dataset1.setAuthor("me");
		dataset1.setBibliography("a bibli value");
		dataset1.setCitation("a citation value");
		dataset1.setCoder("a coder value");
		dataset1.setCollectionNotes("a collection note value");
		dataset1.setContact("a contact value");
		dataset1.setCountry("a country value");
		dataset1.setCoverage("a coverage value");
		dataset1.setDescription("a description value");
		dataset1.setEthnicOrCulturalGroup("ethnical value");
		dataset1.setGeographicCoordinate("geographical coordinate");
		dataset1.setHistory("A history value");
		dataset1.setLicense("A license value");
		dataset1.setName("a name value");
		dataset1.setOtherRepositories("an other repositories value");
		dataset1.setPeriod("a period value");
		dataset1.setPeriodNote("a period note value");
		dataset1.setPermanentLink("a permanent link value");
		dataset1.setPublicationDate(DateTime.now());
		dataset1.setRadiusFromCenter(12L);
		dataset1.setReference("a reference value");
		dataset1.setRegion("a region value");
		dataset1.setShortDescription("a short description value");
		dataset1.setStatus(Dataset.Status.VALIDATED);
		dataset1.setSubmissionDate(DateTime.now());

		Dataset dataset2 = source.createDataset(l10, "Hundred two dataset");

		Collection col1 = source.createCollection(l10, "A new collection", "blablae dlblal", Collection.Scope.PUBLIC);
		source.createCollection(l10, "A new collection2", "blablae dlblal2", Collection.Scope.PRIVATE);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Kidarep target = XMLKidarep.readKidarep(xmlSource);

		//
		Assert.assertEquals(2, source.countOfContributors());
		Assert.assertEquals(2, source.countOfDatasets());
		Assert.assertEquals(2, source.countOfCollections());

		//
		Assert.assertEquals(source.countOfContributors(), target.countOfContributors());
		Assert.assertEquals(source.countOfDatasets(), target.countOfDatasets());
		Assert.assertEquals(source.countOfCollections(), target.countOfCollections());

		//
		Dataset sourceDataset = source.contributors().getByIndex(0).personalDatasets().getByIndex(0);
		Dataset targetDataset = target.contributors().getByIndex(0).personalDatasets().getByIndex(0);

		Assert.assertEquals(sourceDataset.getAtlasCode(), targetDataset.getAtlasCode());
		Assert.assertEquals(sourceDataset.getAuthor(), targetDataset.getAuthor());
		Assert.assertEquals(sourceDataset.getBibliography(), targetDataset.getBibliography());
		Assert.assertEquals(sourceDataset.getCoder(), targetDataset.getCoder());
		Assert.assertEquals(sourceDataset.getCitation(), targetDataset.getCitation());
		Assert.assertEquals(sourceDataset.getCollectionNotes(), targetDataset.getCollectionNotes());
		Assert.assertEquals(sourceDataset.getContact(), targetDataset.getContact());
		Assert.assertEquals(sourceDataset.getCountry(), targetDataset.getCountry());
		Assert.assertEquals(sourceDataset.getCoverage(), targetDataset.getCoverage());
		Assert.assertEquals(sourceDataset.getCreationDate().toString(), targetDataset.getCreationDate().toString());
		Assert.assertEquals(sourceDataset.getDescription(), targetDataset.getDescription());
		Assert.assertEquals(sourceDataset.getEditionDate().toString(), targetDataset.getEditionDate().toString());
		Assert.assertEquals(sourceDataset.getEthnicOrCulturalGroup(), targetDataset.getEthnicOrCulturalGroup());
		Assert.assertEquals(sourceDataset.getGeographicCoordinate(), targetDataset.getGeographicCoordinate());
		Assert.assertEquals(sourceDataset.getHistory(), targetDataset.getHistory());
		Assert.assertEquals(sourceDataset.getId(), targetDataset.getId());
		Assert.assertEquals(sourceDataset.getLicense(), targetDataset.getLicense());
		Assert.assertEquals(sourceDataset.getName(), targetDataset.getName());
		Assert.assertEquals(sourceDataset.getOtherRepositories(), targetDataset.getOtherRepositories());
		Assert.assertEquals(sourceDataset.getPeriod(), targetDataset.getPeriod());
		Assert.assertEquals(sourceDataset.getPeriodNote(), targetDataset.getPeriodNote());
		Assert.assertEquals(sourceDataset.getPermanentLink(), targetDataset.getPermanentLink());
		Assert.assertEquals(sourceDataset.getPublicationDate().toString(), targetDataset.getPublicationDate().toString());
		Assert.assertEquals(sourceDataset.getRadiusFromCenter(), targetDataset.getRadiusFromCenter());
		Assert.assertEquals(sourceDataset.getReference(), targetDataset.getReference());
		Assert.assertEquals(sourceDataset.getRegion(), targetDataset.getRegion());
		Assert.assertEquals(sourceDataset.getShortDescription(), targetDataset.getShortDescription());
		Assert.assertEquals(sourceDataset.getStatus().toString(), targetDataset.getStatus().toString());
		Assert.assertEquals(sourceDataset.getSubmissionDate().toString(), targetDataset.getSubmissionDate().toString());

		Assert.assertEquals(sourceDataset.getOriginFile(), targetDataset.getOriginFile());
		Assert.assertEquals(sourceDataset.getPublicFile(), targetDataset.getPublicFile());
		Assert.assertEquals(sourceDataset.attachments().size(), targetDataset.attachments().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws IOException
	 * @throws KiwaControlException
	 * 
	 */
	@Test
	public void testKidarep05() throws XMLStreamException, XMLBadFormatException, KiwaControlException, IOException
	{
		//
		logger.debug("===== test starting...");

		//
		Kidarep source = new Kidarep();

		Contributor l10 = source.createContributor(new Account(10, "10@foo.org", "FirstNameTen", "LastNameTen", "secret10"));
		Contributor l20 = source.createContributor(new Account(20, "20@foo.org", "FirstNameTwenty", "LastNameTwenty", "secret20"));

		Dataset dataset1 = source.createDataset(l10, "Hundred one dataset");

		dataset1.setOriginFile(new DatasetFile(100, "toto.puc", null));
		byte[] data1 = { 10, 20, 30, 40, 50 };
		dataset1.setPublicFile(new DatasetFile(101, "toto1.puc", data1));
		dataset1.getPublicFile().setDownloadCount(122);
		byte[] data2 = { 11, 21, 31, 41, 51 };
		source.addAttachment(dataset1, "toto.pdf", data2);

		Dataset dataset2 = source.createDataset(l10, "Hundred two dataset");

		Collection col1 = source.createCollection(l10, "A new collection", "blablae dlblal", Collection.Scope.PUBLIC);
		source.createCollection(l10, "A new collection2", "blablae dlblal2", Collection.Scope.PRIVATE);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Kidarep target = XMLKidarep.readKidarep(xmlSource);

		//
		Assert.assertEquals(2, source.countOfContributors());
		Assert.assertEquals(2, source.countOfDatasets());
		Assert.assertEquals(2, source.countOfCollections());

		//
		Assert.assertEquals(source.countOfContributors(), target.countOfContributors());
		Assert.assertEquals(source.countOfDatasets(), target.countOfDatasets());
		Assert.assertEquals(source.countOfCollections(), target.countOfCollections());

		//
		Dataset sourceDataset = source.contributors().getByIndex(0).personalDatasets().getByIndex(0);
		Dataset targetDataset = target.contributors().getByIndex(0).personalDatasets().getByIndex(0);

		//
		{
			DatasetFile sourceFile = sourceDataset.getOriginFile();
			DatasetFile targetFile = targetDataset.getOriginFile();
			Assert.assertEquals(sourceFile.digest(), targetFile.digest());
			Assert.assertEquals(sourceFile.id(), targetFile.id());
			Assert.assertEquals(sourceFile.length(), targetFile.length());
			Assert.assertEquals(sourceFile.loadingDate().toString(), targetFile.loadingDate().toString());
			Assert.assertEquals(sourceFile.name(), targetFile.name());
			Assert.assertEquals(sourceFile.getDownloadCount(), targetFile.getDownloadCount());
			Assert.assertEquals(sourceFile.statistics().size(), targetFile.statistics().size());
		}

		//
		{
			DatasetFile sourceFile = sourceDataset.getPublicFile();
			DatasetFile targetFile = targetDataset.getPublicFile();
			Assert.assertEquals(sourceFile.digest(), targetFile.digest());
			Assert.assertEquals(sourceFile.id(), targetFile.id());
			Assert.assertEquals(sourceFile.length(), targetFile.length());
			Assert.assertEquals(sourceFile.loadingDate().toString(), targetFile.loadingDate().toString());
			Assert.assertEquals(sourceFile.name(), targetFile.name());
			Assert.assertEquals(sourceFile.getDownloadCount(), targetFile.getDownloadCount());
			Assert.assertEquals(sourceFile.statistics().size(), targetFile.statistics().size());
		}

		//
		{
			LoadedFileHeader sourceFile = sourceDataset.attachments().getByIndex(0);
			LoadedFileHeader targetFile = targetDataset.attachments().getByIndex(0);
			Assert.assertEquals(sourceFile.digest(), targetFile.digest());
			Assert.assertEquals(sourceFile.id(), targetFile.id());
			Assert.assertEquals(sourceFile.length(), targetFile.length());
			Assert.assertEquals(sourceFile.loadingDate().toString(), targetFile.loadingDate().toString());
			Assert.assertEquals(sourceFile.name(), targetFile.name());
		}

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testKidarep06() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Kidarep source = new Kidarep();

		Contributor l10 = source.createContributor(new Account(10, "10@foo.org", "FirstNameTen", "LastNameTen", "secret10"));
		Contributor l20 = source.createContributor(new Account(20, "20@foo.org", "FirstNameTwenty", "LastNameTwenty", "secret20"));

		Dataset dataset1 = source.createDataset(l10, "Hundred one dataset");
		Dataset dataset2 = source.createDataset(l10, "Hundred two dataset");

		Collection col1 = source.createCollection(l10, "A new collection", "blablae dlblal", Collection.Scope.PUBLIC);
		source.createCollection(l10, "A new collection2", "blablae dlblal2", Collection.Scope.PRIVATE);

		col1.datasets().add(dataset1);
		col1.datasets().add(dataset2);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLKidarep.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Kidarep target = XMLKidarep.readKidarep(xmlSource);

		//
		Assert.assertEquals(2, source.countOfContributors());
		Assert.assertEquals(2, source.countOfDatasets());
		Assert.assertEquals(2, source.countOfCollections());

		//
		Assert.assertEquals(source.countOfContributors(), target.countOfContributors());
		Assert.assertEquals(source.countOfDatasets(), target.countOfDatasets());
		Assert.assertEquals(source.countOfCollections(), target.countOfCollections());

		//
		Collection sourceCollection = source.contributors().getByIndex(0).collections().getByIndex(0);
		Collection targetCollection = target.contributors().getByIndex(0).collections().getByIndex(0);
		Assert.assertEquals(sourceCollection.countOfDatasets(), targetCollection.countOfDatasets());
		Assert.assertEquals(sourceCollection.getCreationDate().toString(), targetCollection.getCreationDate().toString());
		Assert.assertEquals(sourceCollection.getDescription(), targetCollection.getDescription());
		Assert.assertEquals(sourceCollection.getEditionDate().toString(), targetCollection.getEditionDate().toString());
		Assert.assertEquals(sourceCollection.getOwner().getId(), targetCollection.getOwner().getId());
		Assert.assertEquals(sourceCollection.getScope().toString(), targetCollection.getScope().toString());

		//
		sourceCollection = source.contributors().getByIndex(0).collections().getByIndex(1);
		targetCollection = target.contributors().getByIndex(0).collections().getByIndex(1);
		Assert.assertEquals(sourceCollection.countOfDatasets(), targetCollection.countOfDatasets());
		Assert.assertEquals(sourceCollection.getCreationDate().toString(), targetCollection.getCreationDate().toString());
		Assert.assertEquals(sourceCollection.getDescription(), targetCollection.getDescription());
		Assert.assertEquals(sourceCollection.getEditionDate().toString(), targetCollection.getEditionDate().toString());
		Assert.assertEquals(sourceCollection.getOwner().getId(), targetCollection.getOwner().getId());
		Assert.assertEquals(sourceCollection.getScope().toString(), targetCollection.getScope().toString());

		//
		logger.debug("===== test done.");
	}
}
