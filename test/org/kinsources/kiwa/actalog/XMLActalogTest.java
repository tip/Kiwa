/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actalog;

import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.kinsources.kiwa.actalog.EventLog.Category;

import fr.devinsy.util.xml.XMLBadFormatException;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLActalogTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLActalogTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActalog01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actalog source = new Actalog();

		//
		String xml = XMLActalog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actalog target = XMLActalog.readActalog(xml);

		//
		Assert.assertEquals(source.lastId(), target.lastId());
		Assert.assertEquals(source.eventLogs().size(), target.eventLogs().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActalog02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actalog source = new Actalog();

		source.logEvent(Category.PUBLICATION, "test1");
		source.logEvent(Category.REGISTRATION, "test2", "param1", "param2");

		//
		String xml = XMLActalog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actalog target = XMLActalog.readActalog(xml);

		//
		Assert.assertEquals(source.lastId(), target.lastId());
		Assert.assertEquals(source.eventLogs().size(), target.eventLogs().size());

		Assert.assertEquals(source.eventLogs().getByIndex(0).getId(), target.eventLogs().getByIndex(0).getId());
		Assert.assertEquals(source.eventLogs().getByIndex(0).getTime().toString(), target.eventLogs().getByIndex(0).getTime().toString());
		Assert.assertEquals(source.eventLogs().getByIndex(0).getCategory(), target.eventLogs().getByIndex(0).getCategory());
		Assert.assertEquals(source.eventLogs().getByIndex(0).getArguments().length, target.eventLogs().getByIndex(0).getArguments().length);

		Assert.assertEquals(source.eventLogs().getByIndex(1).getId(), target.eventLogs().getByIndex(1).getId());
		Assert.assertEquals(source.eventLogs().getByIndex(1).getTime().toString(), target.eventLogs().getByIndex(1).getTime().toString());
		Assert.assertEquals(source.eventLogs().getByIndex(1).getCategory(), target.eventLogs().getByIndex(1).getCategory());
		Assert.assertEquals(source.eventLogs().getByIndex(1).getArguments().length, target.eventLogs().getByIndex(1).getArguments().length);
		Assert.assertEquals(source.eventLogs().getByIndex(1).getArguments()[0], target.eventLogs().getByIndex(1).getArguments()[0]);

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testLog01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		EventLog source = new EventLog(10, DateTime.now(), Category.PUBLICATION);

		//
		String xml = XMLActalog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		EventLog target = XMLActalog.readEventLog(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getTime().toString(), target.getTime().toString());
		Assert.assertEquals(source.getCategory(), target.getCategory());
		Assert.assertEquals(source.getArguments().length, target.getArguments().length);

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testLog02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		EventLog source = new EventLog(12, DateTime.now(), Category.PUBLICATION, "arg1", "arg2", "arg3");

		//
		String xml = XMLActalog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		EventLog target = XMLActalog.readEventLog(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getTime().toString(), target.getTime().toString());
		Assert.assertEquals(source.getCategory(), target.getCategory());
		Assert.assertEquals(source.getArguments().length, target.getArguments().length);
		Assert.assertEquals(source.getArguments()[0], target.getArguments()[0]);
		Assert.assertEquals(source.getArguments()[1], target.getArguments()[1]);
		Assert.assertEquals(source.getArguments()[2], target.getArguments()[2]);

		//
		logger.debug("===== test done.");
	}

}
