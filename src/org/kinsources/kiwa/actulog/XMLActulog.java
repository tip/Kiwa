/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actulog;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Actulog File reader and writer.
 * 
 * @author TIP
 */
public class XMLActulog
{
	private static final Logger logger = LoggerFactory.getLogger(XMLActulog.class);

	private static final String DETAIL_SEPARATOR = "£";

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Actulog load(final File file) throws Exception
	{
		Actulog result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readActulog(in);
		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	static public Actulog readActulog(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Actulog result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readActulog(in);

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	static public Actulog readActulog(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Actulog result;

		//
		in.readStartTag("actulog");

		//
		{
			//
			long lastId = Long.parseLong(in.readContentTag("last_id").getContent());

			//
			result = new Actulog(lastId);

			//
			readWires(result.wires(), in);
		}

		//
		in.readEndTag("actulog");

		//
		result.resetLastId();
		result.rebuildIndexes();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	static public Wire readWire(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Wire result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readWire(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	static public Wire readWire(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Wire result;

		//
		in.readStartTag("wire");

		//
		{
			long id = Long.parseLong(in.readContentTag("id").getContent());

			String title = StringEscapeUtils.unescapeXml(in.readContentTag("title").getContent());

			String author = StringEscapeUtils.unescapeXml(in.readNullableContentTag("author").getContent());

			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());

			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());

			DateTime publicationDate;
			String publicationDateValue = in.readNullableContentTag("publication_date").getContent();
			if (publicationDateValue == null)
			{
				publicationDate = null;
			}
			else
			{
				publicationDate = DateTime.parse(publicationDateValue);
			}

			Locale locale = new Locale(StringEscapeUtils.unescapeXml(in.readContentTag("locale").getContent()));

			String leadParagraph = StringEscapeUtils.unescapeXml(in.readNullableContentTag("lead_paragraph").getContent());

			String body = StringEscapeUtils.unescapeXml(in.readNullableContentTag("body").getContent());

			//
			result = new Wire(id, title, author, creationDate, editionDate, publicationDate, locale, leadParagraph, body);
		}

		//
		in.readEndTag("wire");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	static public void readWires(final Wires target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("wires");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("wire"))
			{
				//
				Wire wire = readWire(in);

				//
				target.add(wire);
			}

			//
			in.readEndTag("wires");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Actulog source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				logger.warn(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	static public String toXMLString(final Actulog source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	static public String toXMLString(final Wire source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLActulog.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Actulog source)
	{
		//
		out.writeStartTag("actulog");

		//
		{
			//
			out.writeTag("last_id", source.lastId());

			//
			write(out, source.wires());
		}

		//
		out.writeEndTag("actulog");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	static public void write(final XMLWriter out, final Wire source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("wire");

			//
			{
				out.writeTag("id", source.getId());
				out.writeTag("title", StringEscapeUtils.escapeXml(source.getTitle()));
				out.writeTag("author", StringEscapeUtils.escapeXml(source.getAuthor()));
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				if (source.getPublicationDate() == null)
				{
					out.writeTag("publication_date", null);
				}
				else
				{
					out.writeTag("publication_date", source.getPublicationDate().toString());
				}

				out.writeTag("locale", source.getLocale().getLanguage());
				out.writeTag("lead_paragraph", source.getLeadParagraph());
				out.writeTag("body", source.getBody());
			}

			//
			out.writeEndTag("wire");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	static public void write(final XMLWriter out, final Wires source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("wires");
		}
		else
		{
			//
			out.writeStartTag("wires", "size", String.valueOf(source.size()));

			//
			{
				for (Wire log : source)
				{
					write(out, log);
				}
			}

			//
			out.writeEndTag("wires");
		}
	}
}
