/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import org.tip.puck.net.workers.AttributeFilter;
import org.tip.puck.net.workers.AttributeFilters;

import fr.devinsy.util.StringList;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Filter
{
	public enum IndividualNameAnonymize
	{
		NONE,
		BY_GENDER_AND_ID,
		BY_FIRST_NAME,
		BY_LAST_NAME
	}

	private IndividualNameAnonymize individualNameAnonymize;
	private AttributeFilters attributeFilters;

	/**
	 * 
	 */
	public Filter()
	{
		this.individualNameAnonymize = IndividualNameAnonymize.NONE;
		this.attributeFilters = new AttributeFilters();
	}

	/**
	 * 
	 * @return
	 */
	public AttributeFilters attributeFilters()
	{
		AttributeFilters result;

		result = this.attributeFilters;

		//
		return result;
	}

	public IndividualNameAnonymize getIndividualNameAnonymize()
	{
		return this.individualNameAnonymize;
	}

	public void setIndividualNameAnonymize(final IndividualNameAnonymize individualNameAnonymize)
	{
		this.individualNameAnonymize = individualNameAnonymize;
	}

	/**
	 * 
	 */
	@Override
	public String toString()
	{
		String result;

		//
		StringList buffer = new StringList();

		//
		if ((this.individualNameAnonymize != null) && (this.individualNameAnonymize != IndividualNameAnonymize.NONE))
		{
			buffer.append("individualNameAnonymize=" + this.individualNameAnonymize.toString());
		}

		//
		for (AttributeFilter attributeFilter : this.attributeFilters)
		{
			//
			if ((attributeFilter.getMode() != null) && (attributeFilter.getMode() != AttributeFilter.Mode.NONE) && (attributeFilter.getScope() != null)
					&& (attributeFilter.getScope() != AttributeFilter.Scope.NONE))
			{
				//
				String scope;
				if (attributeFilter.getScope() == AttributeFilter.Scope.RELATION)
				{
					scope = attributeFilter.getOptionalRelationName();
				}
				else
				{
					scope = attributeFilter.getScope().name().toLowerCase();
				}

				//
				String infos = String.format("%s.%s=%s", scope, attributeFilter.getLabel(), attributeFilter.getMode().name().toLowerCase());

				//
				buffer.append(infos);
			}
		}

		result = buffer.toStringWithCommas();

		//
		return result;
	}
}
