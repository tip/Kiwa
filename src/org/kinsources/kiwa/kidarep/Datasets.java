/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.kinsources.kiwa.kidarep.Dataset.Status;
import org.kinsources.kiwa.kidarep.Dataset.Type;
import org.kinsources.kiwa.kidarep.DatasetComparator.Criteria;

/**
 * The <code>Accounts</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Datasets implements Iterable<Dataset>
{
	private List<Dataset> datasets;

	/**
	 * 
	 */
	public Datasets()
	{
		super();

		this.datasets = new ArrayList<Dataset>();
	}

	/**
	 * 
	 */
	public Datasets(final int initialCapacity)
	{
		this.datasets = new ArrayList<Dataset>(initialCapacity);
	}

	/**
	 * 
	 */
	public Dataset add(final Dataset source)
	{
		Dataset result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.datasets.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final Datasets source)
	{
		if (source != null)
		{
			for (Dataset dataset : source)
			{
				if (!this.datasets.contains(dataset))
				{
					add(dataset);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.datasets.clear();
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public boolean contains(final Dataset criteria)
	{
		boolean result;

		result = this.datasets.contains(criteria);

		//
		return result;
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Datasets copy()
	{
		Datasets result;

		//
		result = new Datasets(this.datasets.size());

		//
		for (Dataset account : this.datasets)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public long count(final Status status)
	{
		long result;

		result = 0;
		for (Dataset dataset : this.datasets)
		{
			if (dataset.getStatus() == status)
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfAttachmentFiles()
	{
		long result;

		result = 0;
		for (Dataset dataset : this.datasets)
		{
			result += dataset.countOfAttachmentFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetFiles()
	{
		long result;

		result = 0;
		for (Dataset dataset : this.datasets)
		{
			result += dataset.countOfDatasetFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetFiles(final Status status)
	{
		long result;

		result = 0;
		for (Dataset dataset : this.datasets)
		{
			if ((dataset.getStatus() == status) && (dataset.getOriginFile() != null))
			{
				result += dataset.countOfDatasetFiles();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasets()
	{
		long result;

		result = this.datasets.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFiles()
	{
		long result;

		result = 0;
		for (Dataset dataset : this.datasets)
		{
			result += dataset.countOfFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets find(final Status status)
	{
		Datasets result;

		//
		result = new Datasets();
		for (Dataset dataset : this.datasets)
		{
			if (dataset.getStatus() == status)
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets find(final Type type)
	{
		Datasets result;

		result = new Datasets();
		for (Dataset dataset : this.datasets)
		{
			if (dataset.getType() == type)
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets findGenealogies()
	{
		Datasets result;

		result = find(Type.GENEALOGY);

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findTerminologies()
	{
		Datasets result;

		result = find(Type.TERMINOLOGY);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets findVisible(final Long accountId, final Dataset.Status status)
	{
		Datasets result;

		//
		result = new Datasets(this.datasets.size());

		//
		if (accountId == null)
		{
			result = find(status);
		}
		else if (status == null)
		{
			result = copy();
		}
		else
		{
			for (Dataset dataset : this.datasets)
			{
				if ((dataset.getContributor().getId() == accountId) || (dataset.getStatus() == status))
				{
					result.add(dataset);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findWithCollaborator()
	{
		Datasets result;

		//
		result = new Datasets();

		//
		for (Dataset dataset : this.copy())
		{
			if (dataset.hascollaborators())
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findWithKinshipFile()
	{
		Datasets result;

		result = new Datasets();
		for (Dataset dataset : this.datasets)
		{
			if (dataset.getOriginFile() != null)
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Dataset getByIndex(final int index)
	{
		Dataset result;

		result = this.datasets.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.datasets.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Dataset> iterator()
	{
		Iterator<Dataset> result;

		result = this.datasets.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Dataset dataset : this.datasets)
		{
			if (dataset.getId() > result)
			{
				result = dataset.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Dataset account)
	{
		this.datasets.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public Datasets reverse()
	{
		Datasets result;

		//
		Collections.reverse(this.datasets);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.datasets.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortBy(final Criteria sortCriteria)
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(sortCriteria));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortBy(final String sortCriteria)
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(sortCriteria));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortByEditionDate()
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(Criteria.EDITION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortById()
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortByName()
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(Criteria.NAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortByPublicationDate()
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(Criteria.PUBLICATION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets sortBySubmissionDate()
	{
		Datasets result;

		//
		Collections.sort(this.datasets, new DatasetComparator(Criteria.SUBMISSION_DATE));

		//
		result = this;

		//
		return result;
	}
}
