/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeFilter;
import org.tip.puck.net.workers.AttributeFilters;

import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Kidarep File reader and writer.
 * 
 * @author TIP
 */
public class XMLKidarep
{
	public enum Mode
	{
		SHALLOW,
		DEEP
	}

	private static final Logger logger = LoggerFactory.getLogger(XMLKidarep.class);

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Kidarep load(final File file) throws Exception
	{
		Kidarep result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readKidarep(in);
		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static AttributeFilter readAttributeFilter(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		AttributeFilter result;

		//
		in.readStartTag("attribute_filter");

		//
		{
			//
			AttributeFilter.Scope scope = EnumUtils.getEnum(AttributeFilter.Scope.class, in.readContentTag("scope").getContent());
			String relationName = StringEscapeUtils.unescapeXml(in.readNullableContentTag("optional_relation_name").getContent());
			String label = StringEscapeUtils.unescapeXml(in.readContentTag("label").getContent());
			AttributeFilter.Mode mode = EnumUtils.getEnum(AttributeFilter.Mode.class, in.readContentTag("mode").getContent());

			//
			if (StringUtils.isBlank(relationName))
			{
				result = new AttributeFilter(scope, label, mode);
			}
			else
			{
				result = new AttributeFilter(relationName, label, mode);
			}
		}

		//
		in.readEndTag("attribute_filter");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readAttributeFilters(final AttributeFilters target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		if (in.hasNextStartTag("attribute_filters"))
		{
			//
			XMLTag list = in.readListTag("attribute_filters");

			//
			if (list.getType() != TagType.EMPTY)
			{
				while (in.hasNextStartTag("attribute_filter"))
				{
					AttributeFilter filter = readAttributeFilter(in);

					target.add(filter);
				}

				//
				in.readEndTag("attribute_filters");
			}
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Collaborator readCollaborator(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Collaborator result;

		//
		in.readStartTag("collaborator");

		//
		{
			//
			long accountId = Long.parseLong(in.readContentTag("account_id").getContent());
			String description = StringEscapeUtils.unescapeXml(in.readNullableContentTag("description").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());

			result = new Collaborator(new Account(accountId, "foo@foo.org", "foo", "foo", null), description, creationDate, editionDate);

			result.setFile(readDatasetFile(in));
			readAttributeFilters(result.filters(), in);
		}

		//
		in.readEndTag("collaborator");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readCollaborators(final Collaborators target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		if (in.hasNextStartTag("collaborators"))
		{
			//
			XMLTag list = in.readListTag("collaborators");

			//
			if (list.getType() != TagType.EMPTY)
			{
				while (in.hasNextStartTag("collaborator"))
				{
					Collaborator collaborator = readCollaborator(in);

					target.add(collaborator);
				}

				//
				in.readEndTag("collaborators");
			}
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 * @throws Exception
	 */
	public static Collection readCollection(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Collection result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readCollection(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Collection readCollection(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Collection result;

		//
		in.readStartTag("collection");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			String name = StringEscapeUtils.unescapeXml(in.readContentTag("name").getContent());
			String description = StringEscapeUtils.unescapeXml(in.readContentTag("description").getContent());
			Collection.Scope scope = Collection.Scope.valueOf(in.readContentTag("scope").getContent());
			long ownerId = Long.parseLong(in.readContentTag("owner_id").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());

			result = new Collection(id, new Contributor(new Account(ownerId, "foo@foo.org", "foo", "foo", null)), name, description, scope, creationDate, editionDate);

			String datasetIdsValue = in.readNullableContentTag("dataset_ids").getContent();
			if (datasetIdsValue != null)
			{
				String[] datasetIds = datasetIdsValue.split(",");
				for (String datasetId : datasetIds)
				{
					result.datasets().add(new Dataset(Long.parseLong(datasetId), "foo"));
				}
			}
		}

		//
		in.readEndTag("collection");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readCollections(final Collections target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("collections");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("collection"))
			{
				Collection collection = readCollection(in);

				target.add(collection);
			}

			//
			in.readEndTag("collections");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static Contributor readContributor(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Contributor result;

		//
		in.readStartTag("contributor");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			String fullName = StringEscapeUtils.unescapeXml(in.readContentTag("full_name").getContent());

			//
			result = new Contributor(new Account(id, "foo@foo.org", "foo", fullName, null));

			//
			readDatasets(result.personalDatasets(), in);
			for (Dataset dataset : result.personalDatasets())
			{
				dataset.setContributor(result);
			}

			//
			readCollections(result.collections(), in);
			for (Collection collection : result.collections())
			{
				collection.setOwner(result);
			}
		}

		//
		in.readEndTag("contributor");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readContributors(final Contributors target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("contributors");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("contributor"))
			{
				Contributor contributor = readContributor(in);

				target.add(contributor);
			}

			//
			in.readEndTag("contributors");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 * @throws Exception
	 */
	public static Dataset readDataset(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Dataset result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readDataset(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static Dataset readDataset(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Dataset result;

		//
		in.readStartTag("dataset");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			String name = StringEscapeUtils.unescapeXml(in.readContentTag("name").getContent());

			result = new Dataset(id, name);

			long contributorId = Long.parseLong(in.readContentTag("contributor_id").getContent());
			result.setContributor(new Contributor(new Account(contributorId, "foo@foo.org", "foo", "foo", null)));
			result.setStatus(Dataset.Status.valueOf(in.readContentTag("status").getContent()));
			result.setType(Dataset.Type.valueOf(in.readContentTag("type").getContent()));
			result.setCreationDate(DateTime.parse(in.readContentTag("creation_date").getContent()));
			result.setEditionDate(DateTime.parse(in.readContentTag("edition_date").getContent()));
			String submissionDateValue = in.readNullableContentTag("submission_date").getContent();
			if (submissionDateValue != null)
			{
				result.setSubmissionDate(DateTime.parse(submissionDateValue));
			}
			String publicationDateValue = in.readNullableContentTag("publication_date").getContent();
			if (publicationDateValue != null)
			{
				result.setPublicationDate(DateTime.parse(publicationDateValue));
			}

			//
			// Meta data.
			result.setAtlasCode(StringEscapeUtils.unescapeXml(in.readNullableContentTag("atlas_code").getContent()));
			result.setAuthor(StringEscapeUtils.unescapeXml(in.readNullableContentTag("author").getContent()));
			result.setBibliography(StringEscapeUtils.unescapeXml(in.readNullableContentTag("bibliography").getContent()));
			result.setCitation(StringEscapeUtils.unescapeXml(in.readNullableContentTag("citation").getContent()));
			result.setCoder(StringEscapeUtils.unescapeXml(in.readNullableContentTag("coder").getContent()));
			result.setCollectionNotes(StringEscapeUtils.unescapeXml(in.readNullableContentTag("collection_notes").getContent()));
			result.setContact(StringEscapeUtils.unescapeXml(in.readNullableContentTag("contact").getContent()));
			if (in.hasNextStartTag("continent"))
			{
				result.setContinent(StringEscapeUtils.unescapeXml(in.readNullableContentTag("continent").getContent()));
			}
			result.setCountry(StringEscapeUtils.unescapeXml(in.readNullableContentTag("country").getContent()));
			result.setCoverage(StringEscapeUtils.unescapeXml(in.readNullableContentTag("coverage").getContent()));
			result.setDescription(StringEscapeUtils.unescapeXml(in.readNullableContentTag("description").getContent()));
			result.setEthnicOrCulturalGroup(StringEscapeUtils.unescapeXml(in.readNullableContentTag("ethnic_or_cultural_group").getContent()));
			result.setGeographicCoordinate(StringEscapeUtils.unescapeXml(in.readNullableContentTag("geographic_coordinate").getContent()));
			result.setHistory(StringEscapeUtils.unescapeXml(in.readNullableContentTag("history").getContent()));
			if (in.hasNextStartTag("language_group"))
			{
				result.setLanguageGroup(StringEscapeUtils.unescapeXml(in.readNullableContentTag("language_group").getContent()));
			}
			result.setLicense(StringEscapeUtils.unescapeXml(in.readNullableContentTag("license").getContent()));
			if (in.hasNextStartTag("location"))
			{
				result.setLocation(StringEscapeUtils.unescapeXml(in.readNullableContentTag("location").getContent()));
			}
			result.setOtherRepositories(StringEscapeUtils.unescapeXml(in.readNullableContentTag("other_repositories").getContent()));
			result.setPeriod(StringEscapeUtils.unescapeXml(in.readNullableContentTag("period").getContent()));
			result.setPeriodNote(StringEscapeUtils.unescapeXml(in.readNullableContentTag("period_note").getContent()));
			String radiusParameter = in.readNullableContentTag("radius_from_center").getContent();
			if (radiusParameter == null)
			{
				result.setRadiusFromCenter(null);
			}
			else
			{
				result.setRadiusFromCenter(Long.parseLong(radiusParameter));
			}
			result.setPermanentLink(StringEscapeUtils.unescapeXml(in.readNullableContentTag("permanent_link").getContent()));
			result.setReference(StringEscapeUtils.unescapeXml(in.readNullableContentTag("reference").getContent()));
			if (in.hasNextStartTag("region"))
			{
				result.setRegion(StringEscapeUtils.unescapeXml(in.readNullableContentTag("region").getContent()));
			}
			result.setShortDescription(StringEscapeUtils.unescapeXml(in.readNullableContentTag("short_description").getContent()));

			//
			result.setOriginFile(readDatasetFile(in));
			result.setPublicFile(readDatasetFile(in));
			if (in.hasNextStartTag("attribute_filters"))
			{
				readAttributeFilters(result.publicAttributeFilters(), in);
			}
			readLoadedFileHeaders(result.attachments(), in);
			readCollaborators(result.collaborators(), in);
		}

		//
		in.readEndTag("dataset");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static DatasetFile readDatasetFile(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		DatasetFile result;

		XMLTag tag = in.readNullableStartTag("dataset_file");
		//
		if (tag.getType() == TagType.EMPTY)
		{
			result = null;
		}
		else
		{
			//
			{
				LoadedFileHeader header = readLoadedFileHeader(in);
				result = new DatasetFile(header);
				result.setDownloadCount(Long.parseLong(in.readContentTag("download_count").getContent()));
				readStatistics(result.statistics(), in);
			}

			//
			in.readEndTag("dataset_file");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static void readDatasets(final Datasets target, final String source) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		readDatasets(target, in);
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readDatasets(final Datasets target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("datasets");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("dataset"))
			{
				//
				Dataset dataset = readDataset(in);

				//
				target.add(dataset);
			}

			//
			in.readEndTag("datasets");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 * @throws Exception
	 */
	public static FormatIssue readFormatIssue(final String source) throws XMLStreamException, XMLBadFormatException
	{
		FormatIssue result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readFormatIssue(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static FormatIssue readFormatIssue(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		FormatIssue result;

		//
		in.readStartTag("format_issue");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			long contributorId = Long.parseLong(in.readContentTag("contributor_id").getContent());
			long datasetId = Long.parseLong(in.readContentTag("dataset_id").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			FormatIssue.Status status = FormatIssue.Status.valueOf(in.readContentTag("status").getContent());
			LoadedFile file = readLoadedFile(in);

			//
			result = new FormatIssue(id, contributorId, datasetId, creationDate, file);
			result.setStatus(status);
		}

		//
		in.readEndTag("format_issue");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readFormatIssues(final FormatIssues target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		if (in.hasNextStartTag("format_issues"))
		{
			//
			XMLTag list = in.readListTag("format_issues");

			//
			if (list.getType() != TagType.EMPTY)
			{
				while (in.hasNextStartTag("format_issue"))
				{
					FormatIssue formatIssue = readFormatIssue(in);

					target.add(formatIssue);
				}

				//
				in.readEndTag("format_issues");
			}
		}
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static Kidarep readKidarep(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Kidarep result;

		//
		in.readStartTag("kidarep");

		//
		{
			//
			result = new Kidarep();

			//
			readContributors(result.contributors(), in);

			//
			DatasetIdIndex datasetIdIndex = new DatasetIdIndex();
			datasetIdIndex.rebuild(result.datasets());

			Collections collections = result.collections();
			for (Collection collection : collections)
			{
				for (Dataset dataset : collection.datasets().copy())
				{
					collection.datasets().remove(dataset);
					collection.datasets().add(datasetIdIndex.getById(dataset.getId()));
				}
			}

			//
			readFormatIssues(result.formatIssues(), in);
		}

		//
		in.readEndTag("kidarep");

		//
		result.resetLastDatasetId();
		result.rebuildIndexes();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static LoadedFile readLoadedFile(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		LoadedFile result;

		try
		{
			//
			in.readStartTag("loaded_file");

			//
			{
				//
				LoadedFileHeader header = readLoadedFileHeader(in);

				//
				String dataString = in.readContentTag("file_data").getContent();
				byte[] data;
				data = Hex.decodeHex(dataString.toCharArray());

				//
				result = new LoadedFile(header, data);
			}

			//
			in.readEndTag("loaded_file");

		}
		catch (DecoderException exception)
		{
			logger.error(ExceptionUtils.getStackTrace(exception));
			throw new InternalError("UNABLE TO READ LOADED FILE");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static LoadedFileHeader readLoadedFileHeader(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		LoadedFileHeader result;

		XMLTag tag = in.readNullableStartTag("loaded_file_header");
		//
		if (tag.getType() == TagType.EMPTY)
		{
			result = null;
		}
		else
		{
			{
				//
				long id = Long.parseLong(in.readContentTag("id").getContent());
				String name = StringEscapeUtils.unescapeXml(in.readContentTag("name").getContent());
				long length = Long.parseLong(in.readContentTag("length").getContent());
				String digest = in.readNullableContentTag("digest").getContent();
				DateTime loadingDate = DateTime.parse(in.readContentTag("loading_date").getContent());

				//
				result = new LoadedFileHeader(id, name, length, digest, loadingDate);
			}

			//
			in.readEndTag("loaded_file_header");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readLoadedFileHeaders(final LoadedFileHeaders target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLTag list = in.readListTag("loaded_file_headers");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("loaded_file_header"))
			{
				LoadedFileHeader loadedFile = readLoadedFileHeader(in);

				target.add(loadedFile);
			}

			//
			in.readEndTag("loaded_file_headers");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws DecoderException
	 */
	public static void readStatistics(final Statistics target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLTag list = in.readListTag("statistics");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("statistic"))
			{
				//
				in.readStartTag("statistic");

				{
					//
					String name = in.readContentTag("name").getContent();
					String type = in.readContentTag("type").getContent();
					String value = in.readContentTag("value").getContent();

					if (type.equals("Double"))
					{
						target.add(name, Double.parseDouble(value));
					}
					else if (type.equals("Integer"))
					{
						target.add(name, Integer.parseInt(value));
					}
					else if (type.equals("Long"))
					{
						target.add(name, Long.parseLong(value));
					}
					else if (type.equals("String"))
					{
						target.add(name, value);
					}
					else if (type.equals("Boolean"))
					{
						target.add(name, Boolean.parseBoolean(value));
					}
					else if (type.equals("byte[]"))
					{
						try
						{
							target.add(name, Hex.decodeHex(value.toCharArray()));
						}
						catch (DecoderException exception)
						{
							logger.error(ExceptionUtils.getStackTrace(exception));
							logger.warn("Error decoding byte[] value for key [" + name + "]");
						}
					}
					else
					{
						logger.warn("Unknown type [" + type + "]");
					}
				}

				//
				in.readEndTag("statistic");
			}

			//
			while (in.hasNextStartTag("attribute_descriptor"))
			{
				//
				in.readStartTag("attribute_descriptor");

				{
					//
					AttributeDescriptor.Scope scope = EnumUtils.getEnum(AttributeDescriptor.Scope.class, in.readContentTag("scope").getContent());
					String optionalRelationName = in.readNullableContentTag("optional_relation_name").getContent();
					String label = in.readContentTag("label").getContent();
					long countOfSet = Long.parseLong(in.readContentTag("count_of_set").getContent());
					long countOfBlank = Long.parseLong(in.readContentTag("count_of_blank").getContent());
					long max = Long.parseLong(in.readContentTag("max").getContent());

					//
					AttributeDescriptor descriptor;
					if (scope == AttributeDescriptor.Scope.RELATION)
					{
						descriptor = new AttributeDescriptor(optionalRelationName, label);
					}
					else
					{
						descriptor = new AttributeDescriptor(scope, label);
					}

					descriptor.setCountOfBlank(countOfBlank);
					descriptor.setCountOfSet(countOfSet);
					descriptor.setMax(max);

					//
					target.attributeDescriptors().add(descriptor);
				}

				//
				in.readEndTag("attribute_descriptor");
			}

			//
			if (in.hasNextStartTag("loaded_file_headers"))
			{
				readLoadedFileHeaders(target.graphics(), in);
			}

			//
			in.readEndTag("statistics");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Kidarep source, final String generator) throws Exception
	{
		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				logger.error(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Collection source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLKidarep.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Dataset source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLKidarep.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final FormatIssue source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLKidarep.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final AttributeFilter source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("attribute_filter");

			//
			{
				//
				out.writeTag("scope", source.getScope().toString());
				out.writeTag("optional_relation_name", StringEscapeUtils.escapeXml(source.getOptionalRelationName()));
				out.writeTag("label", StringEscapeUtils.escapeXml(source.getLabel()));
				out.writeTag("mode", source.getMode().toString());
			}

			//
			out.writeEndTag("attribute_filter");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final AttributeFilters source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("attribute_filters");
		}
		else
		{
			//
			out.writeStartTag("attribute_filters", "size", String.valueOf(source.size()));

			//
			{
				for (AttributeFilter filter : source)
				{
					write(out, filter);
				}
			}

			//
			out.writeEndTag("attribute_filters");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Collaborator source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("collaborator");

			//
			{
				//
				out.writeTag("account_id", source.getAccount().getId());
				out.writeTag("description", StringEscapeUtils.escapeXml(source.getDescription()));
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				write(out, source.getFile());
				write(out, source.filters());
			}

			//
			out.writeEndTag("collaborator");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Collaborators source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("collaborators");
		}
		else
		{
			//
			out.writeStartTag("collaborators", "size", String.valueOf(source.size()));

			//
			{
				for (Collaborator collaborator : source)
				{
					write(out, collaborator);
				}
			}

			//
			out.writeEndTag("collaborators");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Collection source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("collection");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("name", StringEscapeUtils.escapeXml(source.getName()));
				out.writeTag("description", StringEscapeUtils.escapeXml(source.getDescription()));
				out.writeTag("scope", source.getScope().toString());
				out.writeTag("owner_id", source.getOwner().getId());
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());

				//
				StringList datasets = new StringList(source.datasets().size());
				for (Dataset dataset : source.datasets())
				{
					datasets.append(dataset.getId());
				}
				out.writeTag("dataset_ids", datasets.toStringSeparatedBy(","));
			}

			//
			out.writeEndTag("collection");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Collections source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("collections");
		}
		else
		{
			//
			out.writeStartTag("collections", "size", String.valueOf(source.size()));

			//
			{
				for (Collection collection : source)
				{
					write(out, collection);
				}
			}

			//
			out.writeEndTag("collections");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Contributor source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("contributor");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("full_name", StringEscapeUtils.escapeXml(source.getFullName()));

				//
				write(out, source.personalDatasets());

				//
				write(out, source.collections());
			}

			//
			out.writeEndTag("contributor");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Contributors source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("contributors");
		}
		else
		{
			//
			out.writeStartTag("contributors", "size", String.valueOf(source.size()));

			//
			{
				for (Contributor contributor : source)
				{
					write(out, contributor);
				}
			}

			//
			out.writeEndTag("contributors");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Dataset source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("dataset");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("name", StringEscapeUtils.escapeXml(source.getName()));
				out.writeTag("contributor_id", source.getContributor().getId());
				out.writeTag("status", source.getStatus().toString());
				out.writeTag("type", source.getType().toString());

				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				if (source.getSubmissionDate() == null)
				{
					out.writeTag("submission_date", null);
				}
				else
				{
					out.writeTag("submission_date", source.getSubmissionDate().toString());
				}
				if (source.getPublicationDate() == null)
				{
					out.writeTag("publication_date", null);
				}
				else
				{
					out.writeTag("publication_date", source.getPublicationDate().toString());
				}

				//
				out.writeTag("atlas_code", StringEscapeUtils.escapeXml(source.getAtlasCode()));
				out.writeTag("author", StringEscapeUtils.escapeXml(source.getAuthor()));
				out.writeTag("bibliography", StringEscapeUtils.escapeXml(source.getBibliography()));
				out.writeTag("citation", StringEscapeUtils.escapeXml(source.getCitation()));
				out.writeTag("coder", StringEscapeUtils.escapeXml(source.getCoder()));
				out.writeTag("collection_notes", StringEscapeUtils.escapeXml(source.getCollectionNotes()));
				out.writeTag("contact", StringEscapeUtils.escapeXml(source.getContact()));
				out.writeTag("continent", StringEscapeUtils.escapeXml(source.getContinent()));
				out.writeTag("country", StringEscapeUtils.escapeXml(source.getCountry()));
				out.writeTag("coverage", StringEscapeUtils.escapeXml(source.getCoverage()));
				out.writeTag("description", StringEscapeUtils.escapeXml(source.getDescription()));
				out.writeTag("ethnic_or_cultural_group", StringEscapeUtils.escapeXml(source.getEthnicOrCulturalGroup()));
				out.writeTag("geographic_coordinate", StringEscapeUtils.escapeXml(source.getGeographicCoordinate()));
				out.writeTag("history", StringEscapeUtils.escapeXml(source.getHistory()));
				out.writeTag("language_group", StringEscapeUtils.escapeXml(source.getLanguageGroup()));
				out.writeTag("license", StringEscapeUtils.escapeXml(source.getLicense()));
				out.writeTag("location", StringEscapeUtils.escapeXml(source.getLocation()));
				out.writeTag("other_repositories", StringEscapeUtils.escapeXml(source.getOtherRepositories()));
				out.writeTag("period", StringEscapeUtils.escapeXml(source.getPeriod()));
				out.writeTag("period_note", StringEscapeUtils.escapeXml(source.getPeriodNote()));
				if (source.getRadiusFromCenter() == null)
				{
					out.writeTag("radius_from_center", null);
				}
				else
				{
					out.writeTag("radius_from_center", String.valueOf(source.getRadiusFromCenter()));
				}
				out.writeTag("permanent_link", StringEscapeUtils.escapeXml(source.getPermanentLink()));
				out.writeTag("reference", StringEscapeUtils.escapeXml(source.getReference()));
				out.writeTag("region", StringEscapeUtils.escapeXml(source.getRegion()));
				out.writeTag("short_description", StringEscapeUtils.escapeXml(source.getShortDescription()));

				//
				write(out, source.getOriginFile());
				write(out, source.getPublicFile());
				write(out, source.publicAttributeFilters());
				write(out, source.attachments());
				write(out, source.collaborators());
			}

			//
			out.writeEndTag("dataset");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final DatasetFile source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			out.writeEmptyTag("dataset_file");
		}
		else
		{
			//
			out.writeStartTag("dataset_file");

			//
			{
				write(out, (LoadedFileHeader) source);
				out.writeTag("download_count", source.getDownloadCount());
				write(out, source.statistics());
			}

			//
			out.writeEndTag("dataset_file");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Datasets source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("datasets");
		}
		else
		{
			//
			out.writeStartTag("datasets", "size", String.valueOf(source.size()));

			//
			{
				for (Dataset dataset : source)
				{
					write(out, dataset);
				}
			}

			//
			out.writeEndTag("datasets");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final FormatIssue source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("format_issue");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("contributor_id", source.getContributorId());
				out.writeTag("dataset_id", source.getDatasetId());
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("status", source.getStatus().toString());
				write(out, source.getFile());
			}

			//
			out.writeEndTag("format_issue");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final FormatIssues source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("format_issues");
		}
		else
		{
			//
			out.writeStartTag("format_issues", "size", String.valueOf(source.size()));

			//
			{
				for (FormatIssue formatIssue : source)
				{
					write(out, formatIssue);
				}
			}

			//
			out.writeEndTag("format_issues");
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Kidarep source)
	{
		//
		out.writeStartTag("kidarep");

		//
		write(out, source.contributors());

		//
		write(out, source.formatIssues());

		//
		out.writeEndTag("kidarep");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final LoadedFile source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("loaded_file");

			//
			{
				write(out, source.header());

				out.writeTag("file_data", Hex.encodeHexString(source.data()));
			}

			//
			out.writeEndTag("loaded_file");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final LoadedFileHeader source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			//
			out.writeEmptyTag("loaded_file_header");
		}
		else
		{
			//
			out.writeStartTag("loaded_file_header");

			//
			{
				//
				out.writeTag("id", source.id());
				out.writeTag("name", StringEscapeUtils.escapeXml(source.name()));
				out.writeTag("length", source.length());
				out.writeTag("digest", source.digest());
				out.writeTag("loading_date", source.loadingDate().toString());
			}

			//
			out.writeEndTag("loaded_file_header");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final LoadedFileHeaders source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("loaded_file_headers");
		}
		else
		{
			//
			out.writeStartTag("loaded_file_headers", "size", String.valueOf(source.size()));

			//
			{
				for (LoadedFileHeader loadedFile : source)
				{
					write(out, loadedFile);
				}
			}

			//
			out.writeEndTag("loaded_file_headers");
		}
	}

	/**
	 * Writes statistics in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Statistics source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("statistics");
		}
		else
		{
			//
			out.writeStartTag("statistics", "size", String.valueOf(source.size()));

			//
			for (String key : source.keySortedList())
			{
				//
				Object value = source.getValue(key);

				if (value != null)
				{
					//
					out.writeStartTag("statistic");

					//
					{
						if (value instanceof Double)
						{
							out.writeTag("name", key);
							out.writeTag("type", "Double");
							out.writeTag("value", value.toString());
						}
						else if (value instanceof Integer)
						{
							out.writeTag("name", key);
							out.writeTag("type", "Integer");
							out.writeTag("value", value.toString());
						}
						else if (value instanceof Long)
						{
							out.writeTag("name", key);
							out.writeTag("type", "Long");
							out.writeTag("value", String.valueOf(value));
						}
						else if (value instanceof String)
						{
							out.writeTag("name", key);
							out.writeTag("type", "String");
							out.writeTag("value", (String) value);
						}
						else if (value instanceof Boolean)
						{
							out.writeTag("name", key);
							out.writeTag("type", "Boolean");
							out.writeTag("value", String.valueOf(value));
						}
						else if (value instanceof byte[])
						{
							out.writeTag("name", key);
							out.writeTag("type", "byte[]");
							out.writeTag("value", Hex.encodeHexString((byte[]) value));
						}
						else
						{
							logger.warn("Unknow type [{}] for key named [{}]", value.getClass(), key);
							out.writeTag("name", key);
							out.writeTag("type", "String");
							out.writeTag("value", "n/a");
						}
					}

					//
					out.writeEndTag("statistic");
				}
			}

			//
			for (AttributeDescriptor descriptor : source.attributeDescriptors())
			{
				//
				out.writeStartTag("attribute_descriptor");

				//
				{
					out.writeTag("scope", descriptor.getScope().name());
					out.writeTag("optional_relation_name", descriptor.getOptionalRelationName());
					out.writeTag("label", descriptor.getLabel());
					out.writeTag("count_of_set", descriptor.getCountOfSet());
					out.writeTag("count_of_blank", descriptor.getCountOfBlank());
					out.writeTag("max", descriptor.getMax());
				}

				//
				out.writeEndTag("attribute_descriptor");
			}

			//
			write(out, source.graphics());

			//
			out.writeEndTag("statistics");
		}
	}
}
