/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.kinsources.kiwa.kidarep.FormatIssue.Status;
import org.kinsources.kiwa.kidarep.FormatIssueComparator.Criteria;

/**
 * The <code>Accounts</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class FormatIssues implements Iterable<FormatIssue>
{
	private List<FormatIssue> formatIssues;

	/**
	 * 
	 */
	public FormatIssues()
	{
		super();

		this.formatIssues = new ArrayList<FormatIssue>();
	}

	/**
	 * 
	 */
	public FormatIssues(final int initialCapacity)
	{
		this.formatIssues = new ArrayList<FormatIssue>(initialCapacity);
	}

	/**
	 * 
	 */
	public FormatIssue add(final FormatIssue source)
	{
		FormatIssue result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.formatIssues.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final FormatIssues source)
	{
		if (source != null)
		{
			for (FormatIssue formatIssue : source)
			{
				if (!this.formatIssues.contains(formatIssue))
				{
					add(formatIssue);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.formatIssues.clear();
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public boolean contains(final FormatIssue criteria)
	{
		boolean result;

		result = this.formatIssues.contains(criteria);

		//
		return result;
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public FormatIssues copy()
	{
		FormatIssues result;

		//
		result = new FormatIssues(this.formatIssues.size());

		//
		for (FormatIssue account : this.formatIssues)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public long count(final Status status)
	{
		long result;

		result = 0;
		for (FormatIssue formatIssue : this.formatIssues)
		{
			if (formatIssue.getStatus() == status)
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFormatIssues()
	{
		long result;

		result = this.formatIssues.size();

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public FormatIssues find(final Status status)
	{
		FormatIssues result;

		result = new FormatIssues();
		if (status != null)
		{
			for (FormatIssue formatIssue : this.formatIssues)
			{
				if (formatIssue.getStatus() == status)
				{
					result.add(formatIssue);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public FormatIssue getById(final long id)
	{
		FormatIssue result;

		boolean ended = false;
		int index = 0;
		result = null;
		while (!ended)
		{
			if (index < this.formatIssues.size())
			{
				FormatIssue item = this.formatIssues.get(index);

				if (item.getId() == id)
				{
					ended = true;
					result = item;
				}
				else
				{
					index += 1;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public FormatIssue getByIndex(final int index)
	{
		FormatIssue result;

		result = this.formatIssues.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.formatIssues.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<FormatIssue> iterator()
	{
		Iterator<FormatIssue> result;

		result = this.formatIssues.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		result = 0;
		for (FormatIssue formatIssue : this.formatIssues)
		{
			if (formatIssue.getId() > result)
			{
				result = formatIssue.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final FormatIssue account)
	{
		this.formatIssues.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public FormatIssues reverse()
	{
		FormatIssues result;

		//
		Collections.reverse(this.formatIssues);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.formatIssues.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public FormatIssues sortBy(final Criteria sortCriteria)
	{
		FormatIssues result;

		//
		Collections.sort(this.formatIssues, new FormatIssueComparator(sortCriteria));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public FormatIssues sortById()
	{
		FormatIssues result;

		//
		Collections.sort(this.formatIssues, new FormatIssueComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}
}
