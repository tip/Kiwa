/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class FormatIssue
{
	public enum Status
	{
		WAITING,
		READY
	}

	// Technical data.
	private long id;
	private LoadedFile file;
	private long contributorId;
	private long datasetId;
	private DateTime creationDate;
	private Status status;

	/**
	 * 
	 */
	public FormatIssue(final long id, final long contributorId, final long datasetId, final DateTime creationDate, final LoadedFile file)
	{
		this.id = id;
		this.contributorId = contributorId;
		this.datasetId = datasetId;
		this.creationDate = creationDate;
		this.status = Status.WAITING;
		this.file = file;
	}

	public long getContributorId()
	{
		return this.contributorId;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public long getDatasetId()
	{
		return this.datasetId;
	}

	public LoadedFile getFile()
	{
		return this.file;
	}

	public long getId()
	{
		return this.id;
	}

	public Status getStatus()
	{
		return this.status;
	}

	public void setContributorId(final long contributorId)
	{
		this.contributorId = contributorId;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setDatasetId(final long datasetId)
	{
		this.datasetId = datasetId;
	}

	public void setFile(final LoadedFile file)
	{
		this.file = file;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	/**
	 * 
	 */
	public void submit()
	{
		this.status = Status.READY;
	}
}
