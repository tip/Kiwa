/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * The <code>DatasetFiles</code> class represents an DatasetFile collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class DatasetFiles extends ArrayList<DatasetFile>
{
	private static final long serialVersionUID = -6746132259539406607L;

	/**
	 * 
	 */
	public DatasetFiles()
	{
		super();
	}

	/**
	 * 
	 */
	public DatasetFiles(final int initialCapacity)
	{
		super(initialCapacity);
	}

	/**
	 * 
	 */
	public LoadedFile add(final LoadedFile source)
	{
		LoadedFile result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public DatasetFiles copy()
	{
		DatasetFiles result;

		//
		result = new DatasetFiles(this.size());

		//
		for (DatasetFile item : this)
		{
			result.add(item);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public DatasetFile getByDatasetFileId(final long id)
	{
		DatasetFile result;

		boolean ended = false;
		Iterator<DatasetFile> iterator = this.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				//
				DatasetFile item = iterator.next();

				//
				if (item.id() == id)
				{
					ended = true;
					result = item;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public DatasetFile getById(final String id)
	{
		DatasetFile result;

		if (NumberUtils.isNumber(id))
		{
			result = getByDatasetFileId(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public DatasetFile getByIndex(final int index)
	{
		DatasetFile result;

		result = this.get(index);

		//
		return result;
	}
}
