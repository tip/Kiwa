/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.kidarep.LoadedFileHeaderComparator.Criteria;

/**
 * The <code>LoadedFiles</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class LoadedFileHeaders implements Iterable<LoadedFileHeader>
{
	private List<LoadedFileHeader> loaddedFiles;

	/**
	 * 
	 */
	public LoadedFileHeaders()
	{
		super();

		this.loaddedFiles = new ArrayList<LoadedFileHeader>();
	}

	/**
	 * 
	 */
	public LoadedFileHeaders(final int initialCapacity)
	{
		this.loaddedFiles = new ArrayList<LoadedFileHeader>(initialCapacity);
	}

	/**
	 * 
	 */
	public LoadedFileHeader add(final int index, final LoadedFileHeader source)
	{
		LoadedFileHeader result;

		//
		if (source != null)
		{
			this.loaddedFiles.add(index, source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public LoadedFileHeader add(final LoadedFileHeader source)
	{
		LoadedFileHeader result;

		//
		if (source != null)
		{
			this.loaddedFiles.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public void addAll(final LoadedFileHeaders source)
	{
		//
		if (source != null)
		{
			for (LoadedFileHeader file : source)
			{
				this.loaddedFiles.add(file);
			}
		}
	}

	/**
	 * 
	 */
	public LoadedFileHeader addToPosition(final int position, final LoadedFileHeader source)
	{
		LoadedFileHeader result;

		int index;
		if (position >= this.loaddedFiles.size())
		{
			index = this.loaddedFiles.size();
		}
		else
		{
			index = position - 1;
		}

		result = add(index, source);

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.loaddedFiles.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public LoadedFileHeaders copy()
	{
		LoadedFileHeaders result;

		//
		result = new LoadedFileHeaders(this.loaddedFiles.size());

		//
		for (LoadedFileHeader account : this.loaddedFiles)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfLoadedFiles()
	{
		long result;

		result = this.loaddedFiles.size();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFileHeaders findByDigest(final String digest)
	{
		LoadedFileHeaders result;

		result = new LoadedFileHeaders();

		for (LoadedFileHeader header : this)
		{
			//
			if (StringUtils.equals(header.digest(), digest))
			{
				result.add(header);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFileHeader getByDigest(final String digest)
	{
		LoadedFileHeader result;

		boolean ended = false;
		Iterator<LoadedFileHeader> iterator = this.loaddedFiles.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				//
				LoadedFileHeader file = iterator.next();

				//
				if (StringUtils.equals(file.digest(), digest))
				{
					ended = true;
					result = file;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFileHeader getById(final long id)
	{
		LoadedFileHeader result;

		boolean ended = false;
		Iterator<LoadedFileHeader> iterator = this.loaddedFiles.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				//
				LoadedFileHeader file = iterator.next();

				//
				if (file.id() == id)
				{
					ended = true;
					result = file;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFileHeader getById(final String id)
	{
		LoadedFileHeader result;

		if (NumberUtils.isNumber(id))
		{
			result = getById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFileHeader getByIndex(final int index)
	{
		LoadedFileHeader result;

		result = this.loaddedFiles.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.loaddedFiles.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<LoadedFileHeader> iterator()
	{
		Iterator<LoadedFileHeader> result;

		result = this.loaddedFiles.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final LoadedFileHeader file)
	{
		this.loaddedFiles.remove(file);
	}

	/**
	 * 
	 * @return
	 */
	public LoadedFileHeaders reverse()
	{
		LoadedFileHeaders result;

		//
		java.util.Collections.reverse(this.loaddedFiles);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.loaddedFiles.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public LoadedFileHeaders sortByName()
	{
		LoadedFileHeaders result;

		//
		java.util.Collections.sort(this.loaddedFiles, new LoadedFileHeaderComparator(Criteria.NAME));

		//
		result = this;

		//
		return result;
	}
}
