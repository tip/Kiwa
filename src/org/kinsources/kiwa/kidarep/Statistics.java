/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.kinsources.kiwa.stag.StatisticTag;
import org.tip.puck.net.workers.AttributeDescriptors;

/**
 * The <code>Statistics</code> class manages some statistic items.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Statistics
{
	private HashMap<String, Object> properties;
	private AttributeDescriptors attributeDescriptors;
	private LoadedFileHeaders graphics;

	/**
	 * 
	 */
	public Statistics()
	{
		super();

		this.properties = new HashMap<String, Object>();
		this.attributeDescriptors = new AttributeDescriptors(0);
		this.graphics = new LoadedFileHeaders();
	}

	/**
	 * 
	 */
	public Statistics(final int initialCapacity)
	{
		this.properties = new HashMap<String, Object>(initialCapacity);
		this.attributeDescriptors = new AttributeDescriptors(0);
		this.graphics = new LoadedFileHeaders();
	}

	/**
	 * 
	 */
	public void add(final String key, final Object value)
	{
		if (key == null)
		{
			throw new IllegalArgumentException("key is null");
		}
		else if (value == null)
		{
			throw new IllegalArgumentException("value is null");
		}
		else
		{
			this.properties.put(key, value);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final Properties source)
	{
		for (Object key : source.keySet())
		{
			add((String) key, source.get(key));
		}
	}

	/**
	 * 
	 * @return
	 */
	public AttributeDescriptors attributeDescriptors()
	{
		AttributeDescriptors result;

		result = this.attributeDescriptors;

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.properties.clear();
		this.attributeDescriptors.clear();
		this.graphics.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Statistics copy()
	{
		Statistics result;

		result = new Statistics(this.properties.size());

		for (String key : keys())
		{
			result.add(key, getValue(key));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Boolean getBooleanValue(final StatisticTag key)
	{
		Boolean result;

		if (key == null)
		{
			result = null;
		}
		else
		{
			result = getBooleanValue(key.name());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Boolean getBooleanValue(final String key)
	{
		Boolean result;

		Object value = this.properties.get(key);

		if (value == null)
		{
			result = null;
		}
		else if (value instanceof Boolean)
		{
			result = (Boolean) value;
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Double getDoubleValue(final StatisticTag key)
	{
		Double result;

		if (key == null)
		{
			result = null;
		}
		else
		{
			result = getDoubleValue(key.name());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Double getDoubleValue(final String key)
	{
		Double result;

		Object value = this.properties.get(key);

		if (value == null)
		{
			result = null;
		}
		else if (value instanceof Double)
		{
			result = (Double) value;
		}
		else if (value instanceof Long)
		{
			result = (double) (Long) value;
		}
		else if (value instanceof Integer)
		{
			result = (double) (Integer) value;
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Long getLongValue(final StatisticTag key)
	{
		Long result;

		if (key == null)
		{
			result = null;
		}
		else
		{
			result = getLongValue(key.name());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Long getLongValue(final String key)
	{
		Long result;

		Object value = this.properties.get(key);

		if (value == null)
		{
			result = null;
		}
		else if (value instanceof Long)
		{
			result = (Long) value;
		}
		else if (value instanceof Integer)
		{
			result = (long) ((Integer) value);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getStringValue(final StatisticTag key)
	{
		String result;

		if (key == null)
		{
			result = null;
		}
		else
		{
			result = getStringValue(key.name());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getStringValue(final String key)
	{
		String result;

		//
		Object value = this.properties.get(key);

		//
		if (value == null)
		{
			result = null;
		}
		else if (value instanceof String)
		{
			result = (String) value;
		}
		else
		{
			result = value.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Object getValue(final String key)
	{
		Object result;

		result = this.properties.get(key);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public LoadedFileHeaders graphics()
	{
		return this.graphics;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.properties.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> keyList()
	{
		List<String> result;

		result = new ArrayList<String>(this.properties.size());
		for (String key : keys())
		{
			result.add(key);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Set<String> keys()
	{
		Set<String> result;

		result = this.properties.keySet();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> keySortedList()
	{
		List<String> result;

		result = keyList();
		java.util.Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final String key)
	{

		this.properties.remove(key);
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.properties.size();

		//
		return result;
	}
}
