/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.text.Normalizer;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.stag.StatisticTag;
import org.tip.puck.net.workers.AttributeFilters;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Dataset
{
	public enum Status
	{
		NOT_CREATED,
		CREATED,
		SUBMITTED,
		VALIDATED,
		RESUBMITTED,
		REMOVED
	}

	public enum Type
	{
		GENEALOGY,
		TERMINOLOGY,
	}

	public static final int NO_ID = 0;

	// Technical data.
	private long id;
	private String name;
	private Contributor contributor;
	private Status status;
	private Type type;
	private DateTime creationDate;
	private DateTime editionDate;
	private DateTime submissionDate;
	private DateTime publicationDate;
	private DatasetFile originFile;
	private DatasetFile publicFile;
	private AttributeFilters publicAttributeFilters;
	private LoadedFileHeaders attachments;
	private Collaborators collaborators;

	// Meta data.
	private String atlasCode;
	private String author;
	private String bibliography;
	private String citation;
	private String coder;
	private String collectionNotes;
	private String contact;
	private String continent;
	private String country;
	private String coverage;
	private String description;
	private String ethnicOrCulturalGroup;
	private String geographicCoordinate;
	private String history;
	private String languageGroup;
	private String license;
	private String location;
	private String otherRepositories;
	private String period;
	private String periodNote;
	private String permanentLink;
	private Long radiusFromCenter;
	private String reference;
	private String region;
	private String shortDescription;

	/**
	 * 
	 */
	public Dataset(final long id, final String name)
	{
		this.id = id;
		this.status = Status.CREATED;
		this.type = Type.GENEALOGY;
		this.name = name;
		this.creationDate = DateTime.now();
		this.editionDate = this.creationDate;
		this.attachments = new LoadedFileHeaders();
		this.collaborators = new Collaborators();
		this.publicAttributeFilters = new AttributeFilters();
	}

	public LoadedFileHeaders attachments()
	{
		return this.attachments;
	}

	/**
	 * 
	 * @return
	 */
	public String baseFileName()
	{
		String result;

		result = "kinsources-" + simplifyFileName(this.name);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborators collaborators()
	{
		return this.collaborators;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfAttachmentFiles()
	{
		long result;

		result = this.attachments.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfCollaborators()
	{
		long result;

		result = this.collaborators.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetFiles()
	{
		long result;

		if (this.originFile == null)
		{
			result = 0;
		}
		else
		{
			result = 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFiles()
	{
		long result;

		//
		result = 0;

		//
		if (this.originFile != null)
		{
			result += 1;
		}

		//
		result += this.attachments.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfGenerations()
	{
		long result;

		Long value = getLongStat(StatisticTag.DEPTH.name());
		if (value == null)
		{
			result = 0L;
		}
		else
		{
			result = value;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfIndividuals()
	{
		long result;

		Long value = getLongStat(StatisticTag.NUM_OF_INDIVIDUALS.name());
		if (value == null)
		{
			result = 0L;
		}
		else
		{
			result = value;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfRelations()
	{
		long result;

		Long value = getLongStat(StatisticTag.NUM_OF_RELATIONS.name());
		if (value == null)
		{
			result = 0L;
		}
		else
		{
			result = value;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfTerms()
	{
		long result;

		Long value = getLongStat(StatisticTag.NUM_OF_TERMS.name());
		if (value == null)
		{
			result = 0L;
		}
		else
		{
			result = value;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfUnions()
	{
		long result;

		Long value = getLongStat(StatisticTag.NUM_OF_UNIONS.name());
		if (value == null)
		{
			result = 0L;
		}
		else
		{
			result = value;
		}

		//
		return result;
	}

	public String getAtlasCode()
	{
		return this.atlasCode;
	}

	public String getAuthor()
	{
		return this.author;
	}

	public String getBibliography()
	{
		return this.bibliography;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getBooleanStat(final String key)
	{
		Boolean result;

		if (this.publicFile != null)
		{
			result = this.publicFile.statistics().getBooleanValue(key);
		}
		else if (this.originFile != null)
		{
			result = this.originFile.statistics().getBooleanValue(key);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	public String getCitation()
	{
		return this.citation;
	}

	public String getCoder()
	{
		return this.coder;
	}

	public String getCollectionNotes()
	{
		return this.collectionNotes;
	}

	public String getContact()
	{
		return this.contact;
	}

	public String getContinent()
	{
		return this.continent;
	}

	public Contributor getContributor()
	{
		return this.contributor;
	}

	public String getCountry()
	{
		return this.country;
	}

	public String getCoverage()
	{
		return this.coverage;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public String getDescription()
	{
		return this.description;
	}

	/**
	 * 
	 * @return
	 */
	public Double getDoubleStat(final String key)
	{
		Double result;

		if (this.publicFile != null)
		{
			result = this.publicFile.statistics().getDoubleValue(key);
		}
		else if (this.originFile != null)
		{
			result = this.originFile.statistics().getDoubleValue(key);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public String getEthnicOrCulturalGroup()
	{
		return this.ethnicOrCulturalGroup;
	}

	public String getGeographicCoordinate()
	{
		return this.geographicCoordinate;
	}

	public String getHistory()
	{
		return this.history;
	}

	public long getId()
	{
		return this.id;
	}

	public String getLanguageGroup()
	{
		return this.languageGroup;
	}

	public String getLicense()
	{
		return this.license;
	}

	public String getLocation()
	{
		return this.location;
	}

	/**
	 * 
	 * @return
	 */
	public Long getLongStat(final String key)
	{
		Long result;

		if (this.publicFile != null)
		{
			result = this.publicFile.statistics().getLongValue(key);
		}
		else if (this.originFile != null)
		{
			result = this.originFile.statistics().getLongValue(key);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	public String getName()
	{
		return this.name;
	}

	public DatasetFile getOriginFile()
	{
		return this.originFile;
	}

	public String getOtherRepositories()
	{
		return this.otherRepositories;
	}

	public String getPeriod()
	{
		return this.period;
	}

	public String getPeriodNote()
	{
		return this.periodNote;
	}

	public String getPermanentLink()
	{
		return this.permanentLink;
	}

	public DateTime getPublicationDate()
	{
		return this.publicationDate;
	}

	public DatasetFile getPublicFile()
	{
		return this.publicFile;
	}

	public Long getRadiusFromCenter()
	{
		return this.radiusFromCenter;
	}

	public String getReference()
	{
		return this.reference;
	}

	public String getRegion()
	{
		return this.region;
	}

	public String getShortDescription()
	{
		return this.shortDescription;
	}

	public Status getStatus()
	{
		return this.status;
	}

	/**
	 * 
	 * @return
	 */
	public String getStringStat(final String key)
	{
		String result;

		if (this.publicFile != null)
		{
			result = this.publicFile.statistics().getStringValue(key);
		}
		else if (this.originFile != null)
		{
			result = this.originFile.statistics().getStringValue(key);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	public DateTime getSubmissionDate()
	{
		return this.submissionDate;
	}

	public Type getType()
	{
		return this.type;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasAttributesDescriptors()
	{
		boolean result;

		if (this.originFile == null)
		{
			result = false;
		}
		else if (this.originFile.statistics().attributeDescriptors().isEmpty())
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hascollaborators()
	{
		boolean result;

		result = !this.collaborators.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasMainFile()
	{
		boolean result;

		if (this.originFile == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isCollaborator(final Account account)
	{
		boolean result;

		if (account == null)
		{
			result = false;
		}
		else
		{
			if (this.collaborators().getByAccountId(account.getId()) == null)
			{
				result = false;
			}
			else
			{
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isContributor(final Account account)
	{
		boolean result;

		if ((account == null) || (this.contributor.getId() != account.getId()))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isGenealogy()
	{
		boolean result;

		if (this.type == Type.GENEALOGY)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isTerminology()
	{
		boolean result;

		if (this.type == Type.TERMINOLOGY)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	public AttributeFilters publicAttributeFilters()
	{
		return this.publicAttributeFilters;
	}

	/**
	 * 
	 * @return
	 */
	public String publicFileName()
	{
		String result;

		if (this.originFile == null)
		{
			result = null;
		}
		else
		{
			if (isGenealogy())
			{
				result = baseFileName() + ".puc";
			}
			else if (isTerminology())
			{
				result = baseFileName() + this.originFile.name().substring(this.originFile.name().lastIndexOf(".term."));
			}
			else
			{
				result = null;
			}
		}

		//
		return result;
	}

	public void setAtlasCode(final String atlasCode)
	{
		this.atlasCode = atlasCode;
	}

	public void setAuthor(final String author)
	{
		this.author = author;
	}

	public void setBibliography(final String bibliography)
	{
		this.bibliography = bibliography;
	}

	public void setCitation(final String citation)
	{
		this.citation = citation;
	}

	public void setCoder(final String coder)
	{
		this.coder = coder;
	}

	public void setCollectionNotes(final String collectionNotes)
	{
		this.collectionNotes = collectionNotes;
	}

	public void setContact(final String contact)
	{
		this.contact = contact;
	}

	public void setContinent(final String continent)
	{
		this.continent = continent;
	}

	public void setContributor(final Contributor contributor)
	{
		this.contributor = contributor;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public void setCoverage(final String coverage)
	{
		this.coverage = coverage;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	public void setEthnicOrCulturalGroup(final String ethnicOrCulturalGroup)
	{
		this.ethnicOrCulturalGroup = ethnicOrCulturalGroup;
	}

	public void setGeographicCoordinate(final String geographicCoordinate)
	{
		this.geographicCoordinate = geographicCoordinate;
	}

	public void setHistory(final String history)
	{
		this.history = history;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setLanguageGroup(final String languageGroup)
	{
		this.languageGroup = languageGroup;
	}

	public void setLicense(final String license)
	{
		this.license = license;
	}

	public void setLocation(final String location)
	{
		this.location = location;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public void setOriginFile(final DatasetFile originFile)
	{
		this.originFile = originFile;
	}

	public void setOtherRepositories(final String otherRepositories)
	{
		this.otherRepositories = otherRepositories;
	}

	public void setPeriod(final String period)
	{
		this.period = period;
	}

	public void setPeriodNote(final String periodNote)
	{
		this.periodNote = periodNote;
	}

	public void setPermanentLink(final String permanentLink)
	{
		this.permanentLink = permanentLink;
	}

	public void setPublicationDate(final DateTime publicationDate)
	{
		this.publicationDate = publicationDate;
	}

	public void setPublicFile(final DatasetFile publicFile)
	{
		this.publicFile = publicFile;
	}

	/**
	 * 
	 */
	public void setPublished()
	{
		this.status = Dataset.Status.VALIDATED;
		if (this.publicationDate == null)
		{
			this.publicationDate = DateTime.now();
		}
	}

	public void setRadiusFromCenter(final Long radiusFromCenter)
	{
		this.radiusFromCenter = radiusFromCenter;
	}

	public void setReference(final String reference)
	{
		this.reference = reference;
	}

	public void setRegion(final String region)
	{
		this.region = region;
	}

	public void setShortDescription(final String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	public void setSubmissionDate(final DateTime submissionDate)
	{
		this.submissionDate = submissionDate;
	}

	/**
	 * Note: if a previous publication date is set then do not remove it.
	 */
	public void setSubmitted()
	{
		this.status = Dataset.Status.SUBMITTED;
		this.submissionDate = DateTime.now();
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(final Type type)
	{
		if (type != null)
		{
			this.type = type;
		}
	}

	/**
	 * This method set the dataset in CREATED status.
	 * 
	 * Note: if a previous publication date is set then do not remove it.
	 */
	public void setUnpublished()
	{
		this.status = Dataset.Status.CREATED;
		this.submissionDate = null;
		this.publicFile = null;
		this.collaborators.clear();
	}

	/**
	 * 
	 * @return
	 */
	public String simplifiedName()
	{
		String result;

		result = simplifyFileName(this.name);

		if (result.equals("_"))
		{
			result = "dataset-" + this.id;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public static String simplifyFileName(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = Normalizer.normalize(source, Normalizer.Form.NFD);
			result = result.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
			result = result.toLowerCase();
			result = result.replaceAll("[^_\\w-]+", "_");
			result = result.replaceAll("_+", "_");
			if (result.startsWith("_"))
			{
				result = StringUtils.removeStart(result, "_");
			}
			if (result.endsWith("_"))
			{
				result = StringUtils.removeEnd(result, "_");
			}
		}

		//
		return result;
	}
}
