/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.util.StringList;
import fr.devinsy.util.StringListWriter;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.util.xml.XMLWriter;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class EADWriter
{
	/**
	 * 
	 * @param dataset
	 * @param websiteUrl
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toString(final Dataset dataset, final String websiteUrl) throws UnsupportedEncodingException
	{
		String result;

		//
		StringListWriter xml = new StringListWriter();
		XMLWriter out = new XMLWriter(xml);

		//
		out.writeXMLHeader();

		//
		out.writeStartTag("ead");
		{
			//
			out.writeStartTag("eadheader", "countryencoding", "iso3166-1", "dateencoding", "iso8601", "langencoding", "iso639-3", "repositoryencoding", "iso15511-adapted", "scriptencoding",
					"iso15924");
			{
				//
				out.writeTag("eadid", "Kinsources Corpus de parenté : " + dataset.getId() + ", \"" + XMLTools.escapeXmlBlank(dataset.getName()) + "\"", "mainagencycode", "FR92050UMR7186",
						"countrycode", "FR", "identifier", "FRKinsources-FR92050UMR7186-" + dataset.getId(), "encodinganalog", "identifier");

				//
				out.writeStartTag("filedesc");
				{
					//
					out.writeStartTag("titlestmt");
					{
						out.writeTag("titleproper", "Corpus de parenté " + XMLTools.escapeXmlBlank(dataset.getName()));
						out.writeTag("subtitle", XMLTools.escapeXmlBlank(StringUtils.substring(dataset.getShortDescription(), 0, 120)));
						out.writeTag("author", "Généré automatiquement par Kinsources.net d'après les informations données par " + XMLTools.escapeXmlBlank(dataset.getContributor().getFullName()));
						out.writeTag("sponsor", "Mis en ligne sur le site Kinsources.net financé par l'ANR Kinsources");
					}
					out.writeEndTag("titlestmt");

					//
					out.writeEmptyTag("editionstmt");
					out.writeStartTag("publicationstmt");
					{
						//
						out.writeTag("publisher", XMLTools.escapeXmlBlank(dataset.getContributor().getFullName()));

						//
						if (dataset.getPublicationDate() == null)
						{
							out.writeEmptyTag("date");
						}
						else
						{
							out.writeTag("date", dataset.getPublicationDate().toString("dd MMMMMMMMMM yyyy", Locale.FRENCH), "normal", dataset.getPublicationDate().toString("yyyy/MM/dd"), "era",
									"ce", "calendar", "gregorian");
						}
					}
					out.writeEndTag("publicationstmt");

					//
					out.writeStartTag("seriesstmt");
					{
						//
						out.writeTag("titleproper", "Kinsources - Collaborative web platform for kinship data share");
					}
					out.writeEndTag("seriesstmt");

					//
					if (dataset.getOtherRepositories() != null)
					{
						out.writeStartTag("seriesstmt");
						{
							out.writeTag("titleproper", XMLTools.escapeXmlBlank(dataset.getOtherRepositories()));
						}
						out.writeEndTag("seriesstmt");
					}
				}
				out.writeEndTag("filedesc");

				//
				out.writeStartTag("profiledesc");
				{
					//
					out.writeTag("creation", "Encoded in accordance with DTD EAD 2002, automatically generated on the website Kinsources.net");
					out.writeTag("langusage", "Catalog in <language langcode=\"eng\">english</language>.");
					out.writeTag("descrules", "Catalog of Kinship Datasets.");
				}
				out.writeEndTag("profiledesc");

				//
				// out.writeEmptyTag("revisiondesc");
			}
			out.writeEndTag("eadheader");

			//
			out.writeStartTag("archdesc", "level", "otherlevel", "otherlevel", "dataset");
			{
				//
				out.writeStartTag("did");
				{
					//
					out.writeTag("unitid", dataset.getId(), "type", "kinsources_identifier");
					out.writeTag("unittitle", "Dataset \"" + XMLTools.escapeXmlBlank(dataset.getName()) + "\" de " + XMLTools.escapeXmlBlank(dataset.getAuthor()));
					out.writeTag("unitdate", XMLTools.escapeXmlBlank(dataset.getPeriod()));

					//
					out.writeStartTag("scopecontent");
					{
						//
						out.writeTag("p", XMLTools.escapeXmlBlank(dataset.getShortDescription()));
						out.writeTag("p", XMLTools.escapeXmlBlank(dataset.getDescription()));

						StringList buffer = new StringList();
						buffer.append("Continent: ").append(XMLTools.escapeXmlBlank(dataset.getContinent())).append(", ");
						buffer.append("Region: ").append(XMLTools.escapeXmlBlank(dataset.getRegion())).append(", ");
						buffer.append("Country: ").append(XMLTools.escapeXmlBlank(dataset.getCountry())).append(", ");
						buffer.append("Location: ").append(XMLTools.escapeXmlBlank(dataset.getLocation())).append(", ");
						buffer.append("Geographic coordinate: ").append(XMLTools.escapeXmlBlank(dataset.getGeographicCoordinate())).append(", ");
						String radius;
						if (dataset.getRadiusFromCenter() == null)
						{
							radius = "";
						}
						else
						{
							radius = String.valueOf(dataset.getRadiusFromCenter());
						}
						buffer.append("Radius from center: ").append(radius);
						out.writeTag("p", buffer.toString());

						buffer = new StringList();
						buffer.append("Ethnic or cultural group: ").append(XMLTools.escapeXmlBlank(dataset.getEthnicOrCulturalGroup())).append(", ");
						buffer.append("Atlas code: ").append(XMLTools.escapeXmlBlank(dataset.getAtlasCode()));
						out.writeTag("p", buffer.toString());

						out.writeTag("p", "Dataset history: " + StringUtils.defaultString(dataset.getHistory()));
						out.writeTag("p", "Reference: " + StringUtils.defaultString(dataset.getReference()));
					}
					out.writeEndTag("scopecontent");

					out.writeTag("accessrestrict", "License: " + XMLTools.escapeXmlBlank(dataset.getLicense()));
					out.writeTag("altformavail", "Other repository: " + XMLTools.escapeXmlBlank(dataset.getOtherRepositories()));

					//
					out.writeStartTag("relatedmaterial");
					{
						//
						for (LoadedFileHeader attachment : dataset.attachments())
						{
							//
							String url = SimpleServletDispatcher.rewriteLongUrl(websiteUrl + "kidarep/dataset_attachment", String.valueOf(dataset.getId()), String.valueOf(attachment.id()),
									attachment.name());

							out.writeTag("archref", attachment.name(), "href", url);
						}
					}
					out.writeEndTag("relatedmaterial");

					//
					out.writeStartTag("bibliography");
					{
						out.writeTag("head", "citation");
						out.writeTag("bibref", XMLTools.escapeXmlBlank(dataset.getCitation()));
					}
					out.writeEndTag("bibliography");

					//
					out.writeStartTag("bibliography");
					{
						out.writeTag("head", "Bibliography");
						out.writeTag("bibref", XMLTools.escapeXmlBlank(dataset.getBibliography()));
					}
					out.writeEndTag("bibliography");
				}
				out.writeEndTag("did");
			}
			out.writeEndTag("archdesc");
		}
		out.writeEndTag("ead");

		//
		result = xml.toString();

		//
		return result;
	}
}
