/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.exceptions.KiwaBadFileFormatException;
import org.kinsources.kiwa.kernel.exceptions.KiwaControlException;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.workers.RelationModelReporter;
import org.tip.puck.net.workers.AttributeFilter;
import org.tip.puck.net.workers.AttributeFilters;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.ControlReporter.ControlType;
import org.tip.puck.report.Report;

import fr.devinsy.util.StringList;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Kidarep
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Kidarep.class);
	private Contributors contributors;
	private ContributorIdIndex contributorIdIndex;
	private CollectionIdIndex collectionIdIndex;
	private DatasetIdIndex datasetIdIndex;
	private long lastCollectionId;
	private long lastDatasetId;
	private long lastFileId;
	private long lastFormatIssueId;
	private FormatIssues formatIssues;

	/**
	 * 
	 */
	public Kidarep()
	{
		//
		this.contributors = new Contributors();
		this.contributorIdIndex = new ContributorIdIndex();

		//
		this.lastDatasetId = 0;
		this.datasetIdIndex = new DatasetIdIndex();

		//
		this.lastCollectionId = 0;
		this.collectionIdIndex = new CollectionIdIndex();

		//
		this.lastFormatIssueId = 0;
		this.formatIssues = new FormatIssues();
	}

	/**
	 * 
	 * @param dataset
	 * @param name
	 * @param data
	 * @return
	 */
	public LoadedFileHeader addAttachment(final Dataset dataset, final String name, final byte[] data)
	{
		LoadedFileHeader result;

		if ((dataset.getStatus() != Dataset.Status.CREATED) && (dataset.getStatus() != Dataset.Status.SUBMITTED))
		{
			throw new IllegalArgumentException("Invalid dataset status.");
		}
		else if (!StringUtils.endsWithAny(name, ".clu", ".csv", ".ged", ".gif", ".jpg", ".ods", ".net", ".paj", ".pdf", ".png", ".txt", ".xls"))
		{
			throw new IllegalArgumentException("Bad format file. Only clu, csv, ged, gif, jpg, ods, net, paj, pdf, png, txt and xls are available.");
		}
		else if (dataset.attachments().getByDigest(LoadedFileHeader.digest(data)) != null)
		{
			throw new IllegalArgumentException("Attachment already loaded.");
		}
		else
		{
			result = new LoadedFileHeader(nextFileId(), name, data);
			dataset.attachments().add(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param name
	 * @param originData
	 * @return
	 * @throws PuckException
	 * @throws KiwaControlException
	 * @throws IOException
	 * @throws KiwaBadFileFormatException
	 */
	public void addDatasetGenealogyFile(final Dataset dataset, final LoadedFile originFile, final List<ControlType> sourceControls, final Locale locale) throws KiwaControlException, IOException,
			KiwaBadFileFormatException
	{
		try
		{
			//
			Net net = Kiwa.loadNet(originFile);

			//
			if (net.individuals().isEmpty())
			{
				logger.info("Update genealogy file without any individuals.");
				throw new KiwaBadFileFormatException("None individual.");
			}
			else
			{
				//
				List<ControlType> controls;
				if (sourceControls == null)
				{
					controls = new ArrayList<ControlType>();
				}
				else
				{
					controls = sourceControls;
				}

				// Add mandatory controls.
				if (!controls.contains(ControlType.AUTO_MARRIAGE))
				{
					controls.add(ControlType.AUTO_MARRIAGE);
				}

				if (!controls.contains(ControlType.CYCLIC_DESCENT_CASES))
				{
					controls.add(ControlType.CYCLIC_DESCENT_CASES);
				}

				// Control.
				Report report = ControlReporter.controlNet(net, locale, controls);

				//
				if (report.status() == 0)
				{
					//
					DatasetFile targetFile = new DatasetFile(nextFileId(), originFile.header());
					dataset.setOriginFile(targetFile);
				}
				else
				{
					//
					StringList buffer = new StringList();
					for (Object output : report.outputs())
					{
						buffer.append(output.toString());
					}

					throw new KiwaControlException(buffer.toString());
				}
			}
		}
		catch (PuckException exception)
		{
			switch (PuckExceptions.valueOf(exception.getCode()))
			{
				case INVALID_PARAMETER:
					throw new IllegalArgumentException(exception.getMessage());

				case BAD_FILE_FORMAT:
				case FORMAT_CONFLICT:
				case UNSUPPORTED_ENCODING:
				case UNSUPPORTED_FILE_FORMAT:
					throw new KiwaBadFileFormatException(exception.getMessage());

				case FILE_NOT_FOUND:
				case IO_ERROR:
				case NOT_A_FILE:
					throw new IOException(exception.getMessage());

				default:
					throw new IllegalArgumentException(exception.getMessage());
			}
		}
	}

	/**
	 * 
	 * @param dataset
	 * @param originFile
	 * @param controls
	 * @param locale
	 * @throws KiwaControlException
	 * @throws IOException
	 * @throws KiwaBadFileFormatException
	 */
	public void addDatasetTerminologyFile(final Dataset dataset, final LoadedFile originFile, final List<RelationModelReporter.ControlType> controls, final Locale locale) throws KiwaControlException,
			IOException, KiwaBadFileFormatException
	{
		try
		{
			//
			RelationModel model = Kiwa.loadTerminology(originFile);

			logger.debug("role count         ={}", model.roles().size());
			logger.debug("roleRelations count={}", model.getRoleRelations().size());

			//
			if ((model.roles().isEmpty()) && (model.getRoleRelations().isEmpty()))
			{
				logger.info("Update terminology file seems to be empty.");
				throw new KiwaBadFileFormatException("Seems be empty terminology.");
			}
			else
			{
				// Control.
				Report report = RelationModelReporter.reportControls(model, controls);

				//
				if (report.status() == 0)
				{
					//
					DatasetFile targetFile = new DatasetFile(nextFileId(), originFile.header());
					dataset.setOriginFile(targetFile);
				}
				else
				{
					//
					StringList buffer = new StringList();
					for (Object output : report.outputs())
					{
						buffer.append(output.toString());
					}

					throw new KiwaControlException(buffer.toString());
				}
			}
		}
		catch (PuckException exception)
		{
			switch (PuckExceptions.valueOf(exception.getCode()))
			{
				case INVALID_PARAMETER:
					throw new IllegalArgumentException(exception.getMessage());

				case BAD_FILE_FORMAT:
				case FORMAT_CONFLICT:
				case UNSUPPORTED_ENCODING:
				case UNSUPPORTED_FILE_FORMAT:
					throw new KiwaBadFileFormatException(exception.getMessage());

				case FILE_NOT_FOUND:
				case IO_ERROR:
				case NOT_A_FILE:
					throw new IOException(exception.getMessage());

				default:
					throw new IllegalArgumentException(exception.getMessage());
			}
		}
	}

	/**
	 * 
	 * @param dataset
	 * @param fileName
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public LoadedFile addPublicDatasetFile(final Dataset dataset, final LoadedFile originFile) throws PuckException
	{
		LoadedFile result;

		if (dataset.isGenealogy())
		{
			//
			Net net = Kiwa.loadNet(originFile);

			//
			if (dataset.publicAttributeFilters().isNotEmpty())
			{
				//
				long attributeFiltredCount = AttributeWorker.filter(net, dataset.publicAttributeFilters());

				//
				String filterString = dataset.publicAttributeFilters().toString();
				if (StringUtils.isNotBlank(filterString))
				{
					net.attributes().add(new Attribute("Kinsources_filter", filterString));
					net.attributes().add(new Attribute("Kinsources_filter_count", String.valueOf(attributeFiltredCount)));
				}
			}

			//
			Kiwa.instance().putPermanentLinkAttribute(net, dataset);

			//
			LoadedFile targetFile = Kiwa.saveNet(dataset.publicFileName(), net);

			//
			DatasetFile datasetFile = new DatasetFile(nextFileId(), targetFile.header());
			dataset.setPublicFile(datasetFile);

			result = new LoadedFile(datasetFile, targetFile.data());
		}
		else if (dataset.isTerminology())
		{
			//
			LoadedFile targetFile = new LoadedFile(dataset.publicFileName(), Arrays.copyOf(originFile.data(), originFile.data().length));

			//
			DatasetFile datasetFile = new DatasetFile(nextFileId(), targetFile.header());
			dataset.setPublicFile(datasetFile);

			result = new LoadedFile(datasetFile, targetFile.data());
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		//
		this.contributors.clear();
		this.contributorIdIndex.clear();

		//
		this.lastDatasetId = 0;
		this.datasetIdIndex.clear();

		//
		this.lastCollectionId = 0;
		this.collectionIdIndex.clear();

		//
		this.lastFileId = 0;
	}

	/**
	 * 
	 * @return
	 */
	public Collections collections()
	{
		Collections result;

		//
		result = this.contributors.collections();

		//
		return result;
	}

	public Contributors contributors()
	{
		return this.contributors;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public Collection copyCollection(final Collection source, final Contributor target)
	{
		Collection result;

		result = createCollection(target, source.getName() + "-copy", source.getDescription(), Collection.Scope.PRIVATE);

		result.datasets().addAll(source.datasets());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfAttachmentFiles()
	{
		long result;

		result = this.contributors.countOfAttachmentFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfCollaborators()
	{
		long result;

		result = 0;
		for (Dataset dataset : findDatasets(Dataset.Status.VALIDATED))
		{
			result += dataset.collaborators().size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfCollections()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfCollections();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfContributors()
	{
		long result;

		result = this.contributors.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetFiles()
	{
		long result;

		result = this.contributors.countOfDatasetFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasets()
	{
		long result;

		result = this.contributors.countOfDatasets();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFiles()
	{
		long result;

		result = this.contributors.countOfFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFormatIssues()
	{
		long result;

		result = this.formatIssues.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfGenealogies()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfGenealogies();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfIndividuals()
	{
		long result;

		//
		Datasets datasets = datasetsFromIndex();

		//
		result = 0;
		for (Dataset dataset : datasets)
		{
			result += dataset.countOfIndividuals();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfPublishedDatasetFiles()
	{
		long result;

		result = this.contributors.countOfPublishedDatasetFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfPublishedDatasets()
	{
		long result;

		result = this.contributors.countOfPublishedDatasets();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfReadyFormatIssues()
	{
		long result;

		result = this.formatIssues.count(FormatIssue.Status.READY);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfRelations()
	{
		long result;

		//
		Datasets datasets = datasetsFromIndex();

		//
		result = 0;
		for (Dataset dataset : datasets)
		{
			result += dataset.countOfRelations();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfTerminologies()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfTerminologies();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfUnions()
	{
		long result;

		//
		Datasets datasets = datasetsFromIndex();

		//
		result = 0;
		for (Dataset dataset : datasets)
		{
			result += dataset.countOfUnions();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param name
	 * @param originData
	 * @return
	 * @throws PuckException
	 * @throws KiwaControlException
	 * @throws IOException
	 */
	public LoadedFile createCollaborator(final Dataset dataset, final Account collaboratorAccount, final String description, final AttributeFilters filters, final LoadedFile sourceFile)
			throws PuckException
	{
		LoadedFile result;

		// Build collaborator file
		// =======================
		//
		Net net = Kiwa.loadNet(sourceFile);

		//
		if (filters != null)
		{
			//
			long attributeFiltredCount = AttributeWorker.filter(net, filters);

			//
			String filterString = filters.toString();
			if (StringUtils.isNotBlank(filterString))
			{
				net.attributes().add(new Attribute("Kinsources_filter", filterString));
				net.attributes().add(new Attribute("Kinsources_filter_count", String.valueOf(attributeFiltredCount)));
			}
		}

		//
		Kiwa.instance().putPermanentLinkAttribute(net, dataset);

		//
		LoadedFile targetFile = Kiwa.saveNet(dataset.baseFileName() + ".puc", net);

		//
		DatasetFile file = new DatasetFile(nextFileId(), targetFile.header());

		// Create collaborator
		// ===================
		Collaborator collaborator = new Collaborator(collaboratorAccount, description, filters);
		collaborator.setFile(file);
		dataset.collaborators().add(collaborator);

		//
		result = new LoadedFile(file, targetFile.data());

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	synchronized public Collection createCollection(final Contributor contributor, final String name, final String description, final Collection.Scope scope)
	{
		Collection result;

		//
		result = new Collection(nextCollectionId(), contributor, name, description, scope);

		//
		contributor.collections().add(result);

		//
		this.collectionIdIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	synchronized public Contributor createContributor(final Account source)
	{
		Contributor result;

		//
		result = new Contributor(source);

		//
		this.contributors.add(result);

		//
		this.contributorIdIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	synchronized public Dataset createDataset(final Contributor contributor, final String name)
	{
		Dataset result;

		//
		if (isAvailableDatasetName(name, (Dataset) null))
		{
			//
			result = new Dataset(nextDatasetId(), name);
			result.setContributor(contributor);

			//
			contributor.personalDatasets().add(result);

			//
			this.datasetIdIndex.put(result);
		}
		else
		{
			throw new IllegalArgumentException("Name already in use.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	synchronized public FormatIssue createFormatIssue(final Contributor contributor, final Dataset dataset, final LoadedFile file)
	{
		FormatIssue result;

		//
		result = new FormatIssue(nextFormatIssueId(), contributor.getId(), dataset.getId(), DateTime.now(), file);

		//
		this.formatIssues.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @param contributorId
	 * @param datasetId
	 * @param file
	 * @return
	 */
	public FormatIssue createFormatIssue(final long contributorId, final long datasetId, final LoadedFile file)
	{
		FormatIssue result;

		//
		result = new FormatIssue(nextFormatIssueId(), contributorId, datasetId, DateTime.now(), file);

		//
		this.formatIssues.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets datasets()
	{
		Datasets result;

		//
		result = this.contributors.datasets();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets datasetsFromIndex()
	{
		Datasets result;

		//
		result = this.datasetIdIndex.datasets();

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Collaborations findCollaborationsByCollaborator(final Account account)
	{
		Collaborations result;

		//
		result = new Collaborations();

		//
		if (account != null)
		{
			for (Dataset dataset : this.datasets().copy())
			{
				Collaborator collaborator = dataset.collaborators().getByAccountId(account.getId());

				if (dataset.isCollaborator(account))
				{
					Collaboration collaboration = new Collaboration(dataset.getContributor(), dataset, collaborator);
					result.add(collaboration);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Contributors findContributors(final Account.Status status)
	{
		Contributors result;

		result = this.contributors.findContributors(status);

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findDatasets(final Dataset.Status status)
	{
		Datasets result;

		result = new Datasets();
		if (status != null)
		{
			for (Contributor contributor : this.contributors)
			{
				for (Dataset dataset : contributor.personalDatasets())
				{
					if (dataset.getStatus() == status)
					{
						result.add(dataset);
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findDatasetsByCollaborator(final Account account)
	{
		Datasets result;

		//
		result = new Datasets();

		//
		if (account != null)
		{
			for (Dataset dataset : this.datasets().copy())
			{
				if (dataset.isCollaborator(account))
				{
					result.add(dataset);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findDatasetsWithCollaborator()
	{
		Datasets result;

		//
		result = new Datasets();

		//
		for (Dataset dataset : this.datasets().copy())
		{
			if (dataset.hascollaborators())
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Datasets findPublishedDatasets()
	{
		Datasets result;

		//
		result = new Datasets();

		//
		Datasets source = datasetsFromIndex();

		//
		for (Dataset dataset : source)
		{
			if (dataset.getStatus() == Dataset.Status.VALIDATED)
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	public FormatIssues formatIssues()
	{
		return this.formatIssues;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Collection getCollectionById(final long id)
	{
		Collection result;

		result = this.collectionIdIndex.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	synchronized public Contributor getContributorById(final long id)
	{
		Contributor result;

		result = this.contributorIdIndex.getById(id);
		if (result == null)
		{
			Account account = Kiwa.instance().accountManager().getAccountById(id);
			if (account == null)
			{
				result = null;
			}
			else
			{
				result = this.contributors.add(new Contributor(account));
				this.contributorIdIndex.put(result);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	synchronized public Contributor getContributorById(final Long id)
	{
		Contributor result;

		if (id == null)
		{
			result = null;
		}
		else
		{
			result = getContributorById((long) id);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	synchronized public Contributor getContributorById(final String id)
	{
		Contributor result;

		if (NumberUtils.isNumber(id))
		{
			result = getContributorById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Dataset getDatasetById(final long id)
	{
		Dataset result;

		result = this.datasetIdIndex.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Dataset getDatasetById(final String id)
	{
		Dataset result;

		if (NumberUtils.isDigits(id))
		{
			result = getDatasetById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public FormatIssue getFormatIssueById(final long id)
	{
		FormatIssue result;

		result = this.formatIssues.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public FormatIssue getFormatIssueById(final String id)
	{
		FormatIssue result;

		if (NumberUtils.isNumber(id))
		{
			result = getFormatIssueById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param target
	 *            dataset to wich the name is expected.
	 * @return
	 */
	public boolean isAvailableDatasetName(final String name, final Dataset target)
	{
		boolean result;

		if (StringUtils.isBlank(name))
		{
			result = false;
		}
		else
		{
			Datasets datasets = datasets();

			String technicalName = Dataset.simplifyFileName(name);

			boolean ended = false;
			Iterator<Dataset> iterator = datasets.iterator();
			result = false;
			while (!ended)
			{
				//
				if (iterator.hasNext())
				{
					//
					Dataset dataset = iterator.next();

					//
					if ((dataset.simplifiedName().equals(technicalName)) && ((target == null) || (dataset.getId() != target.getId())))
					{
						ended = true;
						result = false;
					}
				}
				else
				{
					ended = true;
					result = true;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param target
	 *            dataset to wich the name is expected.
	 * @return
	 */
	public boolean isNotAvailableDatasetName(final String name, final Dataset target)
	{
		boolean result;

		result = !isAvailableDatasetName(name, target);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastCollectionId()
	{
		long result;

		result = this.lastCollectionId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastContibutorId()
	{
		long result;

		result = Kiwa.instance().accountManager().lastAccountId();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastDatasetId()
	{
		long result;

		result = this.lastDatasetId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastFileId()
	{
		long result;

		result = this.lastFileId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastFormatIssueId()
	{
		long result;

		result = this.lastFormatIssueId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextCollectionId()
	{
		long result;

		this.lastCollectionId += 1;

		result = this.lastCollectionId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextDatasetId()
	{
		long result;

		this.lastDatasetId += 1;

		result = this.lastDatasetId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextFileId()
	{
		long result;

		this.lastFileId += 1;

		result = this.lastFileId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextFormatIssueId()
	{
		long result;

		this.lastFormatIssueId += 1;

		result = this.lastFormatIssueId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void purgeFormatIssues(final int days)
	{
		//
		DateTime limit = DateTime.now().minusDays(days);

		//
		for (FormatIssue formatIssue : this.formatIssues.find(FormatIssue.Status.WAITING))
		{
			if (formatIssue.getCreationDate().isBefore(limit))
			{
				this.formatIssues.remove(formatIssue);
			}
		}
	}

	/**
	 * 
	 */
	public void rebuildCollectionIndex()
	{
		this.collectionIdIndex.rebuild(this.contributors.collections());
	}

	/**
	 * 
	 */
	public void rebuildContributorIndex()
	{
		this.contributorIdIndex.rebuild(this.contributors);
	}

	/**
	 * 
	 */
	public void rebuildDatasetIndex()
	{
		this.datasetIdIndex.rebuild(this.contributors.datasets());
	}

	/**
	 * 
	 */
	public void rebuildIndexes()
	{
		rebuildContributorIndex();
		rebuildCollectionIndex();
		rebuildDatasetIndex();
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeCollection(final Collection source)
	{
		//
		source.getOwner().collections().remove(source);

		//
		this.collectionIdIndex.remove(source);
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeDataset(final Dataset source)
	{
		//
		source.getContributor().personalDatasets().remove(source);

		//
		this.datasetIdIndex.remove(source);

		//
		Collections collections = source.getContributor().findCollectionsWith(source);
		for (Collection collection : collections)
		{
			removeDatasetFromCollection(source, collection);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeDatasetFile(final Dataset dataset) throws Exception
	{
		//
		dataset.setOriginFile(null);
		dataset.publicAttributeFilters().clear();
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeDatasetFromCollection(final Dataset source, final Collection collection)
	{

		if (source == null)
		{
			throw new IllegalArgumentException("dataset is null.");
		}
		else if (collection == null)
		{
			throw new IllegalArgumentException("collection is null.");
		}
		else
		{
			collection.datasets().remove(source);
		}
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeFormatIssue(final FormatIssue source)
	{
		//
		this.formatIssues.remove(source);
	}

	/**
	 * 
	 */
	public void resetLastCollectionId()
	{
		this.lastCollectionId = this.contributors.lastCollectionId() + 1;
	}

	/**
	 * 
	 */
	public void resetLastDatasetId()
	{
		this.lastDatasetId = this.contributors.lastPersonalDatasetId() + 1;
	}

	/**
	 * 
	 */
	public void resetLastFileId(final long value)
	{
		this.lastFileId = value;
	}

	/**
	 * 
	 */
	public void resetLastFormatIssueId()
	{
		this.lastFormatIssueId = this.formatIssues.lastId() + 1;
	}

	/**
	 * 
	 */
	public void resetLastFormatIssueId(final long value)
	{
		this.lastFormatIssueId = value;
	}

	/**
	 * 
	 * @param topic
	 */
	public void transferDataset(final Contributor newContributor, final Dataset source)
	{
		source.getContributor().personalDatasets().remove(source);
		newContributor.personalDatasets().add(source);
		source.setContributor(newContributor);
	}

	/**
	 * 
	 * @param collaborator
	 * @param description
	 * @param filters
	 * @param sourceFile
	 * @return
	 * @throws PuckException
	 */
	public LoadedFile updateCollaborator(final Dataset dataset, final Collaborator collaborator, final String description, final AttributeFilters filters, final LoadedFile sourceFile)
			throws PuckException
	{
		LoadedFile result;

		// Build collaborator file
		// =======================
		//
		Net net = Kiwa.loadNet(sourceFile);

		//
		if (filters != null)
		{
			//
			long attributeFiltredCount = AttributeWorker.filter(net, filters);

			//
			String filterString = filters.toString();
			if (StringUtils.isNotBlank(filterString))
			{
				net.attributes().add(new Attribute("Kinsources_filter", filterString));
				net.attributes().add(new Attribute("Kinsources_filter_count", String.valueOf(attributeFiltredCount)));
			}
		}

		//
		Kiwa.instance().putPermanentLinkAttribute(net, dataset);

		//
		LoadedFile targetFile = Kiwa.saveNet(dataset.baseFileName() + ".puc", net);

		//
		DatasetFile file = new DatasetFile(nextFileId(), targetFile.header());

		// Update collaborator
		// ===================
		collaborator.setDescription(description);
		collaborator.setFile(file);
		collaborator.filters().clear();
		for (AttributeFilter filter : filters)
		{
			collaborator.filters().add(filter);
		}
		collaborator.touch();

		//
		result = new LoadedFile(file, targetFile.data());

		//
		return result;
	}
}
