/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.eucles;

import org.joda.time.DateTime;
import org.kinsources.kiwa.utils.MailTools;

/**
 * 
 * @author Christian P. Momon
 */
public class Eucles
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Eucles.class);
	private int currentIndex;
	private Mails mails;
	private long pauseTime; // In millisecond.
	private EuclesTimer timer;

	/**
	 * 
	 */
	public Eucles()
	{
		this.currentIndex = 0;
		this.pauseTime = 15000;
		this.mails = new Mails();
		this.timer = new EuclesTimer(this);
		this.timer.start();
	}

	/**
	 * 
	 */
	public Eucles(final long pauseTime)
	{
		this.currentIndex = 0;
		this.pauseTime = pauseTime;
		this.mails = new Mails();
		this.timer = new EuclesTimer(this);
		this.timer.start();
	}

	/**
	 * 
	 * @return
	 */
	public long countOfMails()
	{
		long result;

		result = this.mails.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfWaitingMails()
	{
		long result;

		result = this.mails.size() - this.currentIndex;

		//
		return result;
	}

	public int getCurrentIndex()
	{
		return this.currentIndex;
	}

	public long getPauseTime()
	{
		return this.pauseTime;
	}

	/**
	 * 
	 */
	public void killTimer()
	{
		this.timer.interrupt();
	}

	/**
	 * 
	 * @return
	 */
	public Mails mails()
	{
		return this.mails;
	}

	/*
	 * 
	 */
	synchronized public void sendMail(final String to, final String from, final String replyTo, final String cc, final String bcc, final String subject, final CharSequence htmlMessage)
	{
		//
		Mail mail = new Mail(this.mails.size(), to, from, replyTo, cc, bcc, subject, htmlMessage.toString());

		//
		this.mails.add(mail);
	}

	/**
	 * 
	 */
	synchronized public void sendNextMail()
	{
		//
		if (this.currentIndex < this.mails.size())
		{
			//
			Mail mail = this.mails.getByIndex(this.currentIndex);
			this.currentIndex += 1;

			if (mail.getStatus() == Mail.Status.WAITING)
			{
				mail.setDispatchDate(DateTime.now());

				//
				boolean statusOk = MailTools.sendSLLHTMLEmail(mail.getTo(), mail.getFrom(), mail.getReplyTo(), mail.getCc(), mail.getBcc(), mail.getSubject(), mail.getHtmlMessage());

				if (statusOk)
				{
					mail.setStatus(Mail.Status.SENT);
				}
				else
				{
					mail.setStatus(Mail.Status.ERROR);
				}
			}
		}
	}

	public void setCurrentIndex(final int currentIndex)
	{
		this.currentIndex = currentIndex;
	}

	public void setPauseTime(final long pauseTime)
	{
		this.pauseTime = pauseTime;
	}
}
