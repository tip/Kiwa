/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.eucles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Mails implements Iterable<Mail>
{
	private ArrayList<Mail> mails;

	/**
	 * 
	 */
	public Mails()
	{
		this.mails = new ArrayList<Mail>();
	}

	/**
	 * 
	 */
	public Mails(final int initialCapacity)
	{
		this.mails = new ArrayList<Mail>(initialCapacity);
	}

	/**
	 * 
	 * @param eventLog
	 */
	public void add(final Mail mail)
	{
		//
		if (mail == null)
		{
			throw new IllegalArgumentException("article is null.");
		}
		else if (StringUtils.isBlank(mail.getTo()))
		{
			throw new IllegalArgumentException("to is blank.");
		}
		else
		{
			this.mails.add(mail);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.mails.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Mails copy()
	{
		Mails result;

		//
		result = new Mails(this.mails.size());

		//
		for (Mail article : this.mails)
		{
			result.add(article);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Mail first()
	{
		Mail result;

		if (this.mails.isEmpty())
		{
			result = null;
		}
		else
		{
			result = this.mails.get(0);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Mails first(final int targetCount)
	{
		Mails result;

		//
		result = new Mails(targetCount);

		//
		boolean ended = false;
		Iterator<Mail> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Mail getByIndex(final int index)
	{
		Mail result;

		result = this.mails.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.mails.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Mail> iterator()
	{
		Iterator<Mail> result;

		result = this.mails.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Mail article : this.mails)
		{
			if (article.getId() > result)
			{
				result = article.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Mail wire)
	{
		this.mails.remove(wire);
	}

	/**
	 * 
	 * @return
	 */
	public Mails reverse()
	{
		Mails result;

		//
		Collections.reverse(this.mails);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.mails.size();

		//
		return result;
	}
}
