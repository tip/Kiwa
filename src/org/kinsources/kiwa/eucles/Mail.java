/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.eucles;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Mail
{
	public enum Status
	{
		WAITING,
		SENT,
		ERROR
	};

	private long id;
	private String to;
	private String from;
	private String replyTo;
	private String cc;
	private String bcc;
	private String subject;
	private CharSequence htmlMessage;
	private DateTime creationDate;
	private DateTime dispatchDate;
	private Status status;

	/**
	 * 
	 * @param category
	 * @param event
	 * @param comment
	 */
	public Mail(final long id, final String to, final String from, final String replyTo, final String cc, final String bcc, final String subject, final String htmlMessage)
	{
		//
		if (StringUtils.isBlank(to))
		{
			throw new IllegalArgumentException("to is blank");
		}
		else if (StringUtils.isBlank(htmlMessage))
		{
			throw new IllegalArgumentException("htmlMessage is blank");
		}
		else
		{
			this.id = id;
			this.to = to;
			this.from = from;
			this.replyTo = replyTo;
			this.cc = cc;
			this.bcc = bcc;
			this.subject = subject;
			this.htmlMessage = htmlMessage;
			this.creationDate = DateTime.now();
			this.dispatchDate = null;
			this.status = Status.WAITING;
		}
	}

	public String getBcc()
	{
		return this.bcc;
	}

	public String getCc()
	{
		return this.cc;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public DateTime getDispatchDate()
	{
		return this.dispatchDate;
	}

	public String getFrom()
	{
		return this.from;
	}

	public CharSequence getHtmlMessage()
	{
		return this.htmlMessage;
	}

	public long getId()
	{
		return this.id;
	}

	public String getReplyTo()
	{
		return this.replyTo;
	}

	public Status getStatus()
	{
		return this.status;
	}

	public String getSubject()
	{
		return this.subject;
	}

	public String getTo()
	{
		return this.to;
	}

	public void setBcc(final String bcc)
	{
		this.bcc = bcc;
	}

	public void setCc(final String cc)
	{
		this.cc = cc;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setDispatchDate(final DateTime dispatchDate)
	{
		this.dispatchDate = dispatchDate;
	}

	public void setFrom(final String from)
	{
		this.from = from;
	}

	public void setHtmlMessage(final CharSequence htmlMessage)
	{
		this.htmlMessage = htmlMessage;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setReplyTo(final String replyTo)
	{
		this.replyTo = replyTo;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	public void setSubject(final String subject)
	{
		this.subject = subject;
	}

	public void setTo(final String to)
	{
		this.to = to;
	}
}
