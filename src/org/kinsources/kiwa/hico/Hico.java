/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.hico;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.xidyn.data.TagDataManager;

/**
 * 
 * @author Christian P. Momon
 */
public class Hico
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Hico.class);
	public static final String BUBBLE_ARTICLE_NAME = "Bubbles";
	private long lastId;
	private Articles articles;
	private ArticleIdIndex articleIdIndex;
	private ArticleNameLocaleIndex articleNameLocaleIndex;
	private Locale[] locales = { Locale.ENGLISH, Locale.FRENCH };
	private BubbleManager bubbleManager;

	/**
	 * 
	 */
	public Hico()
	{
		this.lastId = 0;
		this.articles = new Articles();
		this.articleIdIndex = new ArticleIdIndex();
		this.articleNameLocaleIndex = new ArticleNameLocaleIndex();
		this.bubbleManager = new BubbleManager();
	}

	/**
	 * 
	 */
	public Hico(final long autoIncrementValue)
	{
		this.lastId = autoIncrementValue;
		this.articles = new Articles();
		this.articleIdIndex = new ArticleIdIndex();
		this.articleNameLocaleIndex = new ArticleNameLocaleIndex();
		this.bubbleManager = new BubbleManager();
	}

	/**
	 * 
	 * @return
	 */
	public Articles articles()
	{
		return this.articles;
	}

	/**
	 * 
	 * @return
	 */
	public BubbleManager bubbleManager()
	{
		return this.bubbleManager;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfArticles()
	{
		long result;

		result = this.articles.size();

		//
		return result;
	}

	/**
	 * 
	 */
	public Article createArticle(final String name, final Locale locale)
	{
		Article result;

		result = new Article(nextId(), name, locale);
		this.articles.add(result);
		this.articleIdIndex.put(result);
		this.articleNameLocaleIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles findByName(final String name)
	{
		Articles result;

		result = this.articles.findByName(name);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles findByNameIgnoreCase(final String name)
	{
		Articles result;

		result = this.articles.findByNameIgnoreCase(name);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param bubbleName
	 */
	public void generateBubble(final TagDataManager target, final String bubbleName, final Locale locale)
	{
		//
		String html = getHTMLIconBubble(bubbleName, locale);

		//
		if (StringUtils.isNotBlank(html))
		{
			//
			target.setContent(bubbleName, html);
		}
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Article getArticleById(final long articleId)
	{
		Article result;

		result = this.articleIdIndex.getById(articleId);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Article getArticleById(final Long articleId)
	{
		Article result;

		if (articleId == null)
		{
			result = null;
		}
		else
		{
			result = this.articleIdIndex.getById(articleId);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 *            name of the bubble
	 * @param locale
	 * @return
	 */
	public String getBubbleText(final String name, final Locale locale)
	{
		String result;

		//
		Bubble bubble = this.bubbleManager.getBubble(name, locale);

		//
		if (bubble == null)
		{
			result = null;
		}
		else
		{
			result = bubble.getText();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param bubbleName
	 */
	public String getHTMLIconBubble(final String bubbleName, final Locale locale)
	{
		String result;

		//
		String text = getBubbleText(bubbleName, locale);

		//
		if (StringUtils.isBlank(text))
		{
			result = null;
		}
		else
		{
			result = "<span class=\"bubble_icon\"><img src=\"/hico/question_mark.png\" alt=\"help\" /><span>" + text + " </span></span>";
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param locale
	 * @return
	 */
	public String getText(final String name, final Locale locale)
	{
		String result;

		//
		Article article = getVisibleArticle(name, locale);

		//
		if (article == null)
		{
			result = "Coming soon.";
		}
		else
		{
			result = article.getText();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param locale
	 * @return
	 */
	public Article getVisibleArticle(final String name, final Locale locale)
	{
		Article result;

		//
		Article article = this.articleNameLocaleIndex.get(name, locale);

		//
		if ((article == null) || (!article.isVisible()))
		{
			article = this.articleNameLocaleIndex.get(name, Locale.ENGLISH);
		}

		//
		if ((article == null) || (!article.isVisible()))
		{
			result = null;
		}
		else
		{
			result = article;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastArticleId()
	{
		long result;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Locale[] locales()
	{
		Locale[] result;

		result = this.locales;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextId()
	{
		long result;

		this.lastId += 1;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void rebuildIndexes()
	{
		this.articleIdIndex.rebuild(this.articles);
		this.articleNameLocaleIndex.rebuild(this.articles);
	}

	/**
	 * 
	 * @param account
	 */
	public void removeArticle(final Article article)
	{
		//
		this.articles.remove(article);

		//
		this.articleIdIndex.remove(article);
		this.articleNameLocaleIndex.remove(article);
	}

	/**
	 * 
	 */
	public void resetBubbles()
	{
		//
		Articles target = this.articles.findByName(BUBBLE_ARTICLE_NAME);

		//
		this.bubbleManager.reset(target);
	}

	/**
	 * 
	 */
	public void resetLastId()
	{
		this.lastId = this.articles.lastId();
	}

	/**
	 * 
	 * @param wire
	 * @param title
	 * @param author
	 * @param publicationDate
	 * @param locale
	 * @param leadParagraph
	 * @param body
	 */
	public void updateArticle(final Article article, final String name, final String author, final boolean visibility, final Locale locale, final String text)
	{

		logger.debug("[name={}][author={}][visibility={}][locale={}][text={}]", name, author, visibility, locale, text);

		if (article == null)
		{
			throw new IllegalArgumentException("article is null.");
		}
		else
		{
			//
			this.articleNameLocaleIndex.remove(article);

			//
			article.setName(name);
			article.setAuthor(author);
			article.setVisible(visibility);
			article.setLocale(locale);
			article.setText(text);
			article.touch();

			//
			this.articleNameLocaleIndex.put(article);
		}
	}
}
