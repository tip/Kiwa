/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.hico;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;

/**
 * 
 * @author Christian P. Momon
 */
public class BubbleManager
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BubbleManager.class);

	private Bubbles bubbles;
	private int lastId;
	private BubbleNameLocaleIndex bubbleNameLocaleIndex;

	/**
	 * 
	 */
	public BubbleManager()
	{
		this.bubbles = new Bubbles();
		this.lastId = 0;
		this.bubbleNameLocaleIndex = new BubbleNameLocaleIndex();
	}

	/**
	 * 
	 * @return
	 */
	public Bubbles bubbles()
	{
		Bubbles result;

		result = this.bubbles;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfBubbles()
	{
		long result;

		result = this.bubbles.size();

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param locale
	 * @return
	 */
	public Bubble getBubble(final String name, final Locale locale)
	{
		Bubble result;

		result = this.bubbleNameLocaleIndex.get(name, locale);

		if (result == null)
		{
			result = this.bubbleNameLocaleIndex.get(name, Locale.ENGLISH);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 *            name of the bubble
	 * @param locale
	 * @return
	 */
	public String getBubbleText(final String name, final Locale locale)
	{
		String result;

		//
		Bubble bubble = getBubble(name, locale);

		//
		if (bubble == null)
		{
			result = null;
		}
		else
		{
			result = bubble.getText();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void importBubble(final Article source)
	{
		if (source != null)
		{
			//
			String text = source.getText();

			boolean ended = false;
			MutableInt currentPosition = new MutableInt(0);
			while (!ended)
			{
				//
				Bubble bubble = getTRData(text, currentPosition);

				//
				if (bubble == null)
				{
					ended = true;
				}
				else
				{
					//
					bubble.setId(nextId());
					bubble.setEditionDate(source.getEditionDate());
					bubble.setLocale(source.getLocale());

					//
					this.bubbles.add(bubble);
				}
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public long lastBubbleId()
	{
		long result;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextId()
	{
		long result;

		this.lastId += 1;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void reset(final Articles source)
	{
		//
		this.bubbles.clear();

		//
		if (source != null)
		{
			for (Article article : source)
			{
				importBubble(article);
			}
		}

		//
		this.bubbleNameLocaleIndex.rebuild(this.bubbles);
	}

	/**
	 * 
	 * @param source
	 * @param position
	 * @return
	 */
	public static String getTDContent(final String source, final MutableInt position)
	{
		String result;

		if (StringUtils.isBlank(source))
		{
			result = null;
		}
		else
		{
			int begin = source.indexOf("<td>", position.intValue());

			if (begin == -1)
			{
				result = null;
			}
			else
			{
				begin += 4;
				int end = source.indexOf("</td>", begin + 4);
				if (end == -1)
				{
					throw new IllegalArgumentException("</td> is missing");
				}
				else
				{
					result = source.substring(begin, end);
					position.setValue(end + 5);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param position
	 * @return
	 */
	public static Bubble getTRData(final String source, final MutableInt position)
	{
		Bubble result;

		if (StringUtils.isBlank(source))
		{
			result = null;
		}
		else
		{
			String label = getTDContent(source, position);
			if (label == null)
			{
				result = null;
			}
			else
			{
				String value = getTDContent(source, position);

				if (value == null)
				{
					throw new IllegalArgumentException("value is missing");
				}
				else
				{
					result = new Bubble(label, value);
				}
			}
		}

		//
		return result;
	}
}
