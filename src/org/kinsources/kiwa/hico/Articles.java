/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.hico;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Articles implements Iterable<Article>
{
	private ArrayList<Article> articles;

	/**
	 * 
	 */
	public Articles()
	{
		this.articles = new ArrayList<Article>();
	}

	/**
	 * 
	 */
	public Articles(final int initialCapacity)
	{
		this.articles = new ArrayList<Article>(initialCapacity);
	}

	/**
	 * 
	 * @param eventLog
	 */
	public void add(final Article article)
	{
		//
		if (article == null)
		{
			throw new IllegalArgumentException("article is null.");
		}
		else if (article.getName() == null)
		{
			throw new IllegalArgumentException("name is null.");
		}
		else
		{
			this.articles.add(article);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.articles.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Articles copy()
	{
		Articles result;

		//
		result = new Articles(this.articles.size());

		//
		for (Article article : this.articles)
		{
			result.add(article);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles findByLocale(final Locale locale)
	{
		Articles result;

		//
		result = new Articles(this.articles.size());

		if (locale != null)
		{
			for (Article article : this.articles)
			{
				if (StringUtils.equals(locale.getLanguage(), article.getLocale().getLanguage()))
				{
					result.add(article);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles findByName(final String name)
	{
		Articles result;

		//
		result = new Articles(this.articles.size());

		//
		for (Article article : this.articles)
		{
			if (StringUtils.equals(name, article.getName()))
			{
				result.add(article);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles findByNameIgnoreCase(final String name)
	{
		Articles result;

		//
		result = new Articles(this.articles.size());

		//
		for (Article article : this.articles)
		{
			if (StringUtils.equalsIgnoreCase(name, article.getName()))
			{
				result.add(article);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles findVisibles()
	{
		Articles result;

		//
		result = new Articles(this.articles.size());

		//
		for (Article article : this.articles)
		{
			if (article.isVisible())
			{
				result.add(article);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Article first()
	{
		Article result;

		if (this.articles.isEmpty())
		{
			result = null;
		}
		else
		{
			result = this.articles.get(0);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Articles first(final int targetCount)
	{
		Articles result;

		//
		result = new Articles(targetCount);

		//
		boolean ended = false;
		Iterator<Article> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Article first(final Locale locale)
	{
		Article result;

		boolean ended = false;
		int index = 0;
		result = null;
		while (!ended)
		{
			if (index >= this.articles.size())
			{
				ended = true;
				result = null;
			}
			else
			{
				Article article = this.articles.get(index);
				if (StringUtils.equals(article.getLocale().getLanguage(), locale.getLanguage()))
				{
					ended = true;
					result = article;
				}
				else
				{
					index += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Article getByIndex(final int index)
	{
		Article result;

		result = this.articles.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.articles.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Article> iterator()
	{
		Iterator<Article> result;

		result = this.articles.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Article article : this.articles)
		{
			if (article.getId() > result)
			{
				result = article.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Article wire)
	{
		this.articles.remove(wire);
	}

	/**
	 * 
	 * @return
	 */
	public Articles reverse()
	{
		Articles result;

		//
		Collections.reverse(this.articles);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.articles.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles sortByEditionDate()
	{
		Articles result;

		//
		Collections.sort(this.articles, new ArticleComparator(ArticleComparator.Criteria.EDITIONDATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles sortById()
	{
		Articles result;

		//
		Collections.sort(this.articles, new ArticleComparator(ArticleComparator.Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Articles sortByName()
	{
		Articles result;

		//
		Collections.sort(this.articles, new ArticleComparator(ArticleComparator.Criteria.NAME));

		//
		result = this;

		//
		return result;
	}
}
