/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.accounts.Account.Status;

/**
 * 
 * @author Christian P. Momon
 */
public class AccountManager implements java.io.Serializable
{
	private static final long serialVersionUID = 5663361841723858096L;
	private Accounts accounts;
	private long lastAccountId;
	private AccountIdIndex accountIdIndex;
	private AccountEmailIndex accountEmailIndex;
	private Roles roles;

	/**
	 * 
	 */
	public AccountManager()
	{
		this.accounts = new Accounts();
		this.lastAccountId = 0;
		this.accountIdIndex = new AccountIdIndex();
		this.accountEmailIndex = new AccountEmailIndex();
		this.roles = new Roles();
	}

	/**
	 * 
	 * @return
	 */
	public Accounts accounts()
	{
		Accounts result;

		result = this.accounts;

		//
		return result;
	}

	/**
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public Account authenticate(final String login, final String password)
	{
		Account result;

		result = this.accountEmailIndex.getByEmail(login);
		if ((result != null) && ((result.isNotActivated()) || (!result.password().check(password))))
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.accounts.clear();
		this.accountIdIndex.clear();
		this.accountEmailIndex.clear();
		this.roles.clear();
	}

	/**
	 * 
	 * @return
	 */
	public long countOfAccounts()
	{
		long result;

		result = this.accounts.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfActivatedAccounts()
	{
		long result;

		result = this.accounts.count(Account.Status.ACTIVATED);

		//
		return result;
	}

	/**
	 * 
	 * @param fullName
	 * @param email
	 * @param password
	 * @return
	 */
	synchronized public Account createAccount(final String firstName, final String lastName, final String email, final String password)
	{
		Account result;

		if (StringUtils.isBlank(firstName))
		{
			throw new IllegalArgumentException("firstName is null.");
		}
		else if (StringUtils.isBlank(lastName))
		{
			throw new IllegalArgumentException("lastName is null.");
		}
		else if (StringUtils.isBlank(email))
		{
			throw new IllegalArgumentException("email is null.");
		}
		else if (StringUtils.isBlank(password))
		{
			throw new IllegalArgumentException("password is null.");
		}
		else
		{
			result = new Account(nextAccountId(), email, firstName, lastName, password);
			this.accounts.add(result);
			this.accountIdIndex.put(result);
			this.accountEmailIndex.put(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param roleId
	 * @return
	 */
	public Role createRole(final int roleId, final String name)
	{
		Role result;

		if (name == null)
		{
			throw new IllegalArgumentException("name is null.");
		}
		else if (roleId < 0)
		{
			throw new IndexOutOfBoundsException("negative id");
		}
		else
		{
			if (this.roles.getById(roleId) == null)
			{
				result = this.roles.setRole(new Role(roleId, name));
			}
			else
			{
				throw new IllegalArgumentException("id already in use");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	public Role createRole(final String name)
	{
		Role result;

		if (name == null)
		{
			throw new IllegalArgumentException("name is null.");
		}
		else
		{
			result = createRole(this.roles.nextFreeId(), name);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Accounts findAccountByStatus(final Account.Status status)
	{
		Accounts result;

		result = this.accounts.findAccountByStatus(status);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Accounts findAccountsByRole(final Role role)
	{
		Accounts result;

		result = this.accounts.findAccountsByRole(role);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Account getAccountByEmail(final String email)
	{
		Account result;

		result = this.accountEmailIndex.getByEmail(email);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Account getAccountById(final long userId)
	{
		Account result;

		result = this.accountIdIndex.getById(userId);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Account getAccountById(final Long userId)
	{
		Account result;

		if (userId == null)
		{
			result = null;
		}
		else
		{
			result = this.accountIdIndex.getById(userId);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Account getAccountById(final String id)
	{
		Account result;

		if (NumberUtils.isNumber(id))
		{
			result = getAccountById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Role getRoleById(final int roleId)
	{
		Role result;

		result = this.roles.getById(roleId);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public boolean isActivationPending(final String email)
	{
		boolean result;

		Account account = getAccountByEmail(email);
		if (account == null)
		{
			result = false;
		}
		else if (account.getStatus() == Account.Status.CREATED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastAccountId()
	{
		return this.lastAccountId;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextAccountId()
	{
		long result;

		this.lastAccountId += 1;
		result = this.lastAccountId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void rebuildIndexes()
	{
		this.accountIdIndex.rebuild(this.accounts);
		this.accountEmailIndex.rebuild(this.accounts);
	}

	/**
	 * 
	 * @param account
	 */
	public void removeAccount(final Account account)
	{
		account.setStatus(Status.REMOVED);
	}

	/**
	 * 
	 * @param id
	 */
	public void removeRole(final int roleId)
	{
		this.roles.unsetRole(roleId);
	}

	/**
	 * 
	 * @param role
	 */
	public void removeRole(final Role role)
	{
		if (role == null)
		{
			throw new IllegalArgumentException("role is null");
		}
		else
		{
			this.removeRole(role.getId());
		}
	}

	/**
	 * 
	 */
	public void resetLastId()
	{
		this.lastAccountId = this.accounts.lastId();
	}

	/**
	 * 
	 * @return
	 */
	public Roles roles()
	{
		Roles result;

		result = this.roles;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void updateIndexes(final Account source)
	{
		this.accountEmailIndex.update(source);
	}
}
