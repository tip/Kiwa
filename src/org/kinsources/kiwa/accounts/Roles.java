/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Roles implements Iterable<Role>
{
	private ArrayList<Role> roles;

	/**
	 *
	 */
	public Roles()
	{
		this.roles = new ArrayList<Role>(0);
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.roles.clear();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean contains(final int id)
	{
		boolean result;

		if (getById(id) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Role getById(final int roleId)
	{
		Role result;

		if (roleId < 0)
		{
			throw new IllegalArgumentException("negative role id.");
		}
		else if (roleId >= this.roles.size())
		{
			result = null;
		}
		else
		{
			result = this.roles.get(roleId);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public Role getByName(final String name)
	{
		Role result;

		if (StringUtils.isBlank(name))
		{
			result = null;
		}
		else
		{
			boolean ended = false;
			int index = 0;
			result = null;
			while (!ended)
			{
				if (index < this.roles.size())
				{
					Role role = this.roles.get(index);
					if ((role != null) && (StringUtils.equals(role.getName(), name)))
					{
						result = role;
						ended = true;
					}
					else
					{
						index += 1;
					}
				}
				else
				{
					result = null;
					ended = true;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.roles.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Role> iterator()
	{
		Iterator<Role> result;

		result = toList().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int nextFreeId()
	{
		int result;

		boolean ended = false;
		int roleId = 0;
		result = 0;
		while (!ended)
		{
			if (roleId < this.roles.size())
			{
				if (this.roles.get(roleId) == null)
				{
					ended = true;
					result = roleId;
				}
				else
				{
					roleId += 1;
				}
			}
			else
			{
				ended = true;
				result = roleId;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param role
	 */
	public Role setRole(final Role role)
	{
		Role result;

		if (role == null)
		{
			throw new IllegalArgumentException("null parameter");
		}
		else
		{
			//
			this.roles.ensureCapacity(role.getId() + 1);
			while (this.roles.size() <= role.getId())
			{
				this.roles.add(null);
			}

			//
			this.roles.set(role.getId(), role);
		}

		//
		result = role;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = 0;
		Iterator<Role> iterator = iterator();
		while (iterator.hasNext())
		{
			Role role = iterator.next();
			if (role != null)
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Roles sortedList()
	{
		Roles result;

		//
		java.util.Collections.sort(toList(), new RoleComparator(RoleComparator.Criteria.NAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Role> toList()
	{
		List<Role> result;

		result = new ArrayList<Role>();

		for (Role role : this.roles)
		{
			if (role != null)
			{
				result.add(role);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param role
	 */
	public void unsetRole(final int roleId)
	{

		if (roleId < 0)
		{
			throw new IllegalArgumentException("negative id");
		}
		else if (roleId > this.roles.size())
		{
			throw new IllegalArgumentException("Unknown role");
		}
		else if (this.roles.get(roleId) == null)
		{
			throw new IllegalArgumentException("Unknown role");
		}
		else
		{
			this.roles.set(roleId, null);
		}
	}

	/**
	 * 
	 * @param role
	 */
	public void unsetRole(final Role role)
	{
		if (role == null)
		{
			throw new IllegalArgumentException("role is null");
		}
		else
		{
			unsetRole(role.getId());
		}
	}
}
