/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.accounts.AccountComparator.Criteria;

/**
 * The <code>Accounts</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Accounts implements Iterable<Account>
{
	private List<Account> accounts;

	/**
	 * 
	 */
	public Accounts()
	{
		super();

		this.accounts = new ArrayList<Account>();
	}

	/**
	 * 
	 */
	public Accounts(final int initialCapacity)
	{
		this.accounts = new ArrayList<Account>(initialCapacity);
	}

	/**
	 * 
	 */
	public Account add(final Account source)
	{
		Account result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else if (this.accounts.contains(source))
		{
			throw new IllegalArgumentException("source is already in accounts");
		}
		else
		{
			this.accounts.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.accounts.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Accounts copy()
	{
		Accounts result;

		//
		result = new Accounts(this.accounts.size());

		//
		for (Account account : this.accounts)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public int count(final Status status)
	{
		int result;

		result = 0;
		for (Account account : this.accounts)
		{
			if (account.getStatus() == status)
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Accounts findAccountByStatus(final Account.Status status)
	{
		Accounts result;

		result = new Accounts();

		if (status != null)
		{
			for (Account account : this.accounts)
			{
				if (account.getStatus() == status)
				{
					result.add(account);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Accounts findAccountsByRole(final Role role)
	{
		Accounts result;

		result = new Accounts();
		for (Account account : this)
		{
			if (account.isRole(role))
			{
				result.add(account);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Account getById(final long id)
	{
		Account result;

		//
		boolean ended = false;
		int index = 0;
		result = null;
		while (!ended)
		{
			if (index < this.accounts.size())
			{
				//
				Account account = getByIndex(index);

				//
				if (account.getId() == id)
				{
					ended = true;
					result = account;
				}
				else
				{
					index += 1;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Account getByIndex(final int index)
	{
		Account result;

		result = this.accounts.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.accounts.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Account> iterator()
	{
		Iterator<Account> result;

		result = this.accounts.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Account account : this.accounts)
		{
			if (account.getId() > result)
			{
				result = account.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Account account)
	{
		this.accounts.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public Accounts reverse()
	{
		Accounts result;

		//
		Collections.reverse(this.accounts);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.accounts.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Accounts sortByCreationDate()
	{
		Accounts result;

		//
		Collections.sort(this.accounts, new AccountComparator(Criteria.CREATION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Accounts sortByFullName()
	{
		Accounts result;

		//
		Collections.sort(this.accounts, new AccountComparator(Criteria.FULLNAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Accounts sortByFullNameReversed()
	{
		Accounts result;

		//
		Collections.sort(this.accounts, new AccountComparator(Criteria.FULLNAME_REVERSED));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Accounts sortById()
	{
		Accounts result;

		//
		Collections.sort(this.accounts, new AccountComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Accounts sortByLastConnectionDate()
	{
		Accounts result;

		//
		Collections.sort(this.accounts, new AccountComparator(Criteria.LASTCONNECTION));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Accounts sortByLastName()
	{
		Accounts result;

		//
		Collections.sort(this.accounts, new AccountComparator(Criteria.LASTNAME));

		//
		result = this;

		//
		return result;
	}
}
