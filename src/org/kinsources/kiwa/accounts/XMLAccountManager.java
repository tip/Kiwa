/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.kinsources.kiwa.accounts.Account.EmailScope;
import org.kinsources.kiwa.accounts.Account.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a AccountManager File reader and writer.
 * 
 * @author TIP
 */
public class XMLAccountManager
{
	private static final Logger logger = LoggerFactory.getLogger(XMLAccountManager.class);

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static AccountManager load(final File file) throws Exception
	{
		AccountManager result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readAccountManager(in);

		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Account readAccount(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Account result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readAccount(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Account readAccount(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Account result;

		//
		XMLTag tag = in.readStartTag("account");

		//
		{
			//
			long id = Long.parseLong(tag.attributes().getByLabel("id").getValue());
			String email = StringEscapeUtils.unescapeXml(in.readContentTag("email").getContent());
			String firstName = StringEscapeUtils.unescapeXml(in.readNullableContentTag("first_names").getContent());
			String lastName = StringEscapeUtils.unescapeXml(in.readNullableContentTag("last_name").getContent());

			result = new Account(id, email, firstName, lastName, null);

			result.setStatus(Status.valueOf(in.readContentTag("status").getContent()));
			result.setEmailScope(EmailScope.valueOf(in.readContentTag("email_scope").getContent()));
			result.setTimeZone(DateTimeZone.forID(StringEscapeUtils.unescapeXml(in.readContentTag("timezone_id").getContent())));
			result.setCreationDate(DateTime.parse(in.readContentTag("creation_date").getContent()));
			result.setEditionDate(DateTime.parse(in.readContentTag("edition_date").getContent()));
			String lastConnection = in.readNullableContentTag("last_connection").getContent();
			if (lastConnection == null)
			{
				result.setLastConnection(null);
			}
			else
			{
				result.setLastConnection(DateTime.parse(lastConnection));
			}
			result.setBusinessCard(StringEscapeUtils.unescapeXml(in.readNullableContentTag("business_card").getContent()));
			result.setCountry(StringEscapeUtils.unescapeXml(in.readNullableContentTag("country").getContent()));
			result.setEmailNotification(Boolean.parseBoolean(in.readContentTag("email_notification").getContent()));
			result.setOrganization(StringEscapeUtils.unescapeXml(in.readNullableContentTag("organization").getContent()));
			result.setWebsite(StringEscapeUtils.unescapeXml(in.readNullableContentTag("website").getContent()));

			Password password = readPassword(in);
			result.setPassword(password.digest(), password.editionDate());

			readRoles(result.roles(), in);
			readAttributes(result.attributes(), in);
		}

		//
		in.readEndTag("account");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static AccountManager readAccountManager(final String source) throws XMLStreamException, XMLBadFormatException
	{
		AccountManager result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readAccountManager(in);

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static AccountManager readAccountManager(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		AccountManager result;

		//
		result = new AccountManager();

		//
		in.readStartTag("account_manager");

		//
		{
			//
			readRoles(result.roles(), in);

			//
			readAccounts(result.accounts(), in);
		}

		//
		in.readEndTag("account_manager");

		//
		result.resetLastId();
		result.rebuildIndexes();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readAccounts(final Accounts target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("accounts");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("account"))
			{
				//
				Account account = readAccount(in);

				//
				target.add(account);
			}

			//
			in.readEndTag("accounts");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readAttributes(final Attributes target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("attributes");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("attribute"))
			{
				//
				in.readStartTag("attribute");

				//
				{
					//
					String label = StringEscapeUtils.unescapeXml(in.readContentTag("label").getContent());

					//
					String value = StringEscapeUtils.unescapeXml(in.readNullableContentTag("value").getContent());
					if (value == null)
					{
						value = "";
					}

					//
					target.put(label, value);
				}

				//
				in.readEndTag("attribute");
			}

			//
			in.readEndTag("attributes");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Password readPassword(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Password result;

		//
		in.readStartTag("password");

		//
		{
			//
			String digest = StringEscapeUtils.unescapeXml(in.readNullableContentTag("digest").getContent());

			//
			DateTime editionDate = DateTime.parse(in.readContentTag("editionDate").getContent());

			//
			result = new Password(digest, editionDate);
		}

		//
		in.readEndTag("password");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static void readRoles(final Roles target, final String source) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		readRoles(target, in);
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readRoles(final Roles target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("roles");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("role"))
			{
				//
				in.readStartTag("role");

				//
				{
					//
					int id = Integer.parseInt(in.readContentTag("id").getContent());

					//
					String name = StringEscapeUtils.unescapeXml(in.readContentTag("name").getContent());

					//
					target.setRole(new Role(id, name));
				}

				//
				in.readEndTag("role");
			}

			//
			in.readEndTag("roles");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final AccountManager source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				logger.warn(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Account source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLAccountManager.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final AccountManager source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLAccountManager.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Roles source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLAccountManager.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Account source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("account is null.");
		}
		else
		{
			//
			out.writeStartTag("account", "id", String.valueOf(source.getId()));

			//
			{
				out.writeTag("email", StringEscapeUtils.escapeXml(source.getEmail()));
				out.writeTag("first_names", StringEscapeUtils.escapeXml(source.getFirstNames()));
				out.writeTag("last_name", StringEscapeUtils.escapeXml(source.getLastName()));
				out.writeTag("status", source.getStatus().toString());
				out.writeTag("email_scope", source.getEmailScope().toString());
				out.writeTag("timezone_id", StringEscapeUtils.escapeXml(source.getTimeZone().getID()));
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				if (source.getLastConnection() == null)
				{
					out.writeTag("last_connection", null);
				}
				else
				{
					out.writeTag("last_connection", source.getLastConnection().toString());
				}
				out.writeTag("business_card", StringEscapeUtils.escapeXml(source.getBusinessCard()));
				out.writeTag("country", StringEscapeUtils.escapeXml(source.getCountry()));
				out.writeTag("email_notification", String.valueOf(source.isEmailNotification()));
				out.writeTag("organization", StringEscapeUtils.escapeXml(source.getOrganization()));
				out.writeTag("website", StringEscapeUtils.escapeXml(source.getWebsite()));
				write(out, source.password());
				write(out, source.roles());
				write(out, source.attributes());
			}

			//
			out.writeEndTag("account");
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final AccountManager source)
	{
		//
		out.writeStartTag("account_manager");

		//
		write(out, source.roles());

		//
		write(out, source.accounts());

		//
		out.writeEndTag("account_manager");
	}

	/**
	 * 
	 * @param out
	 * @param source
	 */
	public static void write(final XMLWriter out, final Accounts source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("accounts");
		}
		else
		{
			//
			out.writeStartTag("accounts", "size", String.valueOf(source.size()));

			for (Account account : source)
			{
				write(out, account);
			}

			out.writeEndTag("accounts");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Attributes source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("attributes");
		}
		else
		{
			//
			out.writeStartTag("attributes", "size", String.valueOf(source.size()));

			//
			for (Attribute attribute : source)
			{
				//
				out.writeStartTag("attribute");
				{
					out.writeTag("label", StringEscapeUtils.escapeXml(attribute.getLabel()));
					out.writeTag("value", StringEscapeUtils.escapeXml(attribute.getValue()));
				}

				//
				out.writeEndTag("attribute");
			}

			//
			out.writeEndTag("attributes");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Password source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("password is null.");
		}
		else
		{
			//
			out.writeStartTag("password");

			//
			{
				out.writeTag("digest", StringEscapeUtils.escapeXml(source.digest()));
				out.writeTag("editionDate", source.editionDate().toString());
			}

			//
			out.writeEndTag("password");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Roles source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			out.writeEmptyTag("roles");
		}
		else
		{
			List<Role> roles = source.toList();
			if (roles.isEmpty())
			{
				out.writeEmptyTag("roles");
			}
			else
			{
				//
				out.writeStartTag("roles", "size", String.valueOf(roles.size()));

				//
				for (Role role : roles)
				{
					//
					out.writeStartTag("role");
					{
						//
						out.writeTag("id", role.getId());

						//
						out.writeTag("name", StringEscapeUtils.escapeXml(role.getName()));
					}

					//
					out.writeEndTag("role");
				}

				out.writeEndTag("roles");
			}
		}
	}
}
