/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.sciboard.RequestComparator.Criteria;

/**
 * The <code>Accounts</code> class represents an request collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Requests implements Iterable<Request>
{
	private List<Request> requests;

	/**
	 * 
	 */
	public Requests()
	{
		super();

		this.requests = new ArrayList<Request>();
	}

	/**
	 * 
	 */
	public Requests(final int initialCapacity)
	{
		this.requests = new ArrayList<Request>(initialCapacity);
	}

	/**
	 * 
	 */
	public Request add(final Request source)
	{
		Request result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else if (this.requests.contains(source))
		{
			throw new IllegalArgumentException("source is already in requests");
		}
		else
		{
			this.requests.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.requests.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Requests copy()
	{
		Requests result;

		//
		result = new Requests(this.requests.size());

		//
		for (Request request : this.requests)
		{
			result.add(request);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public int count(final Request.Status status)
	{
		int result;

		result = 0;
		for (Request request : this.requests)
		{
			if (request.getStatus() == status)
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Requests findByContributorId(final long id)
	{
		Requests result;

		result = new Requests();
		for (Request request : this.requests)
		{
			if (request.getContributorId() == id)
			{
				result.add(request);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Requests findByProperty(final String label, final long value)
	{
		Requests result;

		//
		result = findByProperty(label, String.valueOf(value));

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Requests findByProperty(final String label, final String value)
	{
		Requests result;

		//
		result = new Requests();

		//
		for (Request request : this.requests)
		{
			if (StringUtils.equals(request.properties().getProperty(label), value))
			{
				result.add(request);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Requests findByStatus(final Request.Status status)
	{
		Requests result;

		//
		result = new Requests();

		//
		for (Request request : this.requests)
		{
			if (request.getStatus() == status)
			{
				result.add(request);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Request getByIndex(final int index)
	{
		Request result;

		result = this.requests.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.requests.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Request> iterator()
	{
		Iterator<Request> result;

		result = this.requests.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Request request : this.requests)
		{
			if (request.getId() > result)
			{
				result = request.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Request request)
	{
		this.requests.remove(request);
	}

	/**
	 * 
	 * @return
	 */
	public Requests removeLast()
	{
		Requests result;

		if (!this.requests.isEmpty())
		{
			this.requests.remove(this.requests.size() - 1);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Requests reverse()
	{
		Requests result;

		//
		Collections.reverse(this.requests);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.requests.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Requests sortByCreationDate()
	{
		Requests result;

		//
		Collections.sort(this.requests, new RequestComparator(Criteria.CREATION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Requests sortByDecisionDate()
	{
		Requests result;

		//
		Collections.sort(this.requests, new RequestComparator(Criteria.DECISION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Requests sortById()
	{
		Requests result;

		//
		Collections.sort(this.requests, new RequestComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Requests sortByStatus()
	{
		Requests result;

		//
		Collections.sort(this.requests, new RequestComparator(Criteria.STATUS));

		//
		result = this;

		//
		return result;
	}
}
