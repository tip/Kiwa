/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.sciboard.Request.Type;

/**
 * 
 * @author Christian P. Momon
 */
public class Sciboard implements java.io.Serializable
{
	private static final long serialVersionUID = 5663361841723858096L;
	private Requests requests;
	private long lastRequestId;
	private RequestIdIndex requestIdIndex;

	/**
	 * 
	 */
	public Sciboard()
	{
		this.requests = new Requests();
		this.lastRequestId = 0;
		this.requestIdIndex = new RequestIdIndex();
	}

	/**
	 * 
	 * @param request
	 * @param author
	 * @param text
	 * @return
	 */
	public synchronized Comment addComment(final Request request, final Account author, final String text)
	{
		Comment result;

		result = new Comment(request.comments().lastId(), author.getId(), text);
		request.comments().add(result);

		//
		return result;
	}

	/**
	 * 
	 * @param request
	 * @param ownerId
	 * @param comment
	 */
	public synchronized void addComment(final Request request, final long authorId, final String text)
	{
		request.comments().add(new Comment(request.comments().lastId() + 1, authorId, text));
	}

	/**
	 * 
	 * @param request
	 * @param author
	 * @param text
	 * @return
	 */
	public synchronized Vote addVote(final Request request, final Account author, final Vote.Value value)
	{
		Vote result;

		result = new Vote(author.getId(), value);
		request.votes().add(result);

		//
		return result;
	}

	/**
	 * 
	 * @param request
	 * @param comment
	 */
	public void cancelRequest(final Request request, final String comment)
	{
		if (request == null)
		{
			throw new IllegalArgumentException("request is null.");
		}
		else if (comment == null)
		{
			throw new IllegalArgumentException("comment is null.");
		}
		else
		{
			request.setCanceled(request.getContributorId(), comment);

			if (request.getType() == Request.Type.DATASET_PUBLISH)
			{
				String datasetId = request.properties().getProperty("dataset_id");
				Dataset dataset = Kiwa.instance().kidarep().getDatasetById(datasetId);
				dataset.setPublicFile(null);
				dataset.setStatus(Dataset.Status.CREATED);
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.requests.clear();
		this.requestIdIndex.clear();
		this.lastRequestId = 0;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfComments()
	{
		long result;

		result = 0;
		for (Request request : this.requests)
		{
			result += request.countOfComments();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfRequests()
	{
		long result;

		result = this.requests.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfVotes()
	{
		long result;

		result = 0;
		for (Request request : this.requests)
		{
			result += request.countOfVotes();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param fullName
	 * @param email
	 * @param password
	 * @return
	 */
	public synchronized Request createRequest(final Contributor contributor, final String contributorComment, final Type type, final Properties parameters)
	{
		Request result;

		if (contributor == null)
		{
			throw new IllegalArgumentException("contributor is null.");
		}
		else
		{
			result = createRequest(contributor.getId(), contributorComment, type, parameters);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param fullName
	 * @param email
	 * @param password
	 * @return
	 */
	public synchronized Request createRequest(final long contributorId, final String contributorComment, final Type type, final Properties parameters)
	{
		Request result;

		if (type == null)
		{
			throw new IllegalArgumentException("type is null.");
		}
		else
		{
			result = new Request(nextRequestId(), contributorId, contributorComment, type, parameters);
			this.requests.add(result);
			this.requestIdIndex.put(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public Requests findPreviousPublicationRequestsByDatasetId(final String datasetId)
	{
		Requests result;

		result = new Requests();
		if (NumberUtils.isDigits(datasetId))
		{
			for (Request request : this.requests)
			{
				if ((request.getType() == Request.Type.DATASET_PUBLISH) && (StringUtils.equals(request.properties().getProperty("dataset_id"), datasetId)))
				{
					result.add(request);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public Requests findRequests(final Account account)
	{
		Requests result;

		//
		if (account == null)
		{
			result = new Requests();
		}
		else
		{
			result = this.requests.findByContributorId(account.getId());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public Requests findRequestsByDatasetId(final String datasetId)
	{
		Requests result;

		result = this.requests.findByProperty("dataset_id", datasetId);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Request getRequestById(final long id)
	{
		Request result;

		result = this.requestIdIndex.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Request getRequestById(final Long id)
	{
		Request result;

		if (id == null)
		{
			result = null;
		}
		else
		{
			result = this.requestIdIndex.getById(id);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Request getRequestById(final String id)
	{
		Request result;

		if (NumberUtils.isNumber(id))
		{
			result = this.requestIdIndex.getById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastRequestId()
	{
		return this.lastRequestId;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized long nextRequestId()
	{
		long result;

		this.lastRequestId += 1;
		result = this.lastRequestId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void rebuildIndexes()
	{
		this.requestIdIndex.rebuild(this.requests);
	}

	/**
	 * 
	 * @return
	 */
	public Requests requests()
	{
		Requests result;

		result = this.requests;

		//
		return result;
	}

	/**
	 * 
	 */
	public void resetLastRequestId()
	{
		this.lastRequestId = this.requests.lastId();
	}

	/**
	 * 
	 * @param request
	 * @param ownerId
	 * @param value
	 */
	public void setVote(final Request request, final long ownerId, final Vote.Value value)
	{
		request.votes().add(new Vote(ownerId, value));
	}
}
