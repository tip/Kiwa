/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Comments implements Iterable<Comment>
{
	private ArrayList<Comment> comments;

	/**
	 * 
	 */
	public Comments()
	{
		this.comments = new ArrayList<Comment>();
	}

	/**
	 * 
	 */
	public Comments(final int initialCapacity)
	{
		this.comments = new ArrayList<Comment>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final Comment source)
	{
		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			this.comments.add(source);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.comments.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Comments copy()
	{
		Comments result;

		//
		result = new Comments(this.comments.size());

		//
		for (Comment comment : this.comments)
		{
			result.add(comment);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Comments first(final int targetCount)
	{
		Comments result;

		//
		result = new Comments(targetCount);

		//
		boolean ended = false;
		Iterator<Comment> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Comment getByIndex(final int index)
	{
		Comment result;

		result = this.comments.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.comments.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Comment> iterator()
	{
		Iterator<Comment> result;

		result = this.comments.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Comment last()
	{
		Comment result;

		if (this.comments.isEmpty())
		{
			result = null;
		}
		else
		{
			result = this.comments.get(this.comments.size() - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastId()
	{
		long result;

		result = 0;
		for (Comment comment : this)
		{
			if (comment.getId() > result)
			{
				result = comment.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Comment message)
	{
		this.comments.remove(message);
	}

	/**
	 * 
	 * @return
	 */
	public Comments reverse()
	{
		Comments result;

		//
		Collections.reverse(this.comments);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.comments.size();

		//
		return result;
	}
}
