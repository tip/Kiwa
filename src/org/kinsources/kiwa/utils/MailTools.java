/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Christian P. Momon
 * 
 */
public class MailTools
{
	/*
	 * Static nested class to act as a JAF datasource to send HTML e-mail content
	 */
	static class HTMLDataSource implements DataSource
	{
		private String html;

		public HTMLDataSource(final String htmlString)
		{
			this.html = htmlString;
		}

		/**
         * 
         */
		@Override
		public String getContentType()
		{
			return "text/html";
		}

		/**
		 * Return HTML string in an InputStream. A new stream must be returned each time.
		 */
		@Override
		public InputStream getInputStream() throws IOException
		{
			if (this.html == null)
			{
				throw new IOException("Null HTML");
			}
			return new ByteArrayInputStream(this.html.getBytes());
		}

		/**
         * 
         */
		@Override
		public String getName()
		{
			return "JAF text/html dataSource to send e-mail only";
		}

		/**
         * 
         */
		@Override
		public OutputStream getOutputStream() throws IOException
		{
			throw new IOException("This DataHandler cannot write HTML");
		}
	}

	private static final Logger logger = LoggerFactory.getLogger(MailTools.class);

	/**
	 * This method sends a email using context.xml file to set the SMTP protocol.
	 * 
	 * @param to
	 * @param from
	 * @param replyTo
	 * @param cc
	 * @param bcc
	 * @param subject
	 * @param htmlMessage
	 */
	public static boolean sendSLLHTMLEmail(final String to, final String from, final String replyTo, final String cc, final String bcc, final String subject, final CharSequence htmlMessage)
	{
		boolean result;

		try
		{
			logger.debug("[to={}][from={}][replyTo={}][cc={}][bcc={}][subject={}]", to, from, replyTo, cc, bcc, subject);

			//
			Context initialContext = new InitialContext();
			Context environmentContext = (Context) initialContext.lookup("java:comp/env");
			Session session = (Session) environmentContext.lookup("mail/kiwamailer");

			//
			Transport bus = session.getTransport("smtp");

			bus.connect(session.getProperty("mail.smtp.user"), session.getProperty("password"));

			// Instantiate a message
			Message message = new MimeMessage(session);

			//
			InternetAddress[] toRecipients;
			if (to == null)
			{
				toRecipients = new InternetAddress[0];
			}
			else
			{
				toRecipients = InternetAddress.parse(to);
			}
			InternetAddress[] ccRecipients;
			if (cc == null)
			{
				ccRecipients = new InternetAddress[0];
			}
			else
			{
				ccRecipients = InternetAddress.parse(cc);
			}
			InternetAddress[] bccRecipients;
			if (bcc == null)
			{
				bccRecipients = new InternetAddress[0];
			}
			else
			{
				bccRecipients = InternetAddress.parse(bcc);
			}
			InternetAddress[] allRecipients = ArrayUtils.addAll(toRecipients, ccRecipients);
			allRecipients = ArrayUtils.addAll(allRecipients, bccRecipients);

			// Set message attributes
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, toRecipients);
			if (replyTo != null)
			{
				message.setReplyTo(InternetAddress.parse(replyTo, false));
			}
			if (cc != null)
			{
				message.setRecipients(Message.RecipientType.CC, ccRecipients);
			}
			if (bcc != null)
			{
				message.setRecipients(Message.RecipientType.BCC, bccRecipients);
			}

			//
			message.setSubject(subject);

			//
			message.setSentDate(new Date());

			//
			message.setDataHandler(new DataHandler(new HTMLDataSource(htmlMessage.toString())));
			message.saveChanges();

			//
			bus.sendMessage(message, allRecipients);
			bus.close();

			//
			result = true;

		}
		catch (MessagingException mex)
		{
			// Prints all nested (chained) exceptions as well
			mex.printStackTrace();
			// How to access nested exceptions
			while (mex.getNextException() != null)
			{
				// Get next exception in chain
				Exception ex = mex.getNextException();
				ex.printStackTrace();
				if (!(ex instanceof MessagingException))
				{
					break;
				}
				else
				{
					mex = (MessagingException) ex;
				}
			}

			//
			result = false;
		}
		catch (NamingException exception)
		{
			//
			logger.error("Error instanciating the session form the environment configuration.");
			logger.error(ExceptionUtils.getStackTrace(exception));

			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * This method sends a email using parameter to set the SMTP protocol.
	 * 
	 * 
	 * @param to
	 * @param from
	 * @param replyTo
	 * @param cc
	 * @param bcc
	 * @param smtpHost
	 * @param userLogin
	 * @param userPassword
	 * @param subject
	 * @param htmlMessage
	 */
	public static void sendSLLHTMLEmail(final String to, final String from, final String replyTo, final String cc, final String bcc, final String smtpHost, final String userLogin,
			final String userPassword, final String subject, final CharSequence htmlMessage)
	{
		try
		{
			logger.debug("[to={}][from={}][replyTo={}][cc={}][bcc={}][userLogin={}][subject={}]", to, from, replyTo, cc, bcc, userLogin, subject);

			//
			Properties props = new Properties();
			props.put("mail.smtp.host", smtpHost);
			props.put("mail.debug", "true");

			props.put("mail.smtp.socketFactory.port", "587");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "587");

			//
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator()
			{
				@Override
				protected PasswordAuthentication getPasswordAuthentication()
				{
					return new PasswordAuthentication(userLogin, userPassword);
				}
			});
			Transport bus = session.getTransport("smtp");
			bus.connect(smtpHost, userLogin, userPassword);

			// Instantiate a message
			Message message = new MimeMessage(session);

			//
			InternetAddress[] toRecipients;
			if (to == null)
			{
				toRecipients = new InternetAddress[0];
			}
			else
			{
				toRecipients = InternetAddress.parse(to);
			}
			InternetAddress[] ccRecipients;
			if (cc == null)
			{
				ccRecipients = new InternetAddress[0];
			}
			else
			{
				ccRecipients = InternetAddress.parse(cc);
			}
			InternetAddress[] bccRecipients;
			if (bcc == null)
			{
				bccRecipients = new InternetAddress[0];
			}
			else
			{
				bccRecipients = InternetAddress.parse(bcc);
			}
			InternetAddress[] allRecipients = ArrayUtils.addAll(toRecipients, ccRecipients);
			allRecipients = ArrayUtils.addAll(allRecipients, bccRecipients);

			// Set message attributes
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, toRecipients);
			if (replyTo != null)
			{
				message.setReplyTo(InternetAddress.parse(replyTo, false));
			}
			if (cc != null)
			{
				message.setRecipients(Message.RecipientType.CC, ccRecipients);
			}
			if (bcc != null)
			{
				message.setRecipients(Message.RecipientType.BCC, bccRecipients);
			}

			//
			message.setSubject(subject);

			//
			message.setSentDate(new Date());

			//
			message.setDataHandler(new DataHandler(new HTMLDataSource(htmlMessage.toString())));
			message.saveChanges();

			//
			bus.sendMessage(message, allRecipients);
			bus.close();

		}
		catch (MessagingException mex)
		{
			// Prints all nested (chained) exceptions as well
			mex.printStackTrace();
			// How to access nested exceptions
			while (mex.getNextException() != null)
			{
				// Get next exception in chain
				Exception ex = mex.getNextException();
				ex.printStackTrace();
				if (!(ex instanceof MessagingException))
				{
					break;
				}
				else
				{
					mex = (MessagingException) ex;
				}
			}
		}
	}
}
