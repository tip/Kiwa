/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.text.Collator;
import java.util.Enumeration;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.sciboard.Vote;
import org.kinsources.kiwa.website.webmaster.System_infos_xhtml;

import fr.devinsy.util.StringList;

/**
 *
 */
public class KiwaUtils
{
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(KiwaUtils.class);

	private static final Pattern URLIZE_PATTERN = Pattern.compile("(^|\\s)(https?://[^\\s<>]+)(\\s|$)");
	private static final String URLIZE_TARGET = "$1<a href=\"$2\">$2</a>$3";

	/**
	 * 
	 * @return
	 */
	public static String classCompilerVersion()
	{
		String result;

		try
		{
			URL url = System_infos_xhtml.class.getResource("/org/kinsources/kiwa/utils/KiwaUtils.class");

			byte[] bytes = IOUtils.toByteArray(url);
			byte major = bytes[7];
			byte minor = bytes[8];

			String humanName;
			switch (major)
			{
				case 44:
					humanName = "1.0";
				break;
				case 45:
					humanName = "1.1";
				break;
				case 46:
					humanName = "1.2";
				break;
				case 47:
					humanName = "1.3";
				break;
				case 48:
					humanName = "1.4";
				break;
				case 49:
					humanName = "5";
				break;
				case 50:
					humanName = "6";
				break;
				case 51:
					humanName = "7";
				break;
				case 52:
					humanName = "8";
				break;
				default:
					humanName = "unknown";
			}

			result = String.format("Java %s (%d.%d)", humanName, major, minor);
		}
		catch (IOException exception)
		{
			LOGGER.error("Error computing class Compiler version.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable boolean values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, false)  > 0
	 * compare(null, true)   > 0
	 * compare(false, null)  < 0
	 * compare(true, null)   < 0
	 * compare(false, false) = 0
	 * compare(false, true)  < 0
	 * compare(true, false)  > 0
	 * compare(true, true)   = 0
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Boolean a, final Boolean b)
	{
		int result;

		//
		if ((a == null) && (b == null))
		{
			result = 0;
		}
		else if (a == null)
		{
			result = -1;
		}
		else if (b == null)
		{
			result = +1;
		}
		else
		{
			result = a.compareTo(b);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable JODA DateTime values.
	 * 
	 * Null means future then <code>null</code> is always superior.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  > 0
	 * compare(alpha, null)  < 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final DateTime alpha, final DateTime bravo)
	{
		int result;

		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = +1;
		}
		else if (bravo == null)
		{
			result = -1;
		}
		else
		{
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable double values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Double alpha, final Double bravo)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = -1;
		}
		else if (bravo == null)
		{
			result = +1;
		}
		else
		{
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable integer values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Integer alpha, final Integer bravo)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = -1;
		}
		else if (bravo == null)
		{
			result = +1;
		}
		else
		{
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable long values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Long alpha, final Long bravo)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = -1;
		}
		else if (bravo == null)
		{
			result = +1;
		}
		else
		{
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable string values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final String alpha, final String bravo)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = -1;
		}
		else if (bravo == null)
		{
			result = +1;
		}
		else
		{
			result = Collator.getInstance().compare(alpha, bravo);
		}

		//
		return result;
	}

	/**
	 * 
	 * Warning: unchecked.
	 * 
	 * @param source
	 * @return
	 */
	public static String fixXss(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.replace("<!", "&lt;!");
			result = result.replaceAll("<[iI][fF][rR][aA][mM][eE]", "&lt;iframe");
			result = result.replaceAll("<[fF][rR][aA][mM][eE]", "&lt;frame");
			result = result.replaceAll("<[sS][cC][rR][iI][pP][tT]", "&lt;script");
			result = result.replaceAll("<[^>]+ on.*>", "&lt;script");
		}

		//
		return result;
	}

	/**
	 * This method converts a byte count in human visual string.
	 * 
	 * @param source
	 *            Count of bytes.
	 * 
	 * @return Human visual string in kilo-bytes.
	 */
	public static String formatByteSize(final Long source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else if (source < 1024)
		{
			result = "~1 kiB";
		}
		else
		{
			result = String.format("%d kiB", (source / 1024));
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static File getServerXmlFile()
	{
		File result;

		String catalinaBase = org.apache.catalina.startup.Bootstrap.getCatalinaBase();

		if (catalinaBase == null)
		{
			result = null;
		}
		else
		{
			result = new File(catalinaBase, "conf/server.xml");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String getServerXmlPath()
	{
		String result;

		try
		{
			File serverXmlFile = getServerXmlFile();
			if (serverXmlFile == null)
			{
				result = null;
			}
			else
			{
				result = serverXmlFile.getCanonicalPath();
			}
		}
		catch (IOException exception)
		{
			LOGGER.debug("Exception detected.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String htmlize(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.replace("\n", "<br/>");
			result = htmlizeWithoutBR(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String htmlizeWithoutBR(final String source)
	{
		String result;

		result = source.replace("&ldquo;", "&#8220;");
		result = result.replace("&rsquo;", "&#8221;");
		result = urlize(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String humanName(final Request.Status status)
	{
		String result;

		switch (status)
		{
			case ACCEPTED:
				result = "Accepted";
			break;
			case ACCEPTED_WITH_REVISIONS:
				result = "Accepted with revisions";
			break;
			case CANCELLED:
				result = "Cancelled";
			break;
			case DECLINED:
				result = "Declined";
			break;
			case OPENED:
				result = "Opened";
			break;
			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String humanName(final Request.Type type)
	{
		String result;

		switch (type)
		{
			case COOPTATION:
				result = "Cooptation";
			break;
			case DATASET_PUBLISH:
				result = "Publication";
			break;
			case DATASET_UNPUBLISH:
				result = "Unpublication";
			break;
			case ISSUE:
				result = "Issue";
			break;
			case UNCOOPTATION:
				result = "Uncooptation";
			break;
			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String humanName(final Vote.Value value)
	{
		String result;

		switch (value)
		{
			case NONE:
				result = "NONE";
			break;
			case AGREE:
				result = "ACCEPT";
			break;
			case DUBIOUS:
				result = "ACCEPT WITH REVISIONS";
			break;
			case DISAGREE:
				result = "DECLINE";
			break;
			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws SocketException
	 */
	public static String ipAddressInfos() throws SocketException
	{
		String result;

		//
		StringList buffer = new StringList();

		//
		Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
		while (networkInterfaces.hasMoreElements())
		{
			//
			NetworkInterface networkInterface = networkInterfaces.nextElement();

			buffer.append(networkInterface.getName());
			buffer.append(" (");
			buffer.append(networkInterface.getDisplayName());
			buffer.append("): ");

			buffer.append("mac=");
			String mac;
			if (networkInterface.getHardwareAddress() == null)
			{
				mac = "n/a";
			}
			else
			{
				mac = Hex.encodeHexString(networkInterface.getHardwareAddress());
			}
			buffer.append(mac);
			buffer.append(",");

			Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
			while (addresses.hasMoreElements())
			{
				InetAddress address = addresses.nextElement();
				buffer.append(address.getHostAddress()).append(",");
			}
			buffer.removeLast();
			buffer.append(";");
		}
		buffer.removeLast();

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String readProcCpuinfo()
	{
		String result;

		try
		{

			File procVersionFile = new File("/proc/cpuinfo");
			if (procVersionFile.exists())
			{
				result = FileUtils.readFileToString(procVersionFile);
			}
			else
			{
				result = null;
			}
		}
		catch (IOException exception)
		{
			LOGGER.debug("Exception detected.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String readProcCpuinfo0()
	{
		String result;

		result = readProcCpuinfo();

		int separator = StringUtils.indexOf(result, "\n\n");

		if (separator != -1)
		{
			result = result.substring(0, separator);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String readProcMeminfo()
	{
		String result;

		try
		{

			File procVersionFile = new File("/proc/meminfo");
			if (procVersionFile.exists())
			{
				result = FileUtils.readFileToString(procVersionFile);
			}
			else
			{
				result = null;
			}
		}
		catch (IOException exception)
		{
			LOGGER.debug("Exception detected.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String readProcMeminfo6()
	{
		String result;

		result = readProcMeminfo();

		int separator = StringUtils.indexOf(result, "\n");
		separator = StringUtils.indexOf(result, "\n", separator + 1);
		separator = StringUtils.indexOf(result, "\n", separator + 1);
		separator = StringUtils.indexOf(result, "\n", separator + 1);
		separator = StringUtils.indexOf(result, "\n", separator + 1);
		separator = StringUtils.indexOf(result, "\n", separator + 1);
		separator = StringUtils.indexOf(result, "\n", separator + 1);

		if (separator != -1)
		{
			result = result.substring(0, separator);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String readProcVersion()
	{
		String result;

		try
		{

			File procVersionFile = new File("/proc/version");
			if (procVersionFile.exists())
			{
				result = FileUtils.readFileToString(procVersionFile);
			}
			else
			{
				result = null;
			}
		}
		catch (IOException exception)
		{
			LOGGER.debug("Exception detected.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String readServerXml()
	{
		String result;

		try
		{
			File procVersionFile = getServerXmlFile();
			if ((procVersionFile != null) && (procVersionFile.exists()))
			{
				result = FileUtils.readFileToString(procVersionFile);
			}
			else
			{
				result = null;
			}
		}
		catch (IOException exception)
		{
			LOGGER.debug("Exception detected.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String[][] split(final String[] source, final int count)
	{
		String[][] result;

		if ((source == null) || (count < 1))
		{
			result = new String[0][];
		}
		else
		{
			//
			int partitionCount = ((source.length - 1) / count) + 1;

			//
			result = new String[partitionCount][];

			for (int resultIndex = 0; resultIndex < result.length; resultIndex++)
			{
				//
				if (resultIndex < partitionCount - 1)
				{
					//
					result[resultIndex] = new String[count];
				}
				else
				{
					result[resultIndex] = new String[source.length % count];
				}

				//
				for (int index = 0; index < result[resultIndex].length; index++)
				{
					result[resultIndex][index] = source[resultIndex * count + index];
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String urlize(final String source)
	{
		String result;

		result = URLIZE_PATTERN.matcher(source).replaceAll(URLIZE_TARGET);

		//
		return result;
	}
}
