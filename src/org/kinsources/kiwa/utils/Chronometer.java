/**
 * Copyright 2008 Christian P. MOMON (christian.momon@devinsy.fr).
 * Copyright 2012-2014 Christian P. MOMON (christian.momon@devinsy.fr), TIP.
 * Copyright 2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.kinsources.kiwa.utils;

import java.util.Date;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Chronometer
{
	private long sum;
	private long startTime;
	private long partialTime;

	/**
	 * 
	 */
	public Chronometer()
	{
		this.reset();
	}

	/**
	 * 
	 */
	public long appendingInterval()
	{
		long result;

		result = this.sum + new Date().getTime() - this.startTime;

		//
		return result;
	}

	/**
	 * 
	 */
	public long interval()
	{
		long result;

		result = this.sum;

		//
		return result;
	}

	/**
	 * 
	 */
	public long partialInterval()
	{
		long result;

		result = new Date().getTime() - this.partialTime;

		//
		return result;
	}

	/**
	 * 
	 */
	public Chronometer reset()
	{
		Chronometer result;

		//
		this.sum = 0;
		this.startTime = new Date().getTime();
		this.partialTime = this.startTime;

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 */
	public long runningInterval()
	{
		long result;

		result = new Date().getTime() - this.startTime;

		//
		return result;
	}

	public Chronometer start()
	{
		Chronometer result;

		//
		this.startTime = new Date().getTime();

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 */
	public Chronometer step()
	{
		Chronometer result;

		//
		long now = new Date().getTime();
		this.sum += now - this.startTime;
		this.partialTime = this.startTime;
		this.startTime = now;

		//
		result = this;

		//
		return result;
	}

	/**
	 * TO BE COMPLETED.
	 */
	public static String toHumanString(final long interval)
	{
		String result;

		if (interval < 1000)
		{
			result = interval + " ms";
		}
		else if (interval < 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + " s";
		}
		else if (interval < 60 * 60 * 1000)
		{
			result = (interval / 1000 / 60) + " mn " + (interval / 1000) % 60 + "," + interval % 1000 + " s";
		}
		else if (interval < 24 * 60 * 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + " s";
		}
		else if (interval < 7 * 24 * 60 * 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + " s";
		}
		else
		// if (interval < 7*24*60*60*1000)
		{
			result = interval / 1000 + "," + interval % 1000 + " s";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static String toShortHumanString(final long interval)
	{
		String result;

		if (interval < 1000)
		{
			result = interval + " ms";
		}
		else if (interval < 2 * 1000)
		{
			result = interval / 1000 + " seconde";
		}
		else if (interval < 60 * 1000)
		{
			result = interval / 1000 + " secondes";
		}
		else if (interval < 2 * 60 * 1000L)
		{
			result = interval / (60 * 1000L) + " minute";
		}
		else if (interval < 60 * 60 * 1000L)
		{
			result = interval / (60 * 1000L) + " minutes";
		}
		else if (interval < 2 * 60 * 60 * 1000L)
		{
			result = interval / (60 * 60 * 1000L) + " heure";
		}
		else if (interval < 24 * 60 * 60 * 1000L)
		{
			result = interval / (60 * 60 * 1000L) + " heures";
		}
		else if (interval < 2 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (24 * 60 * 60 * 1000L) + " jour";
		}
		else if (interval < 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (24 * 60 * 60 * 1000L) + " jours";
		}
		else if (interval < 2 * 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (7 * 24 * 60 * 60 * 1000L) + " semaine";
		}
		else if (interval < 30 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (7 * 24 * 60 * 60 * 1000L) + " semaines";
		}
		else if (interval < 52 * 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (30 * 24 * 60 * 60 * 1000L) + " mois";
		}
		else if (interval < 2 * 52 * 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (52 * 7 * 24 * 60 * 60 * 1000L) + " année";
		}
		else
		{
			result = interval / (52 * 7 * 24 * 60 * 60 * 1000L) + " années";
		}

		//
		return result;
	}
}
