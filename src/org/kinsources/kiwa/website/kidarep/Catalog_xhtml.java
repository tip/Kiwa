/**
 * Copyright 2014 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * Copyright 2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.stag.StatisticTag;
import org.kinsources.kiwa.website.charter.ErrorView;

/**
 *
 */
public class Catalog_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Catalog_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("Catalog starting...");
			kiwa.logPageHit("pages.kidarep.catalog", request);

			// Get parameters.
			// ===============

			// Use parameters.
			// ===============
			String fileName = "kinsources_catalog-" + DateTime.now().toString("yyyy-MM-dd-HH'h'mm'mn'ss's'") + ".csv";

			response.reset();
			response.setContentType("text/csv; charset=UTF-8");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.flushBuffer();

			//
			PrintWriter out = response.getWriter();

			//
			out.println("Kinsources Catalog");
			out.println("Source: https://www.kinsources.net/");
			out.println("Build time: " + DateTime.now().toString("yyyy-MM-dd-HH'h'mm'mn'ss's'"));
			out.println();

			//
			out.println("ID\tName\tContributor\tAuthor\tPermanent link\tLicense\tIndividuals\tUnions\tRelation models\tRelations\tDepth\tAttribute labels\tAttribute values\tPublication date");

			//
			for (Dataset dataset : kiwa.kidarep().findPublishedDatasets())
			{
				//
				if (dataset.getPublicFile() != null)
				{
					//
					out.print(String.valueOf(dataset.getId()));
					out.print("\t");
					out.print(dataset.getName());
					out.print("\t");
					out.print(dataset.getContributor().getFullName());
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getAuthor(), ""));
					out.print("\t");
					out.print(kiwa.permanentURI(dataset));
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getLicense(), ""));
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getPublicFile().statistics().getStringValue(StatisticTag.NUM_OF_INDIVIDUALS), ""));
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getPublicFile().statistics().getStringValue(StatisticTag.NUM_OF_UNIONS), ""));
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getPublicFile().statistics().getStringValue(StatisticTag.NUM_OF_RELATION_MODELS), ""));
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getPublicFile().statistics().getStringValue(StatisticTag.NUM_OF_RELATIONS), ""));
					out.print("\t");
					out.print(StringUtils.defaultString(dataset.getPublicFile().statistics().getStringValue(StatisticTag.DEPTH), ""));
					out.print("\t");
					out.print(String.valueOf(dataset.getPublicFile().statistics().attributeDescriptors().size()));
					out.print("\t");
					out.print(String.valueOf(dataset.getPublicFile().statistics().attributeDescriptors().total()));
					out.print("\t");
					if (dataset.getPublicationDate() == null)
					{
						out.print("");
					}
					else
					{
						out.print(dataset.getPublicationDate().toString("yyyy-MM-dd"));
					}
					out.println();
				}
			}

			//
			response.flushBuffer();

		}
		catch (final IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Export metadata issue", exception.getMessage(), "/kidarep/kidarep_admin.xhtml");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
