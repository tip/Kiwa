/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Dataset_file_upload_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Dataset_file_upload_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/dataset_file_upload.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.dataset_file_upload", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			// Use parameters.
			// ===============

			// Send response.
			// ==============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

			if (account == null)
			{
				Redirector.redirect(response, "not_logged_upload.xhtml");
			}
			else if (dataset == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else if (dataset.getStatus() != Dataset.Status.CREATED)
			{
				throw new IllegalArgumentException("Invalid status.");
			}
			else if ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				TagDataManager data = new TagDataManager();

				//
				data.setContent("label_title", "Upload a dataset file to " + dataset.getName() + " (ID " + dataset.getId() + ")");
				data.setContent("introduction", "In order to upload a dataset file, please select the control you want:");
				data.setContent("auto_marriages", "Auto-Marriages");
				data.setContent("cyclic_descent", "Cases of Cyclic Descent");
				data.setContent("same_sex_spouses", "Same Sex Spouses");
				data.setContent("female_fathers_or_male_mothers", "Female Fathers or Male Mothers");
				data.setContent("multiple_fathers_or_mothers", "Multiple Fathers or Mothers");
				data.setContent("persons_of_unknown_sex", "Persons of unknown Sex");
				data.setContent("nameless_persons", "Nameless Persons");
				data.setContent("parent_child_marriages", "Parent-Child Marriages");
				data.setContent("label_new_dataset", "New dataset file:");

				data.setAttribute("upload_button", "value", "Upload dataset file");
				data.setAttribute("upload_dataset_id", "value", dataset.getId());

				data.setContent("button_cancel", "Cancel");
				data.setAttribute("button_cancel", "href", kiwa.permanentURI(dataset) + "#dataset_file_section");

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(account.getId(), locale, content);

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
