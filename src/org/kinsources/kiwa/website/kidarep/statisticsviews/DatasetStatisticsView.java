/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.statisticsviews;

import java.util.Locale;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class DatasetStatisticsView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DatasetStatisticsView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/statisticsviews/dataset_statistics_view.html");
	private static GenealogyStatisticsView genealogyStatisticsView = new GenealogyStatisticsView();
	private static TerminologyStatisticsView terminologyStatisticsView = new TerminologyStatisticsView();
	private static GraphicsView graphicsView = new GraphicsView();
	private static AttributesView attributesView = new AttributesView();
	private static ControlsView controlsView = new ControlsView();

	/**
	 * 
	 */
	public DatasetStatisticsView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		// Statistics values
		// =================
		if (dataset.isGenealogy())
		{
			data.setContent("values_view", genealogyStatisticsView.getHtmlBody(dataset, account, locale));
		}
		else if (dataset.isTerminology())
		{
			data.setContent("values_view", terminologyStatisticsView.getHtmlBody(dataset, account, locale));
		}

		// Graphics
		// ========
		data.setContent("graphics_view", graphicsView.getHtmlBody(dataset, account, locale));

		// Attributes
		// ==========
		if (dataset.isGenealogy())
		{
			data.setContent("attributes_view", attributesView.getHtmlBody(dataset, account, locale));
		}
		else
		{
			data.setAttribute("attributes_view", "class", "xid:nodisplay");
		}

		// Controls
		// ========
		data.setContent("controls_view", controlsView.getHtmlBody(dataset, account, locale));

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(dataset, account, locale));

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
