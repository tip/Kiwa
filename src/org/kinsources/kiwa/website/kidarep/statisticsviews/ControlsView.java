/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.statisticsviews;

import java.util.Locale;

import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.Statistics;
import org.tip.puck.net.relations.workers.RelationModelReporter;
import org.tip.puck.net.workers.ControlReporter;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class ControlsView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ControlsView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/statisticsviews/controls_view.html");

	/**
	 * 
	 */
	public ControlsView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		// Controls
		// ========
		DatasetFile datasetFile = kiwa.getDatasetFile(dataset, account);

		if (datasetFile == null)
		{
			data.setContent("controls_section", "No controls.");
		}
		else
		{
			Statistics statistics = datasetFile.statistics();
			if (dataset.isGenealogy())
			{
				int rowCount = 0;
				for (ControlReporter.ControlType type : ControlReporter.ControlType.values())
				{
					data.setContent("control_row", rowCount, "control_name", type.name());

					String value = statistics.getStringValue(type.name());
					if (value != null)
					{
						data.setContent("control_row", rowCount, "control_value", value);

						if ((NumberUtils.isDigits(value)) && (Integer.parseInt(value) == 0))
						{
							data.setAttribute("control_row", rowCount, "control_value", "class", "td_number success");
						}
						else
						{
							data.setAttribute("control_row", rowCount, "control_value", "class", "td_number warning");
						}
					}

					rowCount += 1;
				}
			}
			else
			{
				int rowCount = 0;
				for (RelationModelReporter.ControlType type : RelationModelReporter.ControlType.values())
				{
					data.setContent("control_row", rowCount, "control_name", type.name());

					String value = statistics.getStringValue(type.name());
					if (value != null)
					{
						data.setContent("control_row", rowCount, "control_value", value);

						if ((NumberUtils.isDigits(value)) && (Integer.parseInt(value) == 0))
						{
							data.setAttribute("control_row", rowCount, "control_value", "class", "td_number success");
						}
						else
						{
							data.setAttribute("control_row", rowCount, "control_value", "class", "td_number warning");
						}

					}

					rowCount += 1;
				}
			}
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(dataset, account, locale));

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
