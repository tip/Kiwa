/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.wizard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.kinsources.kiwa.website.kidarep.statisticsviews.DatasetStatisticsView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Kinship_file_upload_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Kinship_file_upload_xhtml.class);
	private static URLPresenter genealogyEmptyView = new URLPresenter("/org/kinsources/kiwa/website/kidarep/wizard/genealogy_file_upload_empty.html");
	private static URLPresenter genealogySetView = new URLPresenter("/org/kinsources/kiwa/website/kidarep/wizard/genealogy_file_upload_set.html");
	private static URLPresenter terminologyEmptyView = new URLPresenter("/org/kinsources/kiwa/website/kidarep/wizard/terminology_file_upload_empty.html");
	private static URLPresenter terminologySetView = new URLPresenter("/org/kinsources/kiwa/website/kidarep/wizard/terminology_file_upload_set.html");
	private static DatasetStatisticsView datasetStatisticsView = new DatasetStatisticsView();
	private static final Kiwa kiwa = Kiwa.instance();
	private static WizardView wizardView = new WizardView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.my_space.wizard.kinship_file_upload", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

			// Send response.
			// ==============
			if (account == null)
			{
				Redirector.redirect(response, request.getContextPath() + "/accounts/login.xhtml");
			}
			else if (dataset == null)
			{
				throw new IllegalArgumentException("Undefined dataset.");
			}
			else if (dataset.getStatus() != Dataset.Status.CREATED)
			{
				throw new IllegalArgumentException("Bad dataset status.");
			}
			else if ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("dataset_name", XMLTools.escapeXmlBlank(dataset.getName()));
				data.setAttribute("upload_dataset_id", "value", dataset.getId());

				//
				data.setAttribute("previous_button", "href", "metadata_edition.xhtml?dataset_id=" + dataset.getId());
				data.setAttribute("previous_button_top", "href", "metadata_edition.xhtml?dataset_id=" + dataset.getId());
				data.setAttribute("next_button", "href", "attachment_upload.xhtml?dataset_id=" + dataset.getId());
				data.setAttribute("next_button_top", "href", "attachment_upload.xhtml?dataset_id=" + dataset.getId());

				//
				StringBuffer content;
				if (dataset.isGenealogy())
				{
					if (dataset.getOriginFile() == null)
					{
						//
						data.setContent("label_title", "Upload a dataset file to " + dataset.getName() + " (ID " + dataset.getId() + ")");
						data.setContent("introduction", "In order to upload your genealogy dataset file, please select the control you want:");
						data.setContent("auto_marriages", "Auto-Marriages");
						data.setContent("cyclic_descent", "Cases of Cyclic Descent");
						data.setContent("same_sex_spouses", "Same Sex Spouses");
						data.setContent("female_fathers_or_male_mothers", "Female Fathers or Male Mothers");
						data.setContent("multiple_fathers_or_mothers", "Multiple Fathers or Mothers");
						data.setContent("persons_of_unknown_sex", "Persons of unknown Sex");
						data.setContent("nameless_persons", "Nameless Persons");
						data.setContent("parent_child_marriages", "Parent-Child Marriages");
						data.setContent("label_new_dataset", "New dataset file:");

						data.setAttribute("upload_button", "value", "Upload dataset file");
						data.setAttribute("upload_dataset_id", "value", dataset.getId());

						//
						content = genealogyEmptyView.dynamize(data);
					}
					else
					{
						//
						data.setContent("lead_paragraph", kiwa.hico().getText("Dataset Publication Wizard > Step 3 > Set", locale));

						//
						data.setContent("origin_file_link", dataset.getOriginFile().name());
						data.setAttribute("origin_file_link", "href",
								SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_origin_file", String.valueOf(dataset.getId()), dataset.getOriginFile().name()));
						data.setContent("origin_file_length", KiwaUtils.formatByteSize(dataset.getOriginFile().length()));

						data.setAttribute("remove_dataset_file_button", "title", "Remove");
						data.setAttribute("remove_dataset_file_button", "href", "remove_dataset_file.xhtml?dataset_id=" + dataset.getId());

						// Statistics
						// ==========
						data.setContent("statistics_view", datasetStatisticsView.getHtmlBody(dataset, account, locale));

						//
						content = genealogySetView.dynamize(data);
					}
				}
				else
				{
					if (dataset.getOriginFile() == null)
					{
						//
						data.setContent("label_title", "Upload a dataset file to " + dataset.getName() + " (ID " + dataset.getId() + ")");
						data.setContent("introduction", "In order to upload your terminology dataset file, please select the control you want:");
						data.setContent("general_check", "General check");

						data.setAttribute("upload_button", "value", "Upload dataset file");
						data.setAttribute("upload_dataset_id", "value", dataset.getId());

						//
						content = terminologyEmptyView.dynamize(data);
					}
					else
					{
						//
						data.setContent("lead_paragraph", kiwa.hico().getText("Dataset Publication Wizard > Step 3 > Set", locale));

						//
						data.setContent("origin_file_link", dataset.getOriginFile().name());
						data.setAttribute("origin_file_link", "href",
								SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_origin_file", String.valueOf(dataset.getId()), dataset.getOriginFile().name()));
						data.setContent("origin_file_length", KiwaUtils.formatByteSize(dataset.getOriginFile().length()));

						data.setAttribute("remove_dataset_file_button", "title", "Remove");
						data.setAttribute("remove_dataset_file_button", "href", "remove_dataset_file.xhtml?dataset_id=" + dataset.getId());

						// Statistics
						// ==========
						data.setContent("statistics_view", datasetStatisticsView.getHtmlBody(dataset, account, locale));

						//
						content = terminologySetView.dynamize(data);
					}
				}

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, wizardView.getHtml(WizardView.Menu.FILE_UPLOAD, content, locale, dataset));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}

		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
