/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.wizard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.utils.ISO_3166_1_alpha_2;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Metadata_edition_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Metadata_edition_xhtml.class);
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/wizard/metadata_edition.html");
	private static final Kiwa kiwa = Kiwa.instance();
	private static WizardView wizardView = new WizardView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.my_space.wizard.metadata_edition", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

			// Send response.
			// ==============
			if (account == null)
			{
				Redirector.redirect(response, request.getContextPath() + "/accounts/login.xhtml");
			}
			else if (dataset == null)
			{
				throw new IllegalArgumentException("Undefined dataset.");
			}
			else if (dataset.getStatus() != Dataset.Status.CREATED)
			{
				throw new IllegalArgumentException("Bad dataset status.");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("lead_paragraph", kiwa.hico().getText("Dataset Publication Wizard > Step 2", locale));

				//
				data.setAttribute("dataset_id", "value", dataset.getId());

				if (!dataset.hasMainFile())
				{
					if (dataset.isGenealogy())
					{
						data.setAttribute("type_genealogy", "checked", "checked");
					}
					else
					{
						data.setAttribute("type_terminology", "checked", "checked");
					}
				}

				//
				data.setContent("label_id", "ID:");
				data.setContent("id", dataset.getId());

				if ((dataset.getStatus() != Dataset.Status.CREATED) || (dataset.getOriginFile() != null))
				{
					data.setAttribute("type_genealogy", "class", "xid:nodisplay");
					data.setAttribute("type_terminology", "class", "xid:nodisplay");
					if (dataset.isGenealogy())
					{
						data.setAttribute("dataset_type_terminology", "class", "xid:nodisplay");
					}
					else
					{
						data.setAttribute("dataset_type_genealogy", "class", "xid:nodisplay");
					}
				}

				data.setContent("label_name", "Name:");
				data.setAttribute("name", "value", XMLTools.escapeXmlBlank(dataset.getName()));

				data.setContent("label_status", "Status:");
				data.setContent("status", dataset.getStatus().toString());

				data.setContent("label_contributor", "Contributor (contributor):");
				data.setContent("contributor", XMLTools.escapeXmlBlank(dataset.getContributor().getFullName()));

				data.setContent("label_creation_date", "Creation date:");
				data.setContent("creation_date", dataset.getCreationDate().toString("dd/MM/YYYY"));

				data.setContent("label_description", "Description:");
				data.setContent("description", XMLTools.escapeXmlBlank(dataset.getDescription()));

				data.setContent("label_edition_date", "Edition date:");
				data.setContent("edition_date", dataset.getEditionDate().toString("dd/MM/YYYY"));

				data.setContent("label_publication_date", "Publication date:");
				if (dataset.getPublicationDate() == null)
				{
					data.setContent("publication_date", "n/a");
				}
				else
				{
					data.setContent("publication_date", dataset.getPublicationDate().toString("dd/MM/YYYY"));
				}

				data.setContent("label_atlas_code", "Atlas code:");
				data.setAttribute("atlas_code", "value", XMLTools.escapeXmlBlank(dataset.getAtlasCode()));

				data.setContent("label_author", "Author (for citation):");
				data.setAttribute("author", "value", XMLTools.escapeXmlBlank(dataset.getAuthor()));

				data.setContent("label_bibliography", "Bibliography:");
				data.setContent("bibliography", XMLTools.escapeXmlBlank(dataset.getBibliography()));

				data.setContent("label_citation", "Citation<br/>(How to cite in publication):");
				data.setContent("citation", XMLTools.escapeXmlBlank(dataset.getCitation()));

				data.setContent("label_coder", "Coder:");
				data.setAttribute("coder", "value", XMLTools.escapeXmlBlank(dataset.getCoder()));

				data.setContent("label_collection_notes", "Collection notes<br/>(circumstances,<br/> sources, methods,<br/> funding, coverage…):");
				data.setContent("collection_notes", XMLTools.escapeXmlBlank(dataset.getCollectionNotes()));

				data.setContent("label_contact", "Contact:");
				data.setContent("contact", XMLTools.escapeXmlBlank(dataset.getContact()));

				data.setContent("label_continent", "Continent:");
				data.setAttribute("continent", "value", XMLTools.escapeXmlBlank(dataset.getContinent()));

				data.setContent("label_country", "Country:");
				data.setAttribute("country", "value", XMLTools.escapeXmlBlank(dataset.getCountry()));

				data.setContent("label_coverage", "Period of data collection:");
				data.setAttribute("coverage", "value", XMLTools.escapeXmlBlank(dataset.getCoverage()));

				data.setContent("label_ethnic_or_cultural_group", "Ethnic or cultural group:");
				data.setAttribute("ethnic_or_cultural_group", "value", XMLTools.escapeXmlBlank(dataset.getEthnicOrCulturalGroup()));

				data.setContent("label_geographic_coordinate", "Geographic coordinate: <br/>(Latitude/Longitude of centre)");
				data.setAttribute("geographic_coordinate", "value", XMLTools.escapeXmlBlank(dataset.getGeographicCoordinate()));

				data.setContent("label_language_group", "Language group:");
				data.setAttribute("language_group", "value", XMLTools.escapeXmlBlank(dataset.getLanguageGroup()));

				data.setContent("label_history", "Dataset history:");
				data.setContent("history", XMLTools.escapeXmlBlank(dataset.getHistory()));

				data.setContent("label_license", "Conditions of use and license:");
				if (StringUtils.isBlank(dataset.getLicense()))
				{
					data.setAttribute("license_option_cc_by_sa_30", "selected", "selected");
				}
				else
				{
					data.setAttribute("license_option_other", "selected", "selected");
					data.setAttribute("license", "value", XMLTools.escapeXmlBlank(dataset.getLicense()));
				}

				data.setContent("label_location", "Location<br/> (maximal precision):");
				data.setAttribute("location", "value", XMLTools.escapeXmlBlank(dataset.getLocation()));

				data.setContent("label_other_repositories", "Other repositories");
				data.setContent("other_repositories", XMLTools.escapeXmlBlank(dataset.getOtherRepositories()));

				data.setContent("label_period", "Period covered by the dataset:");
				data.setAttribute("period", "value", XMLTools.escapeXmlBlank(dataset.getPeriod()));

				data.setContent("label_period_note", "Period note:");
				data.setAttribute("period_note", "value", XMLTools.escapeXmlBlank(dataset.getPeriodNote()));

				data.setContent("label_radius_from_center", "Radius from center (km):");
				if (dataset.getRadiusFromCenter() == null)
				{
					data.setContent("radius_from_center", "");
				}
				else
				{
					data.setAttribute("radius_from_center", "value", dataset.getRadiusFromCenter());
				}

				data.setContent("label_reference", "Reference:");
				data.setAttribute("reference", "value", XMLTools.escapeXmlBlank(dataset.getReference()));

				data.setContent("label_region", "Region:");
				data.setAttribute("region", "value", XMLTools.escapeXmlBlank(dataset.getRegion()));

				data.setContent("label_short_description", "Short description:");
				data.setContent("short_description", XMLTools.escapeXmlBlank(dataset.getShortDescription()));

				//
				int countryIndex = 0;
				for (String code : ISO_3166_1_alpha_2.codes())
				{
					//
					data.setContent("country_option", countryIndex, ISO_3166_1_alpha_2.getLabel(code));
					data.setAttribute("country_option", countryIndex, "value", ISO_3166_1_alpha_2.getLabel(code));

					//
					countryIndex += 1;
				}

				//
				int continentIndex = 0;
				for (String continent : kiwa.stag().getContinentNames().sort())
				{
					//
					data.setContent("continent_option", continentIndex, continent);
					data.setAttribute("continent_option", continentIndex, "value", continent);

					//
					continentIndex += 1;
				}

				//
				kiwa.hico().generateBubble(data, "bubble.dataset.atlas_code", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.author", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.bibliography", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.citation", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.coder", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.collection_notes", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.contact", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.continent", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.country", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.coverage", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.description", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.ethnic_or_cultural_group", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.geographic_coordinate", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.history", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.license", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.location", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.name", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.owner", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.other_repositories", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.period", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.period_note", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.radius_from_center", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.reference", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.region", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.short_description", locale);

				//
				StringBuffer content = view.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, wizardView.getHtml(WizardView.Menu.METADATA_EDITION, content, locale, dataset));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}

		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
