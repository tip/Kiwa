/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.agora;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.agora.Forum;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Agora_admin_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Agora_admin_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/agora/agora_admin.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.agora.agora_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{

				// Get parameters
				// ==============

				// Use parameters
				// ==============
				TagDataManager data = new TagDataManager();

				//
				data.setContent("forum_count", kiwa.agora().countOfForums());
				data.setContent("topic_count", kiwa.agora().countOfTopics());
				data.setContent("message_count", kiwa.agora().countOfMessages());

				data.setContent("forum_last_id", kiwa.agora().lastForumId());
				data.setContent("topic_last_id", kiwa.agora().lastTopicId());
				data.setContent("message_last_id", kiwa.agora().lastMessageId());

				//
				int lineIndex = 0;
				for (Forum forum : kiwa.agora().forums())
				{
					//
					data.setContent("forum", lineIndex, "forum_id", forum.getId());
					data.setContent("forum", lineIndex, "forum_title", StringEscapeUtils.escapeXml(forum.getTitle()));
					data.setAttribute("forum", lineIndex, "forum_title", "href", "forum_edition.xhtml?target_id=" + forum.getId());
					data.setContent("forum", lineIndex, "forum_subtitle", StringEscapeUtils.escapeXml(forum.getSubtitle()));
					data.setContent("forum", lineIndex, "forum_topic_count", forum.countOfTopics());
					data.setContent("forum", lineIndex, "forum_message_count", forum.countOfMessages());
					data.setContent("forum", lineIndex, "forum_creation_date", forum.getCreationDate().toString("dd/MM/yyyy"));
					data.setContent("forum", lineIndex, "forum_edition_date", forum.getEditionDate().toString("dd/MM/yyyy"));

					if (lineIndex == 0)
					{
						data.setContent("forum", lineIndex, "forum_up_cell", "");
					}
					else
					{
						data.setAttribute("forum", lineIndex, "forum_up", "href", "move_forum.xhtml?target_id=" + forum.getId() + "&amp;direction=up");
					}

					if (lineIndex == kiwa.agora().forums().size() - 1)
					{
						data.setContent("forum", lineIndex, "forum_down_cell", "");
					}
					else
					{
						data.setAttribute("forum", lineIndex, "forum_down", "href", "move_forum.xhtml?target_id=" + forum.getId() + "&amp;direction=down");
					}

					//
					lineIndex += 1;
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.FORUM, userAccount.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
