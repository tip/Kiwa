/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.agora;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.agora.Forum;
import org.kinsources.kiwa.agora.Message;
import org.kinsources.kiwa.agora.Topic;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Forum_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Forum_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/agora/forum.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{

		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.agora.forum", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============

			String forumIdParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("forum_id")));
			logger.info("forumId=[{}]", forumIdParameter);
			long forumId = Long.parseLong(forumIdParameter);

			// Use parameters.
			// ===============
			Forum forum = kiwa.agora().getForumById(forumId);

			if (forum == null)
			{
				throw new IllegalArgumentException("Unknown forum [" + forumId + "]");
			}
			else
			{
				// Send response.
				// ==============
				TagDataManager data = new TagDataManager();

				//
				data.setContent("forum_title", StringEscapeUtils.escapeXml(forum.getTitle()));

				//
				data.setAttribute("topic_create_forum_id", "value", forum.getId());
				if ((account == null) || (account.getStatus() != Status.ACTIVATED))
				{
					data.setContent("new_topic", "");
				}

				//
				if (forum.isEmpty())
				{
					data.setContent("topic_title_cell", "Empty");
				}
				else
				{
					//
					int lineIndex = 0;
					for (Topic topic : forum.topics().copy().sortBySticky().reverse())
					{
						//
						if (topic.isNotSticky())
						{
							data.setContent("topic", lineIndex, "topic_sticky", "");
						}

						if (topic.isOpened())
						{
							data.setContent("topic", lineIndex, "topic_locked", "");
						}

						//
						data.setContent("topic", lineIndex, "topic_title", StringEscapeUtils.escapeXml(topic.getTitle()));
						data.setAttribute("topic", lineIndex, "topic_title", "href", "topic.xhtml?topic_id=" + topic.getId());

						Account creator = kiwa.accountManager().getAccountById(topic.getAuthorId());
						if (creator == null)
						{
							data.setContent("topic", lineIndex, "topic_author", StringEscapeUtils.escapeXml(topic.getAuthorName()));
						}
						else
						{
							data.setContent("topic", lineIndex, "topic_author_link", StringEscapeUtils.escapeXml(topic.getAuthorName()));
							data.setAttribute("topic", lineIndex, "topic_author_link", "href", "/browser/contributor.xhtml?contributor_id=" + topic.getAuthorId());
						}

						data.setContent("topic", lineIndex, "topic_creation_date", topic.getCreationDate().toString("dd/MM/yyyy HH:mm"));
						data.setContent("topic", lineIndex, "topic_message_count", topic.countOfMessages() - 1);
						Message latest = topic.latestMessage();
						if (latest == null)
						{
							data.setContent("topic", lineIndex, "topic_last_edition_date", "");
							data.setContent("topic", lineIndex, "topic_last_edition_author", "");
						}
						else
						{
							data.setContent("topic", lineIndex, "topic_last_edition_date", latest.getEditionDate().toString("dd/MM/yyyy HH:mm"));

							Account lastAuthor = kiwa.accountManager().getAccountById(latest.getAuthorId());
							if (lastAuthor == null)
							{
								data.setContent("topic", lineIndex, "topic_last_edition_author", StringEscapeUtils.escapeXml(latest.getAuthorName()));
							}
							else
							{
								data.setContent("topic", lineIndex, "topic_last_edition_author_link", StringEscapeUtils.escapeXml(latest.getAuthorName()));
								data.setAttribute("topic", lineIndex, "topic_last_edition_author_link", "href", "/browser/contributor.xhtml?contributor_id=" + latest.getAuthorId());
							}
						}

						//
						lineIndex += 1;
					}
				}

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.FORUM, Account.getId(account), locale, content);

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
