/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.agora;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.agora.Message;
import org.kinsources.kiwa.agora.Topic;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Topic_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Topic_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/agora/topic.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.agora.topic", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============

			String topicIdParameter = request.getParameter("topic_id");
			logger.info("topicId=[{}]", topicIdParameter);
			long topicId = Long.parseLong(topicIdParameter);

			// Use parameters.
			// ===============
			Topic topic = kiwa.agora().getTopicById(topicId);

			if (topic == null)
			{
				throw new IllegalArgumentException("Unknown topic [" + topicId + "]");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("path_forum_title", StringEscapeUtils.escapeXml(topic.getForum().getTitle()));
				data.setAttribute("path_forum_title", "href", "forum.xhtml?forum_id=" + topic.getForum().getId());

				//
				data.setContent("topic_title", StringEscapeUtils.escapeXml(topic.getTitle()));

				//
				if ((account != null) && (account.isRole(KiwaRoles.WEBMASTER)))
				{
					data.setAttribute("topic_edition_id", "value", topic.getId());
					data.setAttribute("button_topic_edition", "value", "Edit Topic");
					data.setAttribute("remove_topic_id", "value", topic.getId());
					data.setAttribute("button_remove_topic", "value", "Remove Topic");
				}
				else
				{
					data.setContent("webmaster", "");
				}

				//
				int lineIndex = 0;
				for (Message message : topic.messages())
				{
					//
					if (lineIndex == 0)
					{
						//
						if (topic.isNotSticky())
						{
							data.setContent("message", lineIndex, "topic_sticky", "");
						}

						//
						if (topic.isOpened())
						{
							data.setContent("message", lineIndex, "topic_locked", "");
						}

						//
						data.setContent("message", lineIndex, "topic_title2", StringEscapeUtils.escapeXml(topic.getTitle()));

					}
					else
					{
						//
						data.setContent("message", lineIndex, "first_message_title", "");
					}

					//
					Account creator = kiwa.accountManager().getAccountById(message.getAuthorId());
					if (creator == null)
					{
						data.setContent("message", lineIndex, "message_author", StringEscapeUtils.escapeXml(message.getAuthorName()));
					}
					else
					{
						data.setContent("message", lineIndex, "message_author_link", StringEscapeUtils.escapeXml(message.getAuthorName()));
						data.setAttribute("message", lineIndex, "message_author_link", "href", "/browser/contributor.xhtml?contributor_id=" + message.getAuthorId());
					}

					data.setContent("message", lineIndex, "message_text", KiwaUtils.htmlize(StringEscapeUtils.escapeXml(message.getText())));
					data.setContent("message", lineIndex, "message_date", message.getEditionDate().toString("dd/MM/yyyy HH:mm"));

					if ((account != null) && ((account.isRole(KiwaRoles.WEBMASTER)) || (account.getId() == message.getAuthorId())))
					{
						data.setAttribute("message", lineIndex, "message_edition", "href", "message_edition.xhtml?message_id=" + message.getId());
					}
					else
					{
						data.setAttribute("message", lineIndex, "message_edition", "class", "xid:nodisplay");
					}

					if ((lineIndex != 0) && (account != null) && (account.isRole(KiwaRoles.WEBMASTER)))
					{
						data.setAttribute("message", lineIndex, "remove_message", "href", "remove_message.xhtml?message_id=" + message.getId());
					}
					else
					{
						data.setAttribute("message", lineIndex, "remove_message", "class", "xid:nodisplay");
					}

					//
					lineIndex += 1;
				}

				//
				if ((account == null) || (topic.isLocked()))
				{
					data.setContent("reply", "");
				}
				else
				{
					data.setAttribute("topic_id", "value", topic.getId());
					data.setAttribute("button_reply", "value", "Reply");
					data.setContent("tips", "(5000 car.)");
				}

				//
				data.setAttribute("back", "href", "forum.xhtml?forum_id=" + topic.getForum().getId());

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.FORUM, Account.getId(account), locale, content);

				// Send response.
				// ==============
				// response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
