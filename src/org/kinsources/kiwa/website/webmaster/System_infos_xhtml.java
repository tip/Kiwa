/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.webmaster;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.SimpleChronometer;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class System_infos_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(System_infos_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/webmaster/system_infos.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.webmaster.kiwa_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if ((account == null) || (!account.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters
				// ==============

				// Use parameters
				// ==============
				//
				TagDataManager data = new TagDataManager();

				// Kiwa version.
				data.setContent("version_label", "Version");
				data.setContent("version_value", kiwa.buildInformation().version());
				data.setContent("build_date_label", "Build date");
				data.setContent("build_date_value", kiwa.buildInformation().buildDate());
				data.setContent("generator_label", "Generator");
				data.setContent("generator_value", kiwa.buildInformation().generator());
				data.setContent("author_label", "Author");
				data.setContent("author_value", kiwa.buildInformation().author());

				// Kiwa Environment.
				data.setContent("environment_name_label", "Name");
				data.setContent("environment_name_value", XMLTools.escapeXmlBlank(kiwa.environmentInformation().name()));
				data.setContent("environment_website_url_label", "Website URL");
				data.setContent("environment_website_url_value", kiwa.environmentInformation().websiteUrl());
				data.setContent("environment_eucles_time_pause_label", "Eucles time pause");
				data.setContent("environment_eucles_time_pause_value", kiwa.environmentInformation().mailPauseTime());
				data.setContent("environment_log4j_File_Pathname_label", "Log4j file pathname");
				data.setContent("environment_log4j_File_Pathname_value", kiwa.environmentInformation().log4jFilePathname());
				data.setContent("environment_jdbc_settings_label", "JDBC settings");
				data.setContent("environment_jdbc_settings_value", XMLTools.escapeXmlBlank(kiwa.environmentInformation().jdbcSettings()).replace(",", "<br/>"));
				data.setContent("environment_mail_settings_label", "Mail settings");
				data.setContent("environment_mail_settings_value", XMLTools.escapeXmlBlank(kiwa.environmentInformation().mailSettings()).replace(",", "<br/>"));

				// Host.
				data.setContent("host_local_name", XMLTools.escapeXmlBlank(InetAddress.getLocalHost().getHostName()));
				data.setContent("host_cpu_count", Runtime.getRuntime().availableProcessors());
				data.setContent("os_name", SystemUtils.OS_NAME);
				data.setContent("os_arch", SystemUtils.OS_ARCH);
				data.setContent("os_version", SystemUtils.OS_VERSION);
				data.setContent("host_local_ip", InetAddress.getLocalHost().getHostAddress());
				data.setContent("host_proc_version", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(StringUtils.defaultString(KiwaUtils.readProcVersion(), "n/a"))));
				data.setContent("host_proc_cpuinfo0", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(StringUtils.defaultString(KiwaUtils.readProcCpuinfo0(), "n/a"))));
				data.setContent("host_proc_meminfo", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(StringUtils.defaultString(KiwaUtils.readProcMeminfo6(), "n/a"))));

				// Memory.
				long maxMemory = Runtime.getRuntime().maxMemory();
				if (maxMemory == Long.MAX_VALUE)
				{
					data.setContent("java_vm_maxmemory", "no limit");
					data.setContent("java_vm_maxmemory_mb", "");
				}
				else
				{
					data.setContent("java_vm_maxmemory", maxMemory + " bytes");
					data.setContent("java_vm_maxmemory_mb", String.format("%,.02f MB", maxMemory / 1024.0 / 1024.0));
				}

				long freeMemory = Runtime.getRuntime().freeMemory();
				data.setContent("java_vm_freememory", freeMemory + " bytes");
				data.setContent("java_vm_freememory_mb", String.format("%,.02f MB", freeMemory / 1024.0 / 1024.0));

				long totalMemory = Runtime.getRuntime().totalMemory();
				data.setContent("java_vm_totalmemory", totalMemory + " bytes");
				data.setContent("java_vm_totalmemory_mb", String.format("%,.02f MB", totalMemory / 1024.0 / 1024.0));
				data.setContent("java_vm_heap", ManagementFactory.getMemoryMXBean().getHeapMemoryUsage() + " bytes");
				data.setContent("java_vm_nonheap", ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage() + " bytes");

				// JVM life.
				data.setContent("java_vm_starttime", DateFormatUtils.format(ManagementFactory.getRuntimeMXBean().getStartTime(), DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern()));
				data.setContent("java_vm_uptime", SimpleChronometer.shortHumanString(ManagementFactory.getRuntimeMXBean().getUptime()));
				data.setContent("os_loadaverage", String.valueOf(ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage()));

				// Java infos.
				data.setContent("java_classpath", SystemUtils.JAVA_CLASS_PATH);
				data.setContent("java_class_version", SystemUtils.JAVA_CLASS_VERSION);
				data.setContent("java_compiler", SystemUtils.JAVA_COMPILER);
				data.setContent("java_home", SystemUtils.JAVA_HOME);
				data.setContent("java_io_tmpdir", SystemUtils.JAVA_IO_TMPDIR);
				data.setContent("java_runtime_name", SystemUtils.JAVA_RUNTIME_NAME);
				data.setContent("java_runtime_version", SystemUtils.JAVA_RUNTIME_VERSION);
				data.setContent("java_specification_name", SystemUtils.JAVA_SPECIFICATION_NAME);
				data.setContent("java_specification_vendor", SystemUtils.JAVA_SPECIFICATION_VENDOR);
				data.setContent("java_specification_version", SystemUtils.JAVA_SPECIFICATION_VERSION);
				data.setContent("java_vendor", SystemUtils.JAVA_VENDOR);
				data.setContent("java_vendor_url", SystemUtils.JAVA_VENDOR_URL);
				data.setContent("java_version", SystemUtils.JAVA_VERSION);

				// JVM infos.
				data.setContent("java_vm_info", SystemUtils.JAVA_VM_INFO);
				data.setContent("java_vm_name", SystemUtils.JAVA_VM_NAME);
				data.setContent("java_vm_specification_name", SystemUtils.JAVA_VM_SPECIFICATION_NAME);
				data.setContent("java_vm_specification_vendor", SystemUtils.JAVA_VM_SPECIFICATION_VENDOR);
				data.setContent("java_vm_specification_version", SystemUtils.JAVA_VM_SPECIFICATION_VERSION);
				data.setContent("java_vm_vendor", SystemUtils.JAVA_VM_VENDOR);
				data.setContent("java_vm_version", SystemUtils.JAVA_VM_VERSION);

				// Tomcat infos.
				data.setContent("tomcat_info", StringUtils.defaultString(org.apache.catalina.util.ServerInfo.getServerInfo(), "n/a"));
				data.setContent("tomcat_number", StringUtils.defaultString(org.apache.catalina.util.ServerInfo.getServerNumber(), "n/a"));
				data.setContent("tomcat_built", StringUtils.defaultString(org.apache.catalina.util.ServerInfo.getServerBuilt(), "n/a"));
				data.setContent("tomcat_catalina_home", StringUtils.defaultString(org.apache.catalina.startup.Bootstrap.getCatalinaHome(), "n/a"));
				data.setContent("tomcat_catalina_base", StringUtils.defaultString(org.apache.catalina.startup.Bootstrap.getCatalinaBase(), "n/a"));
				data.setContent("tomcat_serverxml_path", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(StringUtils.defaultString(KiwaUtils.getServerXmlPath(), "n/a"))));
				data.setContent("tomcat_serverxml_content", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(StringUtils.defaultString(KiwaUtils.readServerXml(), "n/a"))));

				// User infos.
				data.setContent("java_user_country", SystemUtils.USER_COUNTRY);
				data.setContent("java_user_dir", SystemUtils.USER_DIR);
				data.setContent("java_user_home", SystemUtils.USER_HOME);
				data.setContent("java_user_language", SystemUtils.USER_LANGUAGE);
				data.setContent("java_user_name", SystemUtils.USER_NAME);
				data.setContent("java_user_timezone", SystemUtils.USER_TIMEZONE);

				// Network infos.
				data.setContent("network_ip_address", InetAddress.getLocalHost().getHostAddress());
				String ipAdressInfos = KiwaUtils.ipAddressInfos();
				ipAdressInfos = ipAdressInfos.replace(";", "<br/>");
				ipAdressInfos = ipAdressInfos.replaceAll(" mac", "<br/>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;mac");
				ipAdressInfos = ipAdressInfos.replace(",", "<br/>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;");
				data.setContent("network_interfaces", ipAdressInfos);

				// WAR File infos.
				data.setContent("class_compiler_version", StringUtils.defaultString(KiwaUtils.classCompilerVersion(), "n/a"));

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(account.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
