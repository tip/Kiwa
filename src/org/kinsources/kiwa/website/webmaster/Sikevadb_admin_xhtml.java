/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.webmaster;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.sikevadb.SikevaDB;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Sikevadb_admin_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Sikevadb_admin_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/webmaster/sikevadb_admin.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.webmaster.sikeva_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if ((account == null) || (!account.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters
				// ==============

				// Use parameters
				// ==============
				//
				TagDataManager data = new TagDataManager();

				//
				SikevaDB database = kiwa.storer().database();

				//
				data.setContent("memory_size_all", String.format("%,.02f MB", database.memorySizeOfAll() / 1024.0 / 1024.0));
				data.setContent("memory_size_active", String.format("%,.02f MB", database.memorySize() / 1024.0 / 1024.0));
				data.setContent("memory_size_archived", String.format("%,.02f MB", database.memorySizeOfArchive() / 1024.0 / 1024.0));

				//
				int index = 0;
				for (String key : database.getAllKeys().sort())
				{
					//
					data.setContent("count", index, "label", key);
					data.setContent("count", index, "active_count", String.format(locale, "%,d", database.countOfElements(key)));
					data.setContent("count", index, "archived_count", String.format(locale, "%,d", database.countOfArchivedElements(key)));
					data.setContent("count", index, "all_count", String.format(locale, "%,d", database.countOfAllElements(key)));
					data.setContent("count", index, "active_size", String.format(locale, "%,d", database.memorySize(key)));
					data.setContent("count", index, "archived_size", String.format(locale, "%,d", database.memorySizeOfArchive(key)));
					data.setContent("count", index, "all_size", String.format(locale, "%,d", database.memorySizeOfAll(key)));

					//
					index += 1;
				}

				//
				data.setContent("total_label", "All");
				data.setContent("total_active_count", String.format(locale, "%,d", database.countOfElements()));
				data.setContent("total_archived_count", String.format(locale, "%,d", database.countOfArchivedElements()));
				data.setContent("total_all_count", String.format(locale, "%,d", database.countOfAllElements()));
				data.setContent("total_active_size", String.format(locale, "%,d", database.memorySize()));
				data.setContent("total_archived_size", String.format(locale, "%,d", database.memorySizeOfArchive()));
				data.setContent("total_all_size", String.format(locale, "%,d", database.memorySizeOfAll()));

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(account.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
