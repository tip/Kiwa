/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actulog;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Wire_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Wire_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/actulog/wire.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.actulog.wire", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Get parameters.
			// ===============
			String wireId = SimpleServletDispatcher.shortRewritedUrlParameter(request);
			logger.info("wireId=[{}]", wireId);

			// Use parameters.
			// ===============
			Wire wire = kiwa.actulog().getWireById(wireId);
			if (wire == null)
			{
				throw new IllegalArgumentException("Unknown wire.");
			}
			else if (wire.isNotPublished())
			{
				throw new IllegalArgumentException("Not published wire.");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("wire_title", StringEscapeUtils.escapeXml(wire.getTitle()));
				data.setContent("wire_author", StringEscapeUtils.escapeXml(wire.getAuthor()));
				data.setContent("wire_date", wire.getPublicationDate().toString("dd/MM/yyyy"));
				if (wire.getLeadParagraph() != null)
				{
					data.setContent("wire_lead_paragraph", KiwaUtils.htmlizeWithoutBR(wire.getLeadParagraph()));
				}
				if (wire.getBody() != null)
				{
					data.setContent("wire_body", KiwaUtils.htmlizeWithoutBR(wire.getBody()));
				}

				//
				StringBuffer content = xidyn.dynamize(data);
				StringBuffer page = kiwa.getCharterView().getHtml(accountId, locale, content.toString());

				// Send response.
				// ==============
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(page);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
