/**
 * Copyright 2013-2019 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.charter;

import java.util.Locale;

import org.apache.commons.lang3.ArrayUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.Kiwa.Status;
import org.kinsources.kiwa.kernel.KiwaRoles;

import fr.devinsy.xidyn.data.SimpleTagData;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class KiwaCharterView
{
	public enum Menu
	{
		NONE,
		HOME,
		FORUM,
		ABOUT,
		HOW_TO,
		MY_SPACE,
		BROWSER,
		SEARCH
	}

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(KiwaCharterView.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/charter/kiwa_charter_view.html");
	private static URLPresenter maintenance = new URLPresenter("/org/kinsources/kiwa/website/charter/maintenance.html");

	private static Kiwa kiwa = Kiwa.instance();

	// static private FilePresenter xidyn = new
	// FilePresenter(Kiwa.instance().getWebContentPath() +
	// "/charter/charter.html");

	/**
	 * 
	 */
	public StringBuffer getHtml() throws Exception
	{
		return xidyn.dynamize();
	}

	/**
	 * 
	 */
	public StringBuffer getHtml(final Long accountId, final Locale locale, final CharSequence content) throws Exception
	{
		StringBuffer result;

		result = getHtml(Menu.NONE, accountId, locale, content);

		//
		return result;
	}

	/**
	 * 
	 */
	public StringBuffer getHtml(final Menu menu, final Long accountId, final Locale locale, final CharSequence content) throws Exception
	{
		StringBuffer result;

		//
		Account account = kiwa.accountManager().getAccountById(accountId);

		//
		if ((kiwa.getStatus() == Status.MAINTENANCE) && ((account == null) || (account.isNotRole(KiwaRoles.WEBMASTER))))
		{
			result = new StringBuffer(maintenance.toString());
		}
		else
		{
			//
			TagDataManager data = new TagDataManager();

			// Extract start of content.
			String contentStart;
			if (content == null)
			{
				contentStart = null;
			}
			else if (content.length() < 7)
			{
				contentStart = content.toString();
			}
			else
			{
				contentStart = content.subSequence(0, 6).toString();
			}

			// Set content.
			String fixedContent;
			if ((contentStart != null) && ((contentStart.startsWith("<?")) || (contentStart.startsWith("<!")) || (contentStart.startsWith("<html>"))))
			{
				fixedContent = XidynUtils.extractBodyContent(content);
			}
			else
			{
				fixedContent = content.toString();
			}

			data.setContent("content_zone", fixedContent);

			//
			if (account == null)
			{
				//
				data.setContent("charter_authenticated", "");
				data.setContent("charter_webmaster", "");
				data.setContent("charter_sciboarder", "");
			}
			else
			{
				//
				data.setContent("charter_anonymous", "");
				data.setContent("charter_fullname", account.getFullName());
				data.setContent("charter_logout", "Log out");

				if (account.isRole(KiwaRoles.WEBMASTER))
				{
					data.setContent("charter_webmaster_board", "Webmaster board");
				}
				else
				{
					data.setContent("charter_webmaster", "");
				}

				//
				if ((account.isRole(KiwaRoles.SCIBOARDER)) || (account.isRole(KiwaRoles.WEBMASTER)))
				{
					data.setContent("charter_sciboarder_board", "Sciboarder board");
				}
				else
				{
					data.setContent("charter_sciboarder", "");
				}
			}

			//
			logger.debug("locale=[{}]", locale);
			data.setIterationStrategy("charter_language", SimpleTagData.IterationStrategy.ONLY_ROWS_WITH_ID);

			for (int index = 0; index < kiwa.languages().length; index++)
			{
				data.setAttribute("charter_language_option", index, "value", kiwa.languages()[index].toString());
				data.setContent("charter_language_option", index, kiwa.languages()[index].toString());
			}

			int languageIndex;
			if (locale == null)
			{
				languageIndex = ArrayUtils.INDEX_NOT_FOUND;
			}
			else
			{
				languageIndex = ArrayUtils.indexOf(kiwa.languages(), locale.getLanguage());
			}

			if (languageIndex == ArrayUtils.INDEX_NOT_FOUND)
			{
				data.setAttribute("charter_language_option", 0, "selected", "selected");
			}
			else
			{
				data.setAttribute("charter_language_option", languageIndex, "selected", "selected");
			}

			//
			switch (menu)
			{
				case BROWSER:
					data.setAttribute("menu_browser", "class", "active");
				break;
				case ABOUT:
					data.setAttribute("menu_about", "class", "active");
				break;
				case FORUM:
					data.setAttribute("menu_forums", "class", "active");
				break;
				case HOME:
					data.setAttribute("menu_home", "class", "active");
				break;
				case HOW_TO:
					data.setAttribute("menu_how_to", "class", "active");
				break;
				case SEARCH:
					data.setAttribute("menu_search", "class", "active");
				break;
				case MY_SPACE:
					data.setAttribute("menu_my_space", "class", "active");
				break;
				case NONE:
				default:
				break;
			}

			//
			data.setContent("charter_login", "Login");
			data.setContent("charter_register", "Register");
			data.setContent("charter_about_kinsources", "About Kinsources");
			data.setContent("charter_how_to_use", "How to use Kinsources");
			data.setContent("charter_partners", "Partners");
			data.setContent("charter_contact_us", "Contact us");
			data.setContent("charter_privacy", "Privacy");
			data.setContent("charter_legal", "Legal");
			data.setContent("charter_terms_of_use", "Terms of use");
			data.setContent("charter_sitemap", "Sitemap");
			data.setContent("charter_copyright", "Copyright 2013–2019 Kinsources – v" + kiwa.buildInformation().version());

			//
			result = xidyn.dynamize(data);
		}

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
