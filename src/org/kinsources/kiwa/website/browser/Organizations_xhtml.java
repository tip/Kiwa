/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.browser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.utils.BiWeightedString;
import org.kinsources.kiwa.utils.BiWeightedStrings;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Organizations_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Organizations_xhtml.class);
	private static BrowserView browserView = new BrowserView();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/browser/organizations.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{

		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.browser.organization", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============

			// Use parameters.
			// ===============
			HashMap<String, Double> contributors = new HashMap<String, Double>();
			HashMap<String, Double> datasets = new HashMap<String, Double>();
			for (Contributor contributor : kiwa.kidarep().findContributors(Account.Status.ACTIVATED))
			{
				//
				String organizationName = contributor.account().getOrganization();

				//
				if (organizationName != null)
				{
					//
					Double contributorCount = contributors.get(organizationName);
					if (contributorCount == null)
					{
						contributorCount = 0.0;
					}
					contributorCount += 1;
					contributors.put(organizationName, contributorCount);

					//
					Double datasetCount = datasets.get(organizationName);
					if (datasetCount == null)
					{
						datasetCount = 0.0;
					}
					datasetCount += contributor.personalDatasets().find(Dataset.Status.VALIDATED).size();
					datasets.put(organizationName, datasetCount);
				}
			}

			//
			BiWeightedStrings target = new BiWeightedStrings(contributors.size());
			for (String key : contributors.keySet())
			{
				target.add(new BiWeightedString(key, contributors.get(key), datasets.get(key)));
			}

			//
			target.sortByName();

			// Send response.
			// ==============
			TagDataManager data = new TagDataManager();

			//
			if (target.isEmpty())
			{
				data.setContent("organization", "<td colspan=\"10\">No data.</td>");
			}
			else
			{
				int lineIndex = 0;
				for (BiWeightedString item : target)
				{
					//
					data.setContent("organization", lineIndex, "organization_index", lineIndex + 1);
					data.setContent("organization", lineIndex, "organization_name", XMLTools.escapeXmlBlank(item.getString()));
					data.setAttribute("organization", lineIndex, "organization_name", "href", "organization.xhtml?name=" + XMLTools.escapeXmlBlank(item.getString()));

					data.setContent("organization", lineIndex, "organization_contributor_count", String.valueOf((long) (double) item.getWeight1()));
					data.setContent("organization", lineIndex, "organization_dataset_count", String.valueOf((long) (double) item.getWeight2()));
					data.setAttribute("organization", lineIndex, "field_dataset_count", "href", "organization.xhtml?name=" + XMLTools.escapeXmlBlank(item.getString()));

					//
					lineIndex += 1;
				}
			}

			//
			StringBuffer content = xidyn.dynamize(data);

			//
			StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.BROWSER, accountId, locale, browserView.getHtml(BrowserView.Menu.ALL_ORGANIZATIONS, content, locale, account));

			// Display page.
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
