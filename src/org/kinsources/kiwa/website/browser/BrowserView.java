/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.browser;

import java.util.Locale;

import org.kinsources.kiwa.accounts.Account;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class BrowserView
{
	public enum Menu
	{
		NONE,
		MY_DATASETS,
		ALL_DATASETS,
		ALL_CONTRIBUTORS,
		ALL_AUTHORS,
		ALL_ORGANIZATIONS,
		ALL_FIELDS,
		ALL_STATISTICS
	}

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BrowserView.class);

	private static TranslatorPresenter page = new TranslatorPresenter("/org/kinsources/kiwa/website/browser/browser_view.html");

	/**
	 * 
	 */
	public StringBuffer getHtml(final Menu menu, final CharSequence content, final Locale locale, final Account account) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		//
		if (account == null)
		{
			data.setContent("my_datasets_item", "");
		}

		//
		switch (menu)
		{
			case ALL_AUTHORS:
				data.setAttribute("authors", "class", "active");
			break;
			case ALL_CONTRIBUTORS:
				data.setAttribute("contributors", "class", "active");
			break;
			case ALL_DATASETS:
				data.setAttribute("datasets", "class", "active");
			break;
			case ALL_FIELDS:
				data.setAttribute("fields", "class", "active");
			break;
			case ALL_ORGANIZATIONS:
				data.setAttribute("organizations", "class", "active");
			break;
			case ALL_STATISTICS:
				data.setAttribute("statistics", "class", "active");
			break;
			case MY_DATASETS:
				data.setAttribute("my_datasets", "class", "active");
			break;
			default:
		}

		//
		data.setContent("browser_zone", XidynUtils.extractBodyContent(content));

		//
		result = page.dynamize(data);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
