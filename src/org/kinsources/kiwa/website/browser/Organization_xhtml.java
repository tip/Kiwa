/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.browser;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Contributors;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Organization_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Organization_xhtml.class);
	private static BrowserView browserView = new BrowserView();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/browser/organization.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.browser.organization", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String organizationName = request.getParameter("name");
			logger.info("organizationName=[{}]", organizationName);

			// Use parameters.
			// ===============
			if (organizationName == null)
			{
				throw new IllegalArgumentException("Invalid name.");
			}
			else
			{
				Contributors contributors = new Contributors();
				Datasets datasets = new Datasets();
				for (Contributor contributor : kiwa.kidarep().findContributors(Account.Status.ACTIVATED))
				{
					//
					String name = contributor.account().getOrganization();

					//
					if (StringUtils.equals(name, organizationName))
					{
						//
						contributors.add(contributor);

						//
						datasets.addAll(contributor.personalDatasets().find(Dataset.Status.VALIDATED));
					}
				}

				// Send response.
				// ==============
				TagDataManager data = new TagDataManager();

				//
				data.setContent("title_organization_name", XMLTools.escapeXmlBlank(organizationName));
				data.setContent("contributor_count", contributors.countOfContributors());

				if (contributors.isEmpty())
				{
					//
					data.setContent("contributor", "<td colspan=\"10\">No contributor.</td>");
				}
				else
				{
					//
					contributors.sortByFullName();

					//
					int lineIndex = 0;
					for (Contributor contributor : contributors)
					{
						//
						data.setContent("contributor", lineIndex, "contributor_index", lineIndex + 1);
						data.setContent("contributor", lineIndex, "contributor_id", contributor.getId());
						data.setContent("contributor", lineIndex, "contributor_name", XMLTools.escapeXmlBlank(contributor.getFullName()));
						data.setAttribute("contributor", lineIndex, "contributor_name", "href", "contributor.xhtml?contributor_id=" + contributor.getId());
						data.setContent("contributor", lineIndex, "contributor_dataset_count", contributor.countOfDatasets());

						//
						lineIndex += 1;
					}
				}

				//
				data.setContent("dataset_count", datasets.countOfDatasets());

				if (datasets.isEmpty())
				{
					//
					data.setContent("dataset", "<td colspan=\"10\">No dataset.</td>");
				}
				else
				{
					//
					datasets.sortByName();

					//
					int lineIndex = 0;
					for (Dataset dataset : datasets)
					{
						//
						data.setContent("dataset", lineIndex, "dataset_index", lineIndex + 1);
						data.setContent("dataset", lineIndex, "dataset_id", dataset.getId());

						data.setContent("dataset", lineIndex, "dataset_name", XMLTools.escapeXmlBlank(dataset.getName()));
						data.setAttribute("dataset", lineIndex, "dataset_name", "href", kiwa.permanentURI(dataset));

						if (StringUtils.isBlank(dataset.getAuthor()))
						{
							data.setContent("dataset", lineIndex, "dataset_author", "");
						}
						else
						{
							data.setContent("dataset", lineIndex, "dataset_author_link", XMLTools.escapeXmlBlank(dataset.getAuthor()));
							data.setAttribute("dataset", lineIndex, "dataset_author_link", "href", "field_value.xhtml?name=AUTHOR&amp;value=" + dataset.getAuthor());
						}

						if (StringUtils.isBlank(dataset.getRegion()))
						{
							data.setContent("dataset", lineIndex, "dataset_region", "");
						}
						else
						{
							data.setContent("dataset", lineIndex, "dataset_region_link", XMLTools.escapeXmlBlank(dataset.getRegion()));
							data.setAttribute("dataset", lineIndex, "dataset_region_link", "href", "field_value.xhtml?name=REGION&amp;value=" + dataset.getRegion());
						}

						data.setContent("dataset", lineIndex, "dataset_contributor", XMLTools.escapeXmlBlank(dataset.getContributor().getFullName()));
						data.setAttribute("dataset", lineIndex, "dataset_contributor", "href", "contributor.xhtml?contributor_id=" + dataset.getContributor().getId());

						//
						lineIndex += 1;
					}
				}

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.BROWSER, accountId, locale, browserView.getHtml(BrowserView.Menu.ALL_ORGANIZATIONS, content, locale, account));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
