/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.sciboard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.kinsources.kiwa.website.myspace.MySpaceView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Request_cancel_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Request_cancel_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/sciboard/request_cancel.html");
	private static final Kiwa kiwa = Kiwa.instance();
	private static MySpaceView mySpaceView = new MySpaceView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{

		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.sciboard.request_cancel", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String requestId = request.getParameter("request_id");
			logger.info("requestId=[{}]", requestId);

			// Use parameters.
			// ===============
			Request query = kiwa.sciboard().getRequestById(requestId);

			// Send response.
			// ==============
			TagDataManager data = new TagDataManager();

			//
			if (query == null)
			{
				throw new IllegalArgumentException("Unknown request.");
			}
			else if ((account == null) || (account.getId() != query.getContributorId()))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				//
				data.setAttribute("request_id", "value", query.getId());

				data.setContent("id_label", "Request ID:");
				data.setContent("id", query.getId());

				data.setContent("author_label", "Author:");
				data.setContent("author", kiwa.accountManager().getAccountById(query.getContributorId()).getFullName());

				data.setContent("creation_date_label", "Creation date:");
				data.setContent("creation_date", query.getCreationDate().toString("dd/MM/yyyy"));

				data.setContent("type_label", "Type:");
				data.setContent("type", KiwaUtils.humanName(query.getType()));

				data.setContent("description_label", "Description:");
				data.setContent("description", kiwa.buildRequestDescription(query));

				data.setContent("contributor_comment_label", "Comment:");
				data.setContent("contributor_comment", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(query.getContributorComment())));

				data.setContent("status_label", "Status:");
				data.setContent("status", KiwaUtils.humanName(query.getStatus()));

				data.setContent("comment_label", "Motivation:");

				//
				data.setContent("back_button", "Back");
				data.setAttribute("back_button", "href", "my_request.xhtml?request_id=" + query.getId());

				data.setContent("cancel_request_button", "Confirm cancel request");

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, mySpaceView.getHtml(MySpaceView.Menu.MY_REQUESTS, content, locale));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
