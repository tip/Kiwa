/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.sciboard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.sciboard.Comment;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.sciboard.Requests;
import org.kinsources.kiwa.sciboard.Vote;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Request_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Request_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/sciboard/request.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.sciboard.request", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String requestId = request.getParameter("request_id");
			logger.info("requestId=[{}]", requestId);

			// Use parameters.
			// ===============
			Request currentQuery = kiwa.sciboard().getRequestById(requestId);

			// Send response.
			// ==============
			TagDataManager data = new TagDataManager();

			//
			if (currentQuery == null)
			{
				throw new IllegalArgumentException("Unknown request.");
			}
			else if ((!kiwa.isSciboarder(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				//
				data.setContent("id_label", "Request ID:");
				data.setContent("id", currentQuery.getId());

				data.setContent("author_label", "Requester:");
				data.setContent("author", kiwa.accountManager().getAccountById(currentQuery.getContributorId()).getFullName());

				data.setContent("creation_date_label", "Creation date:");
				data.setContent("creation_date", currentQuery.getCreationDate().toString("dd/MM/yyyy HH:mm"));

				data.setContent("type_label", "Type:");
				data.setContent("type", KiwaUtils.humanName(currentQuery.getType()));

				data.setContent("description_label", "Description:");
				data.setContent("description", kiwa.buildRequestDescription(currentQuery));

				if (((currentQuery.getType() == Request.Type.DATASET_PUBLISH) || (currentQuery.getType() == Request.Type.DATASET_UNPUBLISH))
						&& ((currentQuery.getStatus() == Request.Status.OPENED) || (currentQuery.getStatus() == Request.Status.ACCEPTED)))
				{
					//
					String datasetId = currentQuery.properties().getProperty("dataset_id");
					Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

					//
					if (dataset == null)
					{
						data.setAttribute("dataset_link", "class", "xid:nodisplay");
					}
					else
					{
						data.setContent("dataset_link", "see dataset");
						data.setAttribute("dataset_link", "href", kiwa.permanentURI(dataset));
					}
				}
				else
				{
					data.setAttribute("dataset_link", "class", "xid:nodisplay");
				}

				data.setContent("contributor_comment_label", "Comment:");
				data.setContent("contributor_comment", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(currentQuery.getContributorComment())));

				data.setContent("status_label", "Status:");
				data.setContent("status", KiwaUtils.humanName(currentQuery.getStatus()));
				data.setContent("decision_date_label", "Decision date:");
				data.setContent("decision_comment_label", "Decision note:");
				if (currentQuery.getStatus() == Request.Status.OPENED)
				{
					data.setContent("decision_date", "n/a");
					data.setContent("decision_comment", "n/a");
				}
				else
				{
					data.setContent("decision_date", currentQuery.getDecisionDate().toString("dd/MM/yyyy hh'h'mm"));
					data.setContent("decision_comment", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(currentQuery.getDecisionComment())));
				}

				// Request history.
				Requests otherQueries = kiwa.sciboard().findRequestsByDatasetId(currentQuery.properties().getProperty("dataset_id"));

				if (otherQueries.isEmpty())
				{
					data.setContent("history_row", "<td colspan=\"10\">No history.</td>");
				}
				else
				{
					// Fill history table.
					int rowCount = 0;
					for (Request otherQuery : otherQueries)
					{
						data.setContent("history_row", rowCount, "history_id", otherQuery.getId());
						data.setAttribute("history_row", rowCount, "history_id", "href", "request.xhtml?request_id=" + otherQuery.getId());
						data.setContent("history_row", rowCount, "history_type", KiwaUtils.humanName(otherQuery.getType()));
						data.setContent("history_row", rowCount, "history_requester", kiwa.accountManager().getAccountById(otherQuery.getContributorId()).getFullName());
						data.setContent("history_row", rowCount, "history_comments", otherQuery.comments().size());
						data.setContent("history_row", rowCount, "history_status", KiwaUtils.humanName(otherQuery.getStatus()));

						if (currentQuery.getDecisionDate() == null)
						{
							data.setContent("history_row", rowCount, "history_date", currentQuery.getCreationDate().toString("dd/MM/yyyy HH:mm"));
						}
						else
						{
							data.setContent("history_row", rowCount, "history_date", currentQuery.getDecisionDate().toString("dd/MM/yyyy HH:mm"));
						}

						rowCount += 1;
					}

					// Fill previous comments table.
					Requests previousQueries = new Requests();
					for (Request otherQuery : otherQueries)
					{
						if (otherQuery.getId() < currentQuery.getId())
						{
							previousQueries.add(otherQuery);
						}
					}

					if (previousQueries.isEmpty())
					{
						data.setContent("previous_comment", "<td colspan=\"10\">No previous comment.</td>");
					}
					else
					{
						int lineIndex = 0;
						for (Request previousQuery : previousQueries)
						{
							for (Comment comment : previousQuery.comments().copy())
							{
								//
								data.setContent("previous_comment", lineIndex, "previous_comment_author", kiwa.accountManager().getAccountById(comment.getAuthorId()).getFullName());
								data.setContent("previous_comment", lineIndex, "previous_comment_date", comment.getCreationDate().toString("dd/MM/yyyy HH:mm"));
								data.setContent("previous_comment", lineIndex, "previous_comment_text", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(comment.getText())));
								data.setContent("previous_comment", lineIndex, "previous_comment_request", previousQuery.getId());

								//
								lineIndex += 1;
							}

							//
							data.setContent("previous_comment", lineIndex, "previous_comment_author", kiwa.accountManager().getAccountById(previousQuery.getDecisionMakerId()).getFullName());
							data.setContent("previous_comment", lineIndex, "previous_comment_date", previousQuery.getCreationDate().toString("dd/MM/yyyy HH:mm"));
							StringList decisionComment = new StringList();
							decisionComment.append("Request status is changed to: ").append(previousQuery.getStatus().toString()).appendln(".<br/>");
							decisionComment.appendln(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(previousQuery.getDecisionComment())));
							data.setContent("previous_comment", lineIndex, "previous_comment_text", decisionComment.toString());
							data.setContent("previous_comment", lineIndex, "previous_comment_request", previousQuery.getId());

							//
							lineIndex += 1;
						}
					}
				}

				// Votes.
				if (currentQuery.votes().isEmpty())
				{
					data.setContent("vote", "<td colspan=\"10\">No vote.</td>");
				}
				else
				{
					int lineIndex = 0;
					for (Vote vote : currentQuery.votes().copy())
					{
						//
						data.setContent("vote", lineIndex, "vote_boarder_name", kiwa.accountManager().getAccountById(vote.getOwnerId()).getFullName());
						data.setContent("vote", lineIndex, "vote_boarder_value", KiwaUtils.humanName(vote.getValue()));
						data.setContent("vote", lineIndex, "vote_boarder_date", vote.getEditionDate().toString("dd/MM/yyyy HH:mm"));

						//
						lineIndex += 1;
					}
				}

				// Set my vote.
				if (currentQuery.getStatus() == Request.Status.OPENED)
				{
					data.setAttribute("vote_set_agree", "href", "set_my_vote.xhtml?request_id=" + currentQuery.getId() + "&amp;vote=AGREE#request_history");
					data.setAttribute("vote_set_dubious", "href", "set_my_vote.xhtml?request_id=" + currentQuery.getId() + "&amp;vote=DUBIOUS#request_history");
					data.setAttribute("vote_set_disagree", "href", "set_my_vote.xhtml?request_id=" + currentQuery.getId() + "&amp;vote=DISAGREE#request_history");
				}
				else
				{
					data.setContent("set_my_vote", "");
				}

				// Comments.
				if (currentQuery.comments().isEmpty())
				{
					data.setContent("comment", "<td colspan=\"10\">No comment.</td>");
				}
				else
				{
					int lineIndex = 0;
					for (Comment comment : currentQuery.comments().copy())
					{
						//
						data.setContent("comment", lineIndex, "comment_author", kiwa.accountManager().getAccountById(comment.getAuthorId()).getFullName());
						data.setContent("comment", lineIndex, "comment_date", comment.getCreationDate().toString("dd/MM/yyyy HH:mm"));
						data.setContent("comment", lineIndex, "comment_text", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(comment.getText())));

						//
						lineIndex += 1;
					}
				}

				// Add comment.
				if (currentQuery.getStatus() == Request.Status.OPENED)
				{
					data.setAttribute("add_comment_request_id", "value", currentQuery.getId());
					data.setContent("add_comment_label", "Add comment:");
					data.setAttribute("add_comment_button", "value", "Add comment");

				}
				else
				{
					data.setContent("add_comment", "");
				}

				// Decision pending.
				if ((currentQuery.getStatus() == Request.Status.OPENED) && (kiwa.isScichief(account)))
				{
					data.setAttribute("decision_pending_request_id", "value", currentQuery.getId());
					data.setContent("decision_pending_label", "Decision message:");
					data.setAttribute("decision_pending_button", "value", "Close request");
				}
				else
				{
					data.setContent("decision_pending", "");
				}

				// Decision made.
				if (currentQuery.getStatus() == Request.Status.OPENED)
				{
					data.setContent("decision_made", "");
				}
				else
				{
					data.setContent("decision_pending", "");

					data.setContent("decision_made_maker_label", "Decision maker:");
					data.setContent("decision_made_maker", kiwa.accountManager().getAccountById(currentQuery.getDecisionMakerId()).getFullName());

					data.setContent("decision_made_date_label", "Decision date:");
					data.setContent("decision_made_date", currentQuery.getDecisionDate().toString("dd/MM/yyyy"));

					data.setContent("decision_made_comment_label", "Decision message:");
					data.setContent("decision_made_comment", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(currentQuery.getDecisionComment())));
				}

				//
				data.setContent("back_button", "Back");

				data.setContent("cancel_request_button", "Cancel this request");
				data.setAttribute("cancel_request_button", "href", "cancel_request.xhtml?request_id" + currentQuery.getId());

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, content);

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

}

// ////////////////////////////////////////////////////////////////////////
