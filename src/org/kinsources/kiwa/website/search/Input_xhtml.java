/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.search;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.stag.Stag;
import org.kinsources.kiwa.stag.StatisticTag;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.tip.puck.net.relations.roles.RoleRelationWorker.CousinClassification;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Input_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Input_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/search/input.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.search.search", request);

			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Use parameters.
			// ===============

			// Send response.
			// ==============
			TagDataManager data = new TagDataManager();

			//
			data.setAttribute("individual_count_min", "name", StatisticTag.NUM_OF_INDIVIDUALS.name().toLowerCase() + "_min");
			data.setAttribute("individual_count_max", "name", StatisticTag.NUM_OF_INDIVIDUALS.name().toLowerCase() + "_max");

			data.setAttribute("men_count_min", "name", StatisticTag.NUM_OF_MEN.name().toLowerCase() + "_min");
			data.setAttribute("men_count_max", "name", StatisticTag.NUM_OF_MEN.name().toLowerCase() + "_max");
			data.setAttribute("men_rate_min", "name", StatisticTag.RATE_OF_MEN.name().toLowerCase() + "_min");
			data.setAttribute("men_rate_max", "name", StatisticTag.RATE_OF_MEN.name().toLowerCase() + "_max");

			data.setAttribute("women_count_min", "name", StatisticTag.NUM_OF_WOMEN.name().toLowerCase() + "_min");
			data.setAttribute("women_count_max", "name", StatisticTag.NUM_OF_WOMEN.name().toLowerCase() + "_max");
			data.setAttribute("women_rate_min", "name", StatisticTag.RATE_OF_WOMEN.name().toLowerCase() + "_min");
			data.setAttribute("women_rate_max", "name", StatisticTag.RATE_OF_WOMEN.name().toLowerCase() + "_max");

			data.setAttribute("unknown_count_min", "name", StatisticTag.NUM_OF_UNKNOWN.name().toLowerCase() + "_min");
			data.setAttribute("unknown_count_max", "name", StatisticTag.NUM_OF_UNKNOWN.name().toLowerCase() + "_max");
			data.setAttribute("unknown_rate_min", "name", StatisticTag.RATE_OF_UNKNOWN.name().toLowerCase() + "_min");
			data.setAttribute("unknown_rate_max", "name", StatisticTag.RATE_OF_UNKNOWN.name().toLowerCase() + "_max");

			data.setAttribute("non_single_men_count_min", "name", StatisticTag.NUM_OF_NON_SINGLE_MEN.name().toLowerCase() + "_min");
			data.setAttribute("non_single_men_count_max", "name", StatisticTag.NUM_OF_NON_SINGLE_MEN.name().toLowerCase() + "_max");

			data.setAttribute("non_single_women_count_min", "name", StatisticTag.NUM_OF_NON_SINGLE_WOMEN.name().toLowerCase() + "_min");
			data.setAttribute("non_single_women_count_max", "name", StatisticTag.NUM_OF_NON_SINGLE_WOMEN.name().toLowerCase() + "_max");

			data.setAttribute("union_count_min", "name", StatisticTag.NUM_OF_UNIONS.name().toLowerCase() + "_min");
			data.setAttribute("union_count_max", "name", StatisticTag.NUM_OF_UNIONS.name().toLowerCase() + "_max");

			data.setAttribute("marriage_count_min", "name", StatisticTag.NUM_OF_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("marriage_count_max", "name", StatisticTag.NUM_OF_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("marriage_density_min", "name", StatisticTag.DENSITY_OF_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("marriage_density_max", "name", StatisticTag.DENSITY_OF_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("fertile_marriage_count_min", "name", StatisticTag.NUM_OF_FERTILE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("fertile_marriage_count_max", "name", StatisticTag.NUM_OF_FERTILE_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("fertile_marriage_rate_min", "name", StatisticTag.RATE_OF_FERTILE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("fertile_marriage_rate_max", "name", StatisticTag.RATE_OF_FERTILE_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("cross_first_cousin_marriages_label", "title", Stag.CROSS_FIRST_COUSIN_MARRIAGE_PATTERN.toString());
			data.setAttribute("cross_first_cousin_marriage_count_min", "name", StatisticTag.NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("cross_first_cousin_marriage_count_max", "name", StatisticTag.NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("cross_first_cousin_marriage_rate_min", "name", StatisticTag.RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("cross_first_cousin_marriage_rate_max", "name", StatisticTag.RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("levirate_marriages_label", "title", Stag.LEVIRATE_MARRIAGE_PATTERN.toString());
			data.setAttribute("levirate_marriage_count_min", "name", StatisticTag.NUM_OF_LEVIRATE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("levirate_marriage_count_max", "name", StatisticTag.NUM_OF_LEVIRATE_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("levirate_marriage_rate_min", "name", StatisticTag.RATE_OF_LEVIRATE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("levirate_marriage_rate_max", "name", StatisticTag.RATE_OF_LEVIRATE_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("sororate_marriages_label", "title", Stag.SORORATE_MARRIAGE_PATTERN.toString());
			data.setAttribute("sororate_marriage_count_min", "name", StatisticTag.NUM_OF_SORORATE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("sororate_marriage_count_max", "name", StatisticTag.NUM_OF_SORORATE_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("sororate_marriage_rate_min", "name", StatisticTag.RATE_OF_SORORATE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("sororate_marriage_rate_max", "name", StatisticTag.RATE_OF_SORORATE_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("double_or_exchange_marriages_label", "title", Stag.DOUBLE_OR_EXCHANGE_MARRIAGE_PATTERN.toString());
			data.setAttribute("double_or_exchange_marriage_count_min", "name", StatisticTag.NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("double_or_exchange_marriage_count_max", "name", StatisticTag.NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("double_or_exchange_marriage_rate_min", "name", StatisticTag.RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("double_or_exchange_marriage_rate_max", "name", StatisticTag.RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("niece_nephew_marriages_label", "title", Stag.NIECE_NEPHEW_MARRIAGE_PATTERN.toString());
			data.setAttribute("niece_nephew_marriage_count_min", "name", StatisticTag.NUM_OF_NIECE_NEPHEW_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("niece_nephew_marriage_count_max", "name", StatisticTag.NUM_OF_NIECE_NEPHEW_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("niece_nephew_marriage_rate_min", "name", StatisticTag.RATE_OF_NIECE_NEPHEW_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("niece_nephew_marriage_rate_max", "name", StatisticTag.RATE_OF_NIECE_NEPHEW_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("parallel_first_cousin_marriages_label", "title", Stag.PARALLEL_FIRST_COUSIN_MARRIAGE_PATTERN.toString());
			data.setAttribute("parallel_first_cousin_marriage_count_min", "name", StatisticTag.NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("parallel_first_cousin_marriage_count_max", "name", StatisticTag.NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("parallel_first_cousin_marriage_rate_min", "name", StatisticTag.RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("parallel_first_cousin_marriage_rate_max", "name", StatisticTag.RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("first_cousin_marriages_label", "title", Stag.FIRST_COUSIN_MARRIAGE_PATTERN.toString());
			data.setAttribute("first_cousin_marriage_count_min", "name", StatisticTag.NUM_OF_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("first_cousin_marriage_count_max", "name", StatisticTag.NUM_OF_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("first_cousin_marriage_rate_min", "name", StatisticTag.RATE_OF_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("first_cousin_marriage_rate_max", "name", StatisticTag.RATE_OF_FIRST_COUSIN_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("double_marriages_label", "title", Stag.DOUBLE_MARRIAGE_PATTERN.toString());
			data.setAttribute("double_marriage_count_min", "name", StatisticTag.NUM_OF_DOUBLE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("double_marriage_count_max", "name", StatisticTag.NUM_OF_DOUBLE_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("double_marriage_rate_min", "name", StatisticTag.RATE_OF_DOUBLE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("double_marriage_rate_max", "name", StatisticTag.RATE_OF_DOUBLE_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("exchange_marriages_label", "title", Stag.EXCHANGE_MARRIAGE_PATTERN.toString());
			data.setAttribute("exchange_marriage_count_min", "name", StatisticTag.NUM_OF_EXCHANGE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("exchange_marriage_count_max", "name", StatisticTag.NUM_OF_EXCHANGE_MARRIAGES.name().toLowerCase() + "_max");
			data.setAttribute("exchange_marriage_rate_min", "name", StatisticTag.RATE_OF_EXCHANGE_MARRIAGES.name().toLowerCase() + "_min");
			data.setAttribute("exchange_marriage_rate_max", "name", StatisticTag.RATE_OF_EXCHANGE_MARRIAGES.name().toLowerCase() + "_max");

			data.setAttribute("parent_child_ties_count_min", "name", StatisticTag.NUM_OF_PARENT_CHILD_TIES.name().toLowerCase() + "_min");
			data.setAttribute("parent_child_ties_count_max", "name", StatisticTag.NUM_OF_PARENT_CHILD_TIES.name().toLowerCase() + "_max");

			data.setAttribute("filiation_density_min", "name", StatisticTag.DENSITY_OF_FILIATION.name().toLowerCase() + "_min");
			data.setAttribute("filiation_density_max", "name", StatisticTag.DENSITY_OF_FILIATION.name().toLowerCase() + "_max");

			data.setAttribute("depth_min", "name", StatisticTag.DEPTH.name().toLowerCase() + "_min");
			data.setAttribute("depth_max", "name", StatisticTag.DEPTH.name().toLowerCase() + "_max");

			data.setAttribute("depth_mean_min", "name", StatisticTag.DEPTH_MEAN.name().toLowerCase() + "_min");
			data.setAttribute("depth_mean_max", "name", StatisticTag.DEPTH_MEAN.name().toLowerCase() + "_max");

			data.setAttribute("agnatic_fratry_size_mean_min", "name", StatisticTag.MEAN_AGNATIC_FRATRY_SIZE.name().toLowerCase() + "_min");
			data.setAttribute("agnatic_fratry_size_mean_max", "name", StatisticTag.MEAN_AGNATIC_FRATRY_SIZE.name().toLowerCase() + "_max");

			data.setAttribute("uterine_fratry_size_mean_min", "name", StatisticTag.MEAN_UTERINE_FRATRY_SIZE.name().toLowerCase() + "_min");
			data.setAttribute("uterine_fratry_size_mean_max", "name", StatisticTag.MEAN_UTERINE_FRATRY_SIZE.name().toLowerCase() + "_max");

			data.setAttribute("component_count_min", "name", StatisticTag.NUM_OF_COMPONENTS.name().toLowerCase() + "_min");
			data.setAttribute("component_count_max", "name", StatisticTag.NUM_OF_COMPONENTS.name().toLowerCase() + "_max");

			//
			data.setAttribute("term_count_min", "name", StatisticTag.NUM_OF_TERMS.name().toLowerCase() + "_min");
			data.setAttribute("term_count_max", "name", StatisticTag.NUM_OF_TERMS.name().toLowerCase() + "_max");

			//
			kiwa.hico().generateBubble(data, "bubble.search.name", locale);
			kiwa.hico().generateBubble(data, "bubble.search.author", locale);
			kiwa.hico().generateBubble(data, "bubble.search.coder", locale);
			kiwa.hico().generateBubble(data, "bubble.search.contributor", locale);
			kiwa.hico().generateBubble(data, "bubble.search.organization", locale);
			kiwa.hico().generateBubble(data, "bubble.search.atlas_code", locale);
			kiwa.hico().generateBubble(data, "bubble.search.location", locale);
			kiwa.hico().generateBubble(data, "bubble.search.country", locale);
			kiwa.hico().generateBubble(data, "bubble.search.region", locale);
			kiwa.hico().generateBubble(data, "bubble.search.continent", locale);
			kiwa.hico().generateBubble(data, "bubble.search.dataset_file", locale);
			kiwa.hico().generateBubble(data, "bubble.search.individuals", locale);
			kiwa.hico().generateBubble(data, "bubble.search.men", locale);
			kiwa.hico().generateBubble(data, "bubble.search.women", locale);
			kiwa.hico().generateBubble(data, "bubble.search.unknown", locale);
			kiwa.hico().generateBubble(data, "bubble.search.non_single_men", locale);
			kiwa.hico().generateBubble(data, "bubble.search.non_single_women", locale);
			kiwa.hico().generateBubble(data, "bubble.search.unions", locale);
			kiwa.hico().generateBubble(data, "bubble.search.marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.marriage_density", locale);
			kiwa.hico().generateBubble(data, "bubble.search.fertile_marriage", locale);
			kiwa.hico().generateBubble(data, "bubble.search.parent_child_ties", locale);
			kiwa.hico().generateBubble(data, "bubble.search.filiation_density", locale);
			kiwa.hico().generateBubble(data, "bubble.search.first_cousin_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.cross_first_cousin_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.parallel_first_cousin_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.niece_nephew_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.levirate_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.sororate_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.double_or_exchange_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.double_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.exchange_marriages", locale);
			kiwa.hico().generateBubble(data, "bubble.search.depth", locale);
			kiwa.hico().generateBubble(data, "bubble.search.depth_mean", locale);
			kiwa.hico().generateBubble(data, "bubble.search.agnatic_fratry_size_mean", locale);
			kiwa.hico().generateBubble(data, "bubble.search.uterine_fratry_size_mean", locale);
			kiwa.hico().generateBubble(data, "bubble.search.components", locale);

			// Atlas code list.
			int lineIndex = 0;
			for (String atlasCodeName : kiwa.stag().getAtlasCodeNames())
			{
				String name = XMLTools.escapeXmlBlank(atlasCodeName);
				data.setContent("atlas_code_option", lineIndex, name);
				data.setAttribute("atlas_code_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Author name list.
			lineIndex = 0;
			for (String authorName : kiwa.stag().getAuthorNames())
			{
				String name = XMLTools.escapeXmlBlank(authorName);
				data.setContent("author_option", lineIndex, name);
				data.setAttribute("author_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Coder name list.
			lineIndex = 0;
			for (String coderName : kiwa.stag().getCoderNames())
			{
				String name = XMLTools.escapeXmlBlank(coderName);
				data.setContent("coder_option", lineIndex, name);
				data.setAttribute("coder_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Dataset names list.
			lineIndex = 0;
			for (String datasetName : kiwa.stag().getDatasetNames())
			{
				String name = XMLTools.escapeXmlBlank(datasetName);
				data.setContent("dataset_name_option", lineIndex, name);
				data.setAttribute("dataset_name_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Country names list.
			lineIndex = 0;
			for (String countryName : kiwa.stag().getCountryNames())
			{
				String name = XMLTools.escapeXmlBlank(countryName);
				data.setContent("country_option", lineIndex, name);
				data.setAttribute("country_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Contributor names list.
			lineIndex = 0;
			for (String contributorName : kiwa.stag().getContributorNames())
			{
				String name = XMLTools.escapeXmlBlank(contributorName);
				data.setContent("contributor_option", lineIndex, name);
				data.setAttribute("contributor_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Organization names list.
			lineIndex = 0;
			for (String organizationName : kiwa.stag().getOrganizationNames())
			{
				String name = XMLTools.escapeXmlBlank(organizationName);
				data.setContent("organization_option", lineIndex, name);
				data.setAttribute("organization_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Region names list.
			lineIndex = 0;
			for (String regionName : kiwa.stag().getRegionNames())
			{
				String name = XMLTools.escapeXmlBlank(regionName);
				data.setContent("region_option", lineIndex, name);
				data.setAttribute("region_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Location names list.
			lineIndex = 0;
			for (String locationName : kiwa.stag().getLocationNames())
			{
				String name = XMLTools.escapeXmlBlank(locationName);
				data.setContent("location_option", lineIndex, name);
				data.setAttribute("location_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// Continent names list.
			lineIndex = 0;
			for (String continentName : kiwa.stag().getContinentNames())
			{
				String name = XMLTools.escapeXmlBlank(continentName);
				data.setContent("continent_option", lineIndex, name);
				data.setAttribute("continent_option", lineIndex, "value", name);

				lineIndex += 1;
			}

			// ==============================================================
			data.setAttribute("terms_self_term", "name", StatisticTag.SELF_NAME.name().toLowerCase());

			data.setAttribute("terms_count_min", "name", StatisticTag.NUM_OF_TERMS.name().toLowerCase() + "_min");
			data.setAttribute("terms_count_max", "name", StatisticTag.NUM_OF_TERMS.name().toLowerCase() + "_max");

			data.setAttribute("terms_male_min", "name", StatisticTag.NUM_OF_MALES.name().toLowerCase() + "_min");
			data.setAttribute("terms_male_max", "name", StatisticTag.NUM_OF_MALES.name().toLowerCase() + "_max");

			data.setAttribute("terms_male_exclusive_min", "name", StatisticTag.NUM_OF_MALES_EXCLUSIVE.name().toLowerCase() + "_min");
			data.setAttribute("terms_male_exclusive_max", "name", StatisticTag.NUM_OF_MALES_EXCLUSIVE.name().toLowerCase() + "_max");

			data.setAttribute("terms_female_min", "name", StatisticTag.NUM_OF_FEMALES.name().toLowerCase() + "_min");
			data.setAttribute("terms_female_max", "name", StatisticTag.NUM_OF_FEMALES.name().toLowerCase() + "_max");

			data.setAttribute("terms_female_exclusive_min", "name", StatisticTag.NUM_OF_FEMALES_EXCLUSIVE.name().toLowerCase() + "_min");
			data.setAttribute("terms_female_exclusive_max", "name", StatisticTag.NUM_OF_FEMALES_EXCLUSIVE.name().toLowerCase() + "_max");

			data.setAttribute("terms_male_speaker_min", "name", StatisticTag.NUM_OF_MALES_SPEAKER.name().toLowerCase() + "_min");
			data.setAttribute("terms_male_speaker_max", "name", StatisticTag.NUM_OF_MALES_SPEAKER.name().toLowerCase() + "_max");

			data.setAttribute("terms_male_speaker_exclusive_min", "name", StatisticTag.NUM_OF_MALES_SPEAKER_EXCLUSIVE.name().toLowerCase() + "_min");
			data.setAttribute("terms_male_speaker_exclusive_max", "name", StatisticTag.NUM_OF_MALES_SPEAKER_EXCLUSIVE.name().toLowerCase() + "_max");

			data.setAttribute("terms_female_speaker_min", "name", StatisticTag.NUM_OF_FEMALES_SPEAKER.name().toLowerCase() + "_min");
			data.setAttribute("terms_female_speaker_max", "name", StatisticTag.NUM_OF_FEMALES_SPEAKER.name().toLowerCase() + "_max");

			data.setAttribute("terms_female_speaker_exclusive_min", "name", StatisticTag.NUM_OF_FEMALES_SPEAKER_EXCLUSIVE.name().toLowerCase() + "_min");
			data.setAttribute("terms_female_speaker_exclusive_max", "name", StatisticTag.NUM_OF_FEMALES_SPEAKER_EXCLUSIVE.name().toLowerCase() + "_max");

			data.setAttribute("terms_autoreciprocal_min", "name", StatisticTag.NUM_OF_AUTORECIPROCAL_TERMS.name().toLowerCase() + "_min");
			data.setAttribute("terms_autoreciprocal_max", "name", StatisticTag.NUM_OF_AUTORECIPROCAL_TERMS.name().toLowerCase() + "_max");

			data.setAttribute("terms_recursive", "name", StatisticTag.RECURSIVE_TERMS.name().toLowerCase());

			{
				data.setContent("terms_ascendant_classification", 0, "terms_ascendant_classification_value", "All");
				data.setAttribute("terms_ascendant_classification", 0, "terms_ascendant_classification_radio", "name", "terms_ascendant_classification");
				data.setAttribute("terms_ascendant_classification", 0, "terms_ascendant_classification_radio", "value", "");
				data.setAttribute("terms_ascendant_classification", 0, "terms_ascendant_classification_radio", "checked", "checked");

				int index = 1;
				for (CousinClassification classification : CousinClassification.values())
				{
					String label = StringUtils.capitalize(classification.name().toLowerCase().replace('_', ' '));
					data.setContent("terms_ascendant_classification", index, "terms_ascendant_classification_value", label);
					data.setAttribute("terms_ascendant_classification", index, "terms_ascendant_classification_radio", "name", "terms_ascendant_classification");
					data.setAttribute("terms_ascendant_classification", index, "terms_ascendant_classification_radio", "value", classification.name());
					index += 1;
				}
			}

			{
				data.setContent("terms_cousin_classification", 0, "terms_cousin_classification_value", "All");
				data.setAttribute("terms_cousin_classification", 0, "terms_cousin_classification_radio", "name", "terms_cousin_classification");
				data.setAttribute("terms_cousin_classification", 0, "terms_cousin_classification_radio", "value", "");
				data.setAttribute("terms_cousin_classification", 0, "terms_cousin_classification_radio", "checked", "checked");

				int index = 1;
				for (CousinClassification classification : CousinClassification.values())
				{
					String label = StringUtils.capitalize(classification.name().toLowerCase().replace('_', ' '));
					data.setContent("terms_cousin_classification", index, "terms_cousin_classification_value", label);
					data.setAttribute("terms_cousin_classification", index, "terms_cousin_classification_radio", "name", "terms_cousin_classification");
					data.setAttribute("terms_cousin_classification", index, "terms_cousin_classification_radio", "value", classification.name());
					index += 1;
				}
			}

			data.setAttribute("terms_density_male_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_male_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");
			data.setAttribute("terms_density_female_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_female_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");

			data.setAttribute("terms_mean_degree_male_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE.name().toLowerCase() + "_min");
			data.setAttribute("terms_mean_degree_male_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE.name().toLowerCase() + "_max");
			data.setAttribute("terms_mean_degree_female_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE.name().toLowerCase() + "_min");
			data.setAttribute("terms_mean_degree_female_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE.name().toLowerCase() + "_max");

			data.setAttribute("terms_density_male_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_male_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");
			data.setAttribute("terms_density_female_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_female_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");

			data.setAttribute("terms_density_male_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_male_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");
			data.setAttribute("terms_density_female_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_female_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");

			data.setAttribute("terms_density_male_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_male_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");
			data.setAttribute("terms_density_female_speaker_min", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_min");
			data.setAttribute("terms_density_female_speaker_max", "name", StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name().toLowerCase() + "_max");

			// ==============================================================

			//
			StringBuffer content = xidyn.dynamize(data);

			//
			StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.SEARCH, accountId, locale, content);

			// Display page.
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
