/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * Copyright 2017      Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.editorial;

import java.util.Locale;

import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;

/**
 *
 */
public class AboutView
{
	public enum MenuItem
	{
		TAKE_A_TOUR,
		ABOUT_KINSOURCES,
		ANR_KINSOURCES,
		PARTNERS,
		CONTACT_US,
		LEGAL,
		PRIVACY,
		TERMS_OF_USE,
		THE_KINSOURCES_LOGO,
		RESOURCES,
		SCIENTIFIC_BOARD,
		RSS,
		LITERATURE,
		SITEMAP,
		COMMUNITY,
	}

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AboutView.class);
	private static TranslatorPresenter editorial = new TranslatorPresenter("/org/kinsources/kiwa/website/editorial/about_view.html");
	private MenuItem menuItem;

	/**
	 * 
	 */
	public AboutView(final MenuItem menuItem)
	{
		this.menuItem = menuItem;
	}

	/**
	 * 
	 */
	public StringBuffer getHtml(final Long accountId, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		//
		data.setContent("take_a_tour", "Take a tour of Kinsources");
		data.setContent("about_kinsources", "About Kinsources");
		data.setContent("anr_kinsources", "ANR Kinsources");
		data.setContent("partners", "Partners");
		data.setContent("contact_us", "Contact us");
		data.setContent("legal", "Legal notices");
		data.setContent("privacy", "Privacy");
		data.setContent("terms_of_use", "Terms of use");
		data.setContent("the_kinsources_logo", "The Kinsources logo");
		data.setContent("tools", "Tools");
		data.setContent("resources", "Resources");
		data.setContent("how_to_use_kinsources", "How to use Kinsources");
		data.setContent("how_to_submit_a_dataset", "How to submit a dataset");
		data.setContent("how_to_choose_a_license", "How to choose a license");
		data.setContent("dataset_formats", "Dataset formats");
		data.setContent("scientific_board", "Scientific Board");
		data.setContent("rss", "RSS feeds");
		data.setContent("literature", "Literature");
		data.setContent("sitemap", "Sitemap");
		data.setContent("community", "Community");

		//
		data.setAttribute(this.menuItem.toString().toLowerCase(), "class", "active");

		//
		data.setContent("article", Kiwa.instance().hico().getText(this.menuItem.toString().replace('_', ' '), locale));

		//
		StringBuffer content = editorial.dynamize(data);

		result = Kiwa.instance().getCharterView().getHtml(KiwaCharterView.Menu.ABOUT, accountId, locale, content);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
