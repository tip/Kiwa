/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;

/**
 *
 */
public class Log_in_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Log_in_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			kiwa.logPageHit("pages.log_in", request);

			// Get parameters.
			// ===============
			String login = request.getParameter("login");
			if ((login == null) || (login.trim().equals("")))
			{
				throw new Exception("Empty login value.");
			}
			login = login.trim();
			logger.info("login=[" + login + "]");

			String password = request.getParameter("password");
			if ((password == null) || (password.trim().equals("")))
			{
				throw new Exception("Empty password value.");
			}
			password = password.trim();
			// DO NOT UNCOMMENT logger.info("password=[" + password + "]");

			//
			logger.info("CHECK_LOGIN for login [{}]", login);

			// Use parameters.
			// ===============

			if (kiwa.accountManager().isActivationPending(login))
			{
				Redirector.redirect(response, "/accounts/activation_pending.xhtml");
			}
			else
			{
				//
				Account account = kiwa.accountManager().authenticate(login, password);

				//
				if ((account == null) || (account.isNotActivated()))
				{
					//
					logger.info("authentication failed for [" + login + "]");
					kiwa.log(Level.INFO, "events.unsuccessful_login", "[login={}]", login);

					//
					response.setHeader("Location", "/accounts/login_failed.xhtml");
					response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
				}
				else
				{
					//
					logger.info("Successful authentication for [" + login + "]");
					kiwa.log(Level.INFO, "events.successful_login", "[login={}]", login);

					//
					kiwa.updateAccountLastConnection(account);

					//
					kiwa.securityAgent().setAuthenticated(request, response, String.valueOf(account.getId()), account.getFullName());

					//
					Redirector.redirect(response, "/");
				}
			}

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
