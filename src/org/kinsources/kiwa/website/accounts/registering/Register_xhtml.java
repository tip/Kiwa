/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.registering;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.EmailScope;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;

/**
 *
 */
public class Register_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Register_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static TranslatorPresenter mailPresenter = new TranslatorPresenter("/org/kinsources/kiwa/website/accounts/registering/activation_mail.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);

			//
			kiwa.logPageHit("pages.accounts.registering.register", request);

			String firstNames = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("first_names")));
			logger.info("firstNames=[{}]", firstNames);

			String lastName = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("last_name")));
			logger.info("lastName=[{}]", lastName);

			String email = XMLTools.unescapeXmlBlank(StringUtils.lowerCase(StringUtils.trim(request.getParameter("email"))));
			logger.info("email=[{}]", email);

			String emailConfirmation = XMLTools.unescapeXmlBlank(StringUtils.lowerCase(StringUtils.trim(request.getParameter("email_confirmation"))));
			logger.info("emailConfirmation=[{}]", emailConfirmation);

			String password = StringUtils.trim(request.getParameter("password"));
			// Do not log the password.

			String passwordConfirmation = StringUtils.trim(request.getParameter("password_confirmation"));
			// Do not log the password.

			String organization = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("organization")));
			logger.info("organization=[{}]", organization);

			String aboutMe = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("about_me")));
			logger.info("aboutMe=[{}]", aboutMe);

			String website = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("website")));
			logger.info("website=[{}]", website);

			String country = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("country")));
			logger.info("country=[{}]", country);

			String timeZoneParameter = StringUtils.trim(request.getParameter("timezone"));
			DateTimeZone timeZone = DateTimeZone.forID(timeZoneParameter);
			logger.info("timezone=[{}][{}]", timeZoneParameter, timeZone);

			String emailNotificationParameter = StringUtils.trim(request.getParameter("email_notification"));
			boolean emailNotification = Boolean.valueOf(emailNotificationParameter);
			logger.info("emailNotification=[{}][{}]", emailNotificationParameter, emailNotification);

			String emailScopeParameter = StringUtils.trim(request.getParameter("email_scope"));
			EmailScope emailScope;
			if (StringUtils.equalsIgnoreCase(emailScopeParameter, "public"))
			{
				emailScope = EmailScope.PUBLIC;
			}
			else
			{
				emailScope = EmailScope.PRIVATE;
			}
			logger.info("emailScope=[{}][{}]", emailScopeParameter, emailScope);

			String captchaKey = StringUtils.trim(request.getParameter("captcha_key"));
			logger.info("captchaKey=[{}]", captchaKey);

			String captchaTime = StringUtils.trim(request.getParameter("captcha_time"));
			logger.info("captchaTime=[{}]", captchaTime);

			String captchaAnswer = StringUtils.trim(request.getParameter("captcha_answer"));
			logger.info("captchaAnswer=[{}]", captchaAnswer);

			// Use parameters.
			// ===============
			logger.info("REGISTER for login [{}]", email);
			kiwa.log(Level.INFO, "events.register", "[email={}]", email);

			//
			if (UpLetterCaptcha.isNotValid("Kinsources", captchaTime, captchaKey, captchaAnswer))
			{
				throw new IllegalArgumentException("Invalid captcha.");
			}
			else if (!StringUtils.equals(email, emailConfirmation))
			{
				throw new IllegalArgumentException("Your email and confirmation are differents.");
			}
			else if (!StringUtils.equals(password, passwordConfirmation))
			{
				throw new IllegalArgumentException("Your password and confirmation are differents.");
			}
			else
			{
				//
				Account account = kiwa.createAccount(email, firstNames, lastName, passwordConfirmation, emailScope, organization, aboutMe, country, website, timeZone, emailNotification);

				//
				sendActivationMail(request, locale, account);

				//
				notifyWebmasters(account, locale);

				//
				Redirector.redirect(response, "registering.xhtml");
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your registration is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + exception.getMessage() + "</pre>");

			ErrorView.show(request, response, "Registration issue", message, "/accounts/registering/registration.xhtml");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private String notifyWebmasters(final Account account, final Locale locale) throws Exception
	{
		String result;

		//
		StringList directLink = new StringList();
		directLink.append(kiwa.getWebsiteUrl());
		directLink.append("accounts/admin/account_edition.xhtml");
		directLink.append("?target_id=").append(account.getId());

		//
		StringList html = new StringList();
		html.appendln("<p>New account created.</p>");
		html.appendln("<p>");
		html.append("Email: ").append(StringEscapeUtils.escapeXml(account.getEmail())).appendln("<br/>");
		html.append("Full name: ").append(StringEscapeUtils.escapeXml(account.getFullName())).appendln("<br/>");
		html.append("<br/>");
		html.append("<a href=\"").append(directLink.toString()).appendln("\" class=\"button_second\">Direct link</a><br/>");
		html.appendln("</p>");

		kiwa.notifyWebmasters(account, "Account event: new account", html.toString(), locale);

		//
		result = html.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private void sendActivationMail(final HttpServletRequest request, final Locale locale, final Account account) throws Exception
	{
		// Build a key.
		String activationTime = String.valueOf(DateTime.now().getMillis());
		String activationKey = kiwa.securityAgent().computeAuth(String.valueOf(account.getId()), activationTime);

		// Build the mail content.
		TagDataManager data = new TagDataManager();

		//
		StringList activationLinkBuffer = new StringList();
		activationLinkBuffer.append(kiwa.getWebsiteUrl());
		activationLinkBuffer.append("accounts/registering/confirm_registration.xhtml");
		activationLinkBuffer.append("?id=").append(account.getId());
		activationLinkBuffer.append("&activation_time=").append(activationTime);
		activationLinkBuffer.append("&activation_key=").append(activationKey);

		String activationLink = activationLinkBuffer.toString();
		logger.debug("activationLink=[{}]", activationLink);

		//
		data.setAttribute("activation_link", "href", activationLink);

		//
		StringBuffer content = mailPresenter.dynamize(data);
		StringBuffer html = kiwa.getEmailCharterView().getHtml(account.getId(), locale, content);

		// Send activation email.
		kiwa.sendEmail(account.getEmail(), "[Kinsources] Welcome to Kinsources", html);
	}
}

// ////////////////////////////////////////////////////////////////////////
