/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.registering;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.kernel.Kiwa;

import fr.devinsy.util.StringList;

/**
 *
 */
public class UpLetterCaptcha
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UpLetterCaptcha.class);

	private String word;
	private long time;
	private String key;
	private String question;
	private String answer;
	public static final int duration = 10 * 60 * 1000;

	/**
	 * 
	 * @param word
	 */
	public UpLetterCaptcha(final String word)
	{
		//
		if (StringUtils.isBlank(word))
		{
			throw new IllegalArgumentException("word is null");
		}
		else
		{
			//
			this.word = word;

			//
			this.time = DateTime.now().getMillis();

			//
			build();
		}
	}

	/**
	 * 
	 */
	public void build()
	{
		//
		this.key = Kiwa.instance().securityAgent().computeAuth(this.word, String.valueOf(this.time));

		//
		StringBuffer questionBuffer = new StringBuffer(this.word.length());
		StringBuffer answerBuffer = new StringBuffer(this.word.length());
		for (int index = 0; index < this.word.length(); index++)
		{
			char letter = this.key.charAt(index);
			if (letter % 2 == 0)
			{
				questionBuffer.append(Character.toLowerCase(this.word.charAt(index)));
			}
			else
			{
				questionBuffer.append(Character.toUpperCase(this.word.charAt(index)));
				answerBuffer.append(Character.toLowerCase(this.word.charAt(index)));
			}
		}
		this.question = questionBuffer.toString();
		this.answer = answerBuffer.toString().toUpperCase();

	}

	public String getAnswer()
	{
		return this.answer;
	}

	public String getKey()
	{
		return this.key;
	}

	public String getQuestion()
	{
		return this.question;
	}

	public String getQuestionBolded()
	{
		String result;

		//
		String source = getQuestion();
		StringBuffer target = new StringBuffer(source.length() * 2);
		for (char letter : source.toCharArray())
		{
			if (Character.isUpperCase(letter))
			{
				target.append("<strong>").append(letter).append("</strong>");
			}
			else
			{
				target.append(letter);
			}
		}

		//
		result = target.toString();

		//
		return result;
	}

	public long getTime()
	{
		return this.time;
	}

	public String getWord()
	{
		return this.word;
	}

	public void setAnswer(final String answer)
	{
		this.answer = answer;
	}

	public void setKey(final String key)
	{
		this.key = key;
	}

	public void setQuestion(final String question)
	{
		this.question = question;
	}

	public void setTime(final long time)
	{
		this.time = time;
	}

	public void setWord(final String word)
	{
		this.word = word;
	}

	/**
	 * 
	 */
	@Override
	public String toString()
	{
		String result;

		StringList buffer = new StringList();

		buffer.appendln("[word=" + this.word + "]");
		buffer.appendln("[time=" + this.time + "]");
		buffer.appendln("[key=" + this.key + "]");
		buffer.appendln("[question=" + this.question + "]");
		buffer.appendln("[answer=" + this.answer + "]");

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param word
	 * @param time
	 * @param key
	 * @param answer
	 * @return
	 */
	public static boolean isNotValid(final String word, final long time, final String key, final String answer)
	{
		boolean result;

		result = !isValid(word, time, key, answer);

		//
		return result;
	}

	/**
	 * 
	 * @param word
	 * @param time
	 * @param key
	 * @param answer
	 * @return
	 */
	public static boolean isNotValid(final String word, final String time, final String key, final String answer)
	{
		boolean result;

		result = !isValid(word, time, key, answer);

		//
		return result;
	}

	/**
	 * 
	 * @param word
	 * @param time
	 * @param key
	 * @param answer
	 * @return
	 */
	public static boolean isValid(final String word, final long time, final String key, final String answer)
	{
		boolean result;

		if (answer == null)
		{
			result = false;
		}
		else
		{
			//
			UpLetterCaptcha captcha = new UpLetterCaptcha(word);
			captcha.setTime(time);
			captcha.build();

			//
			if (!StringUtils.equals(key, captcha.getKey()))
			{
				result = false;
			}
			else if (DateTime.now().getMillis() - time > duration)
			{
				result = false;
			}
			else
			{
				result = StringUtils.equals(captcha.getAnswer(), answer.toUpperCase());
			}

			//
			logger.debug("CAPTCHA time=" + captcha.getTime() + "?" + time);
			logger.debug("CAPTCHA time=" + captcha.getAnswer() + "?" + answer);
			logger.debug("CAPTCHA keyo=" + captcha.getKey());
			logger.debug("CAPTCHA keyi=" + key);
			logger.debug("CAPTCHA result=" + result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param word
	 * @param time
	 * @param key
	 * @param answer
	 * @return
	 */
	public static boolean isValid(final String word, final String time, final String key, final String answer)
	{
		boolean result;

		if (NumberUtils.isNumber(time))
		{
			result = isValid(word, Long.parseLong(time), key, answer);
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(final String[] args)
	{
		UpLetterCaptcha c = new UpLetterCaptcha("Kinsources");
		System.out.println(c);

		System.out.println(isValid("Kinsources", c.getTime(), c.getKey(), "Kinsources"));
		System.out.println(isValid("Kinsources", c.getTime(), c.getKey(), "KSRC"));
		System.out.println(isValid("Kinsources", c.getTime() + 60 * 60 * 1000, c.getKey(), c.getAnswer()));
		System.out.println(isValid("Kinsources", c.getTime(), c.getKey(), c.getAnswer()));
		System.out.println(isValid("Kinsources", c.getTime(), c.getKey(), c.getAnswer().toLowerCase()));
		System.out.println(isValid("Kinsources", c.getTime(), c.getKey(), c.getAnswer().toUpperCase()));
	}
}

// ////////////////////////////////////////////////////////////////////////
