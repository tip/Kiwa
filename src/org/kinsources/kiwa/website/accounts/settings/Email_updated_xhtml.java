/**
 * Copyright (C) 2013 DEVINSY <christian.momon@devinsy.fr>
 * 
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3, 29 June
 * 2007; or any later version you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * 
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.kinsources.kiwa.website.accounts.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.xidyn.presenters.TranslatorPresenter;

/**
 *
 */
public class Email_updated_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Email_updated_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static TranslatorPresenter presenter = new TranslatorPresenter("/org/kinsources/kiwa/website/accounts/settings/email_updated.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.accounts.settings.email_updated", request);

			// Get parameters
			// ==============
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Use parameters
			// ==============

			// Send response
			// ============
			StringBuffer html = kiwa.getCharterView().getHtml(accountId, locale, presenter.toString(locale));

			//
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////