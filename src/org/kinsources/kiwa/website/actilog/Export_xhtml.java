/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actilog;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.CSVActilog;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.actilog.Logs;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaExportCriteria;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.seligo.Seligo;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;

/**
 *
 */
public class Export_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Export_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("Actilog export starting...");
			kiwa.logPageHit("pages.actilog.export", request);

			//
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if ((account == null) || (!account.isRole(KiwaRoles.WEBMASTER)))
			{
				//
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String levelParameter = request.getParameter("criteria_level");
				logger.info("levelParameter=[{}]", levelParameter);
				Level criteriaLevel = EnumUtils.getEnum(Level.class, levelParameter);
				if (criteriaLevel == null)
				{
					criteriaLevel = Level.ALL;
				}

				String startDateParameter = StringUtils.trim(request.getParameter("criteria_start_date"));
				logger.info("startDateParameter=[{}]", startDateParameter);
				DateTime criteriaStartDate;
				try
				{
					criteriaStartDate = DateTime.parse(startDateParameter, DateTimeFormat.forPattern("dd/MM/yyyy"));
				}
				catch (Exception exception)
				{
					criteriaStartDate = null;
				}

				String endDateParameter = StringUtils.trim(request.getParameter("criteria_end_date"));
				logger.info("endDateParameter=[{}]", endDateParameter);
				DateTime criteriaEndDate;
				try
				{
					criteriaEndDate = DateTime.parse(endDateParameter, DateTimeFormat.forPattern("dd/MM/yyyy"));
				}
				catch (Exception exception)
				{
					criteriaEndDate = null;
				}

				String criteriaEvent = StringEscapeUtils.unescapeXml(request.getParameter("criteria_event"));
				logger.info("event=[{}]", criteriaEvent);

				String criteriaDetails = StringEscapeUtils.unescapeXml(request.getParameter("criteria_details"));
				logger.info("details=[{}]", criteriaDetails);

				String format = StringUtils.trim(request.getParameter("export_format"));
				logger.info("format=[{}]", format);

				logger.info("ACTILOG EXPORT [{}]", format);
				kiwa.log(Level.INFO, "events.actilog.export", "[format={}]", format);

				// Use parameters.
				// ===============
				Logs logs = kiwa.actilog().logs().findByLevel(criteriaLevel);

				if (criteriaStartDate != null)
				{
					logs = logs.findAfter(criteriaStartDate);
				}

				if (criteriaEndDate != null)
				{
					logs = logs.findBefore(criteriaEndDate);
				}

				if (StringUtils.isNotBlank(criteriaEvent))
				{
					logs = logs.findByEvent(Seligo.toRegex(criteriaEvent));
				}

				if (StringUtils.isNotBlank(criteriaDetails))
				{
					logs = logs.findByDetails(Seligo.toRegex(criteriaDetails));
				}

				logs = logs.sortByDate().reverse();

				//
				if ((format != null) && (format.equals("csv")))
				{
					//
					String fileName = "actilog-" + DateTime.now().toString("yyyy-MM-dd-HH'h'mm'mn'ss's'") + ".csv";

					//
					response.reset();
					response.setContentType(getServletContext().getMimeType(fileName));
					response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
					response.flushBuffer();

					//
					CSVActilog.write(response.getWriter(), logs);

				}
				else
				{
					//
					String fileName = "actilog-" + DateTime.now().toString("yyyy-MM-dd-HH'h'mm'mn'ss's'") + ".xml.zip";

					//
					response.reset();
					response.setContentType(getServletContext().getMimeType(fileName));
					response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
					response.flushBuffer();

					//
					KiwaExportCriteria criteria = new KiwaExportCriteria();
					criteria.setActilog(true);
					kiwa.export(response.getOutputStream(), criteria);
				}

				//
				response.flushBuffer();
			}
		}
		catch (final IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Actilog export issue", exception.getMessage(), "/actilog/actilog_admin.xhtml");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
