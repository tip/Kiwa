/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actilog;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Actilog;
import org.kinsources.kiwa.actilog.Log;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.actilog.Logs;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.seligo.Seligo;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Log_board_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Log_board_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/actilog/log_board.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.actilog.log_board", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if ((account == null) || (!account.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{

				// Get parameters
				// ==============
				String levelParameter = request.getParameter("criteria_level");
				logger.info("levelParameter=[{}]", levelParameter);
				Level criteriaLevel = EnumUtils.getEnum(Level.class, levelParameter);

				String startDateParameter = StringUtils.trim(request.getParameter("criteria_start_date"));
				logger.info("startDateParameter=[{}]", startDateParameter);
				DateTime criteriaStartDate;
				try
				{
					criteriaStartDate = DateTime.parse(startDateParameter, DateTimeFormat.forPattern("dd/MM/yyyy"));
				}
				catch (Exception exception)
				{
					criteriaStartDate = null;
				}

				String endDateParameter = StringUtils.trim(request.getParameter("criteria_end_date"));
				logger.info("endDateParameter=[{}]", endDateParameter);
				DateTime criteriaEndDate;
				try
				{
					criteriaEndDate = DateTime.parse(endDateParameter, DateTimeFormat.forPattern("dd/MM/yyyy"));
				}
				catch (Exception exception)
				{
					criteriaEndDate = null;
				}

				String criteriaEvent = StringEscapeUtils.unescapeXml(request.getParameter("criteria_event"));
				logger.info("event=[{}]", criteriaEvent);

				String criteriaDetails = StringEscapeUtils.unescapeXml(request.getParameter("criteria_details"));
				logger.info("details=[{}]", criteriaDetails);

				String criteriaLimit = StringEscapeUtils.unescapeXml(request.getParameter("criteria_limit"));
				logger.info("limit=[{}]", criteriaLimit);

				// Use parameters
				// ==============
				Actilog actilog = kiwa.actilog();

				//
				TagDataManager data = new TagDataManager();

				//
				if (criteriaLevel == null)
				{
					criteriaLevel = actilog.getLogLevel();
				}

				int levelIndex = 0;
				for (Level level : Log.Level.values())
				{
					//
					data.setContent("criteria_level_option", levelIndex, level.toString());
					data.setAttribute("criteria_level_option", levelIndex, "value", level.toString());

					//
					if (criteriaLevel == level)
					{
						data.setAttribute("criteria_level_option", levelIndex, "selected", "selected");
					}

					//
					levelIndex += 1;
				}

				//
				if (criteriaStartDate != null)
				{
					data.setAttribute("criteria_start_date", "value", criteriaStartDate.toString("dd/MM/yyyy"));
				}

				//
				if (criteriaEndDate != null)
				{
					data.setAttribute("criteria_end_date", "value", criteriaEndDate.toString("dd/MM/yyyy"));
				}

				//
				if (StringUtils.isNotBlank(criteriaEvent))
				{
					data.setAttribute("criteria_event", "value", StringEscapeUtils.escapeXml(criteriaEvent));
				}

				//
				if (StringUtils.isNotBlank(criteriaDetails))
				{
					data.setAttribute("criteria_details", "value", StringEscapeUtils.escapeXml(criteriaDetails));
				}

				//
				int limit;
				if (StringUtils.isBlank(criteriaLimit))
				{
					//
					data.setAttribute("limit_option_500", "selected", "selected");
					limit = 500;
				}
				else if (NumberUtils.isDigits(criteriaLimit))
				{
					//
					data.setAttribute("criteria_details", "value", StringEscapeUtils.escapeXml(criteriaDetails));

					//
					limit = Integer.parseInt(criteriaLimit);

					switch (limit)
					{
						case 500:
							//
							data.setAttribute("limit_option_500", "selected", "selected");
						break;
						case 1000:
							//
							data.setAttribute("limit_option_1000", "selected", "selected");
						break;
						case 5000:
							//
							data.setAttribute("limit_option_5000", "selected", "selected");
						break;
						case 10000:
							//
							data.setAttribute("limit_option_10000", "selected", "selected");
						break;
						default:
							//
							data.setAttribute("limit_option_500", "selected", "selected");
					}
				}
				else
				{
					data.setAttribute("limit_option_ALL", "selected", "selected");
					limit = Integer.MAX_VALUE;
				}

				//
				Logs logs = actilog.logs().findByLevel(criteriaLevel);

				if (criteriaStartDate != null)
				{
					logs = logs.findAfter(criteriaStartDate);
				}

				if (criteriaEndDate != null)
				{
					logs = logs.findBefore(criteriaEndDate);
				}

				if (StringUtils.isNotBlank(criteriaEvent))
				{
					logs = logs.findByEvent(Seligo.toRegex(criteriaEvent));
				}

				if (StringUtils.isNotBlank(criteriaDetails))
				{
					logs = logs.findByDetails(Seligo.toRegex(criteriaDetails));
				}

				if (limit > logs.size())
				{
					limit = logs.size();
				}

				logs = logs.sortByDate().reverse();

				//
				data.setContent("log_count", logs.size());

				//
				DateTime now = DateTime.now();
				for (int index = 0; index < limit; index++)
				{
					//
					Log log = logs.getByIndex(index);

					//
					data.setContent("log", index, "log_id", log.getId());
					data.setContent("log", index, "log_date", log.getTime().toString());
					data.setContent("log", index, "log_days", new Period(log.getTime(), now, PeriodType.days()).getDays());
					data.setContent("log", index, "log_level", log.getLevel().toString());
					data.setContent("log", index, "log_event", StringEscapeUtils.escapeXml(log.getEvent()));
					data.setContent("log", index, "log_details", StringEscapeUtils.escapeXml(log.getDetails()));
				}

				//
				data.setAttribute("export_csv_level", "value", criteriaLevel.toString());
				data.setAttribute("export_xml_level", "value", criteriaLevel.toString());

				if (criteriaStartDate != null)
				{
					data.setAttribute("export_csv_start_date", "value", criteriaStartDate.toString("dd/MM/yyyy"));
					data.setAttribute("export_xml_start_date", "value", criteriaStartDate.toString("dd/MM/yyyy"));
				}

				if (criteriaEndDate != null)
				{
					data.setAttribute("export_csv_end_date", "value", criteriaEndDate.toString("dd/MM/yyyy"));
					data.setAttribute("export_xml_end_date", "value", criteriaEndDate.toString("dd/MM/yyyy"));
				}

				if (StringUtils.isNotBlank(criteriaEvent))
				{
					data.setAttribute("export_csv_event", "value", StringEscapeUtils.escapeXml(criteriaEvent));
					data.setAttribute("export_xml_event", "value", StringEscapeUtils.escapeXml(criteriaEvent));
				}

				if (StringUtils.isNotBlank(criteriaDetails))
				{
					data.setAttribute("export_csv_details", "value", StringEscapeUtils.escapeXml(criteriaDetails));
					data.setAttribute("export_xml_details", "value", StringEscapeUtils.escapeXml(criteriaDetails));
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				// StringBuffer html =
				// kiwa.getCharterView().getHtml(account.getId(), locale,
				// view.getHtml(content.toString()));
				StringBuffer html = kiwa.getCharterView().getHtml(account.getId(), locale, content);

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
